<?php
/*
Package: Kentha
Description: Woocommerce template page
*/

get_header();

$layout = 'fullpage';

if (is_shop() || is_tax( 'product_cat' )){
	$layout = get_theme_mod( 'kentha_woocommerce_design', 'left-sidebar' );

} else {
	$layout = 'fullpage'; // seems we have to force it for some reason
}
?>
<!-- ======================= MAIN SECTION  ======================= -->
<div id="maincontent" class="kentha-woocommerce-content">
	<div class="qt-main qt-clearfix qt-3dfx-content">
		<?php get_template_part( 'phpincludes/part-background' ); ?>

		<div id="qtarticle" <?php post_class("qt-container qt-main-contents"); ?>>
			<div class="row">
				<?php 
				switch ($layout){
					case 'fullpage':
						?>
						<div class="qt-sidebar col s12 m12">
							<?php woocommerce_content(); ?>
						</div>
						<?php
						break;
					case 'right-sidebar':
						?>
						<div class="col s12 m8 l8">
							<?php woocommerce_content(); ?>
						</div>
						<div class="qt-sidebar col s12 m4 l4">
							<hr class="qt-spacer-m">
							<?php get_sidebar('woocommerce'); ?>
						</div>
						<?php
						break;
					case 'left-sidebar':
					default:
						?>
						<div class="qt-sidebar col s12 m4 l4">
							<hr class="qt-spacer-m">
							<?php get_sidebar('woocommerce'); ?>
						</div>
						<div class="col s12 m8 l8">
							<?php woocommerce_content(); ?>
						</div>
						<?php
						break;
				}
				?>
			</div>
			<hr class="qt-spacer-l">
		</div>



	</div>
</div>
<!-- ======================= MAIN SECTION END ======================= -->
<?php get_footer();