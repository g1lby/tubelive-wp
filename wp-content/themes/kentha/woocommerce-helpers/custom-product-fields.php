<?php
/*
* Package: Kentha
* This is a WooCommerce support file to add custom fields to products
*/



add_action('init', 'kentha_woocommerce_associated_release_fields');  
if(!function_exists('kentha_woocommerce_associated_release_fields')){
function kentha_woocommerce_associated_release_fields() {
	$fields_release = array(
		array(
			'label' => esc_html__('Connect album playlist', "kentha"),
			'id' => 'kentha_related_release',
			'type' => 'post_chosen',
			'posttype' => 'release'
			),
		
	);
	if( post_type_exists( 'product' ) && class_exists('custom_add_meta_box')){
		$details_box = new custom_add_meta_box( 'associated_release_fields', 'Connect album release', $fields_release, 'product', true );
	}
}}