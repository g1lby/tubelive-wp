<?php
/**
 * This is the configuration for the installer plguin.
 * This class (T2GConnectorConfig) is retriven by the official plugin function
 */
if(!class_exists("T2GConnectorConfig")){
class T2GConnectorConfig {
	public function __construct(){
		
	}
	///return $this->login_url.$this->client_id.'&redirect_uri='.$this->login_page;
	// This function needs to stay in the functions.php of the theme
	public function t2gconnector_product_sku() {
		return '21148850'; // if false we don't do check or auto updates of theme
	}
	// generic settings
	public function t2gconnector_settings(){
		$settings = array (
			"dashboard_title" => "Kentha",
			'logo' => get_template_directory_uri() . '/img/logo.png',
			'auto_update_theme' => false,
			'manual_url' => esc_url('http://qantumthemes.xyz/manuals/kentha/'),
			'helpdesk_url' => esc_url('http://qantumthemes.com/helpdesk/forums/forum/kentha/'),
			"deepcheck" => false,
			'messageboard_top' => esc_url('http://qantumthemes.xyz/t2gconnector-comm/kentha/kentha_messageboard_top.php'),
			'icon' => get_template_directory_uri() . '/img/theme-icon.png',
		);
		return $settings;
	}
	// for auto plugin update
	public function t2gconnector_plugins_list(){
		$repo = esc_url('http://qantumthemes.xyz/public_plugins/kentha/');
		$folder = 'knth190213-Psocial/';
		$urlpath = $repo.$folder;
		$plugins = array (
			array(
				'name'      		 => esc_html__('Kentha Theme Dashboard','kentha'),
				'slug'      		 => 't2gconnectorclient',
				'version'			 => '1.3.7',
				'required'  		 => true,
				'source'             =>  $repo . 'permanent/t2gconnectorclient-1.3.7.zip', 
				'force_activation'   => false, 
				'force_deactivation' => false
			),
			array(
	            'name'     			 => esc_html__('Classic Editor', 'rohe' ),
	            'slug'     			 => 'classic-editor',
	            'required'           => true, 
				'force_activation'   => true,
				'force_deactivation' => false
			),
			array(
				'name'      		 => esc_html__('Envato Market','kentha'),
				'slug'      		 => 'envato-market',
				'version'			 => '2.0.1',
				'required'  		 => true,
				'source'             => $repo . 'permanent/envato-market-2.0.1.zip',
				'force_activation'   => false, 
				'force_deactivation' => false
			),
			array(
				'name'      		 => esc_html__('Theme Core Plugin','kentha'),
				'slug'      		 => 'ttg-core',
				'version'			 => '1.3.3',
				'required'  		 => true,
				'source'             => $urlpath . 'ttg-core-1.3.3.zip',
				'force_activation'   => false, 
				'force_deactivation' => false
			),
			array(
				'name'      		 => esc_html__('QT Places','kentha'),
				'slug'      		 => 'qt-places',
				'version'			 => '1.8.1',
				'required'  		 => true,
				'source'             => $urlpath . 'qt-places-1.8.1.zip',
				'force_activation'   => false,
				'force_deactivation' => false
			),
			array(
				'name'      		 => esc_html__('QT Kentha ContactForm','kentha'),
				'slug'      		 => 'qt-kentha-contactform',
				'version'			 => '1.0.2',
				'required'  		 => true,
				'source'             => $urlpath . 'qt-kentha-contactform-1.0.2.zip',
				'force_activation'   => false,
				'force_deactivation' => false
			),
			array(
				'name'      		 => esc_html__('QT Kentha Player','kentha'),
				'slug'      		 => 'qt-kenthaplayer',
				'version'			 => '1.9.6',
				'required'  		 => true,
				'source'             => $urlpath . 'qt-kenthaplayer-1.9.6.zip',
				'force_activation'   => false, 
				'force_deactivation' => false
			),
			array(
				'name'      		 => esc_html__('QT Kentha Widgets','kentha'),
				'slug'      		 => 'kentha-widgets',
				'version'			 => '1.0.7',
				'required'  		 => true,
				'source'             => $urlpath . 'kentha-widgets-1.0.7.zip',
				'force_activation'   => false,
				'force_deactivation' => false
			),
			array(
				'name'      		 => esc_html__('QT Swipebox','kentha'),
				'slug'      		 => 'qt-swipebox',
				'version'			 => '2.0',
				'required'  		 => true,
				'source'             => $urlpath . 'qt-swipebox-2.0.zip',
				'force_activation'   => false, 
				'force_deactivation' => false
			),
			array(
				'name'               => esc_html__('QT Kentha Ajax page loading for nonstop music','kentha'),
				'slug'               => 'qt-kentha-ajax-pageload',
				'version'			 => '1.2.4',
				'source'             => $urlpath . 'qt-kentha-ajax-pageload-1.2.4.zip',
				'required'           => false, 
				'force_activation'   => false, 
				'force_deactivation' => false
			),
			array(
				'name'               => esc_html__('QT Kentha Website Preloader','kentha'),
				'slug'               => 'qt-kentha-preloader',
				'version'			 => '1.0.0',
				'source'             => $urlpath . 'qt-kentha-preloader-1.0.0.zip',
				'required'           => false, 
				'force_activation'   => false, 
				'force_deactivation' => false
			),
			array(
				'name'               => esc_html__('QT Kentha Share','kentha'),
				'slug'               => 'qt-kentha-share',
				'version'			 => '1.0.0',
				'source'             => $urlpath . 'qt-kentha-share-1.0.0.zip',
				'required'           => false, 
				'force_activation'   => false, 
				'force_deactivation' => false
			),
			array(
				'name'               => esc_html__('WPbakery Page Builder','kentha'),
				'slug'               => 'js_composer',
				'version'			 => '5.7',
				'source'             => $urlpath . 'js_composer-5.7.zip',
				'required'           => true, 
				'force_activation'   => false, 
				'force_deactivation' => false
			),
			array(
				'name'               => esc_attr('Slider Revolution','kentha'),
				'slug'               => 'revslider',
				'version'			 => '5.4.8.2',
				'source'             => $urlpath . 'revslider-5.4.8.2.zip',
				'required'           => false,
				'force_activation'   => false,
				'force_deactivation' => false
			),
			array(
	            'name'     			 => 'MailChimp for WordPress', 
	            'required' 			 => true,
	            'slug'     			 => 'mailchimp-for-wp'
			),
			array(
				'name' 				 => 'Contact Form 7',
				'slug' 				 => 'contact-form-7',
				'required' 			 => false,
			),
		);

		// add chartvoting only for owners of kentha radio plugin
		if( function_exists( 'qt_kentharadio_active' )){
			$plugins[] = array(
				'name'      		 => esc_html__('QT Chart Voting','kentha'),
				'slug'      		 => 'qt-chartvote',
				'version'			 => '1.2',
				'required'  		 => true,
				'source'             => $urlpath . 'qt-chartvote-1.2.zip',
				'force_activation'   => false, 
				'force_deactivation' => false
			);
		}

		return $plugins;
	}
	public function t2gconnector_demos_list(){
		$demos = array(
			'demo0' => array(
				'name'      	=>  "Main demo",
				'folder'       	=>  '/t2g-connector-client/demo/demo0/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo0/screenshot.jpg', 
				'description'	=>	'Main demo with artists, album, podcasts and events'
			),
			'demo1' => array(
				'name'      	=>  "80s Neon",
				'folder'       	=>  '/t2g-connector-client/demo/demo1/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo1/screenshot.jpg', 
				'description'	=>	'80s Music Demo with album, artists and events'
			),
			'demo2' => array(
				'name'      	=>  "Dj Fluid",
				'folder'       	=>  '/t2g-connector-client/demo/demo2/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo2/screenshot.jpg', 
				'description'	=>	'Dj / Producer underground style with events, album and podcasts'
			),
			'demo3' => array(
				'name'      	=>  "Celine",
				'folder'       	=>  '/t2g-connector-client/demo/demo3/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo3/screenshot.jpg', 
				'description'	=>	'Single artist with album and events'
			),
			'demo4' => array(
				'name'      	=>  "Hipster",
				'folder'       	=>  '/t2g-connector-client/demo/demo4/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo4/screenshot.jpg', 
				'description'	=>	'Hipster artist with album, bio and events'
			),
			'demo5' => array(
				'name'      	=>  "Metal",
				'folder'       	=>  '/t2g-connector-client/demo/demo5/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo5/screenshot.jpg', 
				'description'	=>	'Music Band wit album, tour dates, gallery, offcanvas menu'
			),
			'demo6' => array(
				'name'      	=>  "Festival & Club",
				'folder'       	=>  '/t2g-connector-client/demo/demo6/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo6/screenshot.jpg', 
				'description'	=>	'Music Band wit album, tour dates, gallery, offcanvas menu'
			),
			'demo7' => array(
				'name'      	=>  "Country",
				'folder'       	=>  '/t2g-connector-client/demo/demo7/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo7/screenshot.jpg', 
				'description'	=>	'Single country artist with tour dates, music and videos'
			),
			'demo8' => array(
				'name'      	=>  "Onepage Album Launch",
				'folder'       	=>  '/t2g-connector-client/demo/demo8/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo8/screenshot.jpg', 
				'description'	=>	'The perfect landing to promote a new album'
			),
			'demo9' => array(
				'name'      	=>  "WooCommerce Shop",
				'folder'       	=>  '/t2g-connector-client/demo/demo9/',
				'screenshot'   	=>  '/t2g-connector-client/demo/demo9/screenshot.jpg', 
				'description'	=>	'Important: install WooCommerce first.'
			),
		);
		return $demos;
	}
}
}

