<?php 
/**
 * Require QT Kentha Contactform Plugin
 */
if(shortcode_exists( 'qt-kentha-contactform' )){
	echo do_shortcode('[qt-kentha-contactform]');
}
