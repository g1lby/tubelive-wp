<div id="qtSharelayer" class="qt-sharelayer">
	<div class="qt-sharecont">
		<a href="#" target="_blank" data-sharetype-facebook class="qt-facebook"><span class="socicon-facebook"></span> Facebook</a>
		<a href="#" target="_blank" data-sharetype-twitter class="qt-twitter"><span class="socicon-twitter"></span> Twitter</a>
		<a href="#" target="_blank" data-sharetype-pinterest class="qt-pinterest"><span class="socicon-pinterest"></span> Pinterest</a>
		<a href="#" target="_blank" data-sharetype-gplus class="qt-gplus"><span class="socicon-googleplus"></span> Google+</a>
	</div>
	<i class="material-icons qt-close">close</i>
</div>