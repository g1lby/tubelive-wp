<?php global $product; ?>
<article <?php post_class('qt-part-archive-item qt-grid-item qt-part-archive-item-grid qt-paper'); ?>>
	<a href="<?php the_permalink(); ?>" class="qt-prod-thumb" <?php if(has_post_thumbnail()){ ?><?php } ?>  data-bgimage="<?php echo get_the_post_thumbnail_url(null,'medium'); ?>" data-parallax="0" data-attachment="local">
	<?php if(has_post_thumbnail()){ ?>
			<?php the_post_thumbnail('kentha-squared'); ?>
	<?php } ?>
	</a>

	<header class="qt-headings">
		<h4 class="qt-center"><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h4>
	</header>
	<div class="qt-summary">
		<h4 class="qt-item-metas qt-center">
			<?php if ( $price_html = $product->get_price_html() ) : ?><span class="price"><?php echo $price_html; ?></span> <?php endif; ?> 
		</h4>
	</div>
	<footer class="qt-center">
		<?php  
		if( $product->is_type( 'simple' ) ){
			$prodid = get_the_id();
			$buylink = add_query_arg("add-to-cart", $prodid, get_the_permalink());
		    ?>
		    <a href="<?php echo esc_url($buylink); ?>" data-quantity="1" data-product_id="<?php echo esc_attr($prodid); ?>" class="qt-btn qt-btn-primary qt-btn-l qt-cart product_type_simple add_to_cart_button ajax_add_to_cart">		<?php esc_attr_e( 'Add to cart', 'kentha' ); ?>
		    </a>
		  	<?php
		} elseif( $product->is_type( 'variable' ) ){
		   // Product has variations
		   	?>
		    <a href="<?php the_permalink(); ?>" class="qt-btn qt-btn-primary qt-btn-l"><?php esc_attr_e( 'View details', 'kentha' ); ?></a>
		  	<?php
		} else {
		 	echo apply_filters( 'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
			sprintf( '<a href="%s" data-quantity="%s" class="%s" %s>%s</a>',
				esc_url( $product->add_to_cart_url() ),
				1,
				'qt-btn qt-btn-primary qt-btn-l',
				'',
				esc_html( $product->add_to_cart_text() )
			),
			$product, array() );
		 }
		?>
	</footer>
</article>