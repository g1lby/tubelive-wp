<?php die(); ?><!doctype html>
<html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns#">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
		<title>- TubeLive</title>

<!-- This site is optimized with the Yoast SEO plugin v10.0.1 - https://yoast.com/wordpress/plugins/seo/ -->
<!-- Admin only notice: this page does not show a meta description because it does not have one, either write it for this page specifically or go into the [SEO - Search Appearance] menu and set up a template. -->
<link rel="canonical" href="https://dev.tubelive.de/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="- TubeLive" />
<meta property="og:url" content="https://dev.tubelive.de/" />
<meta property="og:site_name" content="TubeLive" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="- TubeLive" />
<script type='application/ld+json'>{"@context":"https://schema.org","@type":"WebSite","@id":"https://dev.tubelive.de/#website","url":"https://dev.tubelive.de/","name":"TubeLive","potentialAction":{"@type":"SearchAction","target":"https://dev.tubelive.de/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//maps.googleapis.com' />
<link rel='dns-prefetch' href='//www.google.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="TubeLive &raquo; Feed" href="https://dev.tubelive.de/feed/" />
<link rel="alternate" type="application/rss+xml" title="TubeLive &raquo; Comments Feed" href="https://dev.tubelive.de/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/dev.tubelive.de\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.1"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='dashicons-css'  href='https://dev.tubelive.de/wp-includes/css/dashicons.min.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='https://dev.tubelive.de/wp-includes/css/admin-bar.min.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='dripicons-css'  href='https://dev.tubelive.de/wp-content/plugins/qt-chartvote/dripicons/webfont.css?ver=1.2'  media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='https://dev.tubelive.de/wp-includes/css/dist/block-library/style.min.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://dev.tubelive.de/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='qt_kentharadio_style-css'  href='https://dev.tubelive.de/wp-content/plugins/qt-kentharadio/assets/css/qt-kentharadio-style.css?ver=1.4'  media='all' />
<link rel='stylesheet' id='qtPlacesStyle-css'  href='https://dev.tubelive.de/wp-content/plugins/qt-places/inc/frontend/assets/styles.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='https://dev.tubelive.de/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css?ver=5.7'  media='all' />
<link rel='stylesheet' id='QtswipeStyle-css'  href='https://dev.tubelive.de/wp-content/plugins/qt-swipebox/swipebox/css/swipebox.min.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='https://dev.tubelive.de/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8.2'  media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='kentha-style-css'  href='https://dev.tubelive.de/wp-content/themes/kentha/style.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='kentha-child-style-css'  href='https://dev.tubelive.de/wp-content/themes/kentha-child/style.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='duplicate-post-css'  href='https://dev.tubelive.de/wp-content/plugins/duplicate-post/duplicate-post.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='yoast-seo-adminbar-css'  href='https://dev.tubelive.de/wp-content/plugins/wordpress-seo/css/dist/adminbar-1001.min.css?ver=10.0.1'  media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='https://dev.tubelive.de/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.7'  media='all' />
<link rel='stylesheet' id='kirki_google_fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3Aregular%2C700%7CMontserrat%3A700&subset=latin-ext%2Clatin-ext%2Clatin-ext%2Clatin-ext%2Clatin-ext%2Clatin-ext%2Clatin-ext%2Clatin-ext%2Clatin-ext%2Clatin-ext'  media='all' />
<link rel='stylesheet' id='kirki-styles-kentha_config-css'  href='https://dev.tubelive.de/wp-content/plugins/qt-kentharadio/inc/backend/customizer/kirki/assets/css/kirki-styles.css'  media='all' />
<style id='kirki-styles-kentha_config-inline-css' type='text/css'>
body, html, .qt-side-nav.qt-menu-offc ul{font-family:"Open Sans", Helvetica, Arial, sans-serif;font-weight:400;font-style:normal;}p strong, li strong, table strong{font-family:"Open Sans", Helvetica, Arial, sans-serif;font-weight:700;font-style:normal;}h1, h2, h3, h4, h5, h6, .qt-txtfx {font-family:Montserrat, Helvetica, Arial, sans-serif;font-weight:700;font-style:normal;letter-spacing:-0.03em;text-transform:uppercase;}.qt-logo-link{font-family:Montserrat, Helvetica, Arial, sans-serif;font-weight:700;font-style:normal;letter-spacing:-0.03em;text-transform:uppercase;}.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .qt-desktopmenu, .qt-side-nav, .qt-menu-footer, .qt-capfont, .qt-details, .qt-capfont-thin, .qt-btn, a.qt-btn,.qt-capfont strong, .qt-item-metas, button, input[type="button"], .button{font-family:Montserrat, Helvetica, Arial, sans-serif;font-weight:400;font-style:normal;letter-spacing:0.03em;text-transform:uppercase;}
</style>
<link rel='stylesheet' id='mediaelement-css'  href='https://dev.tubelive.de/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.6-78496d1'  media='all' />
<link rel='stylesheet' id='wp-mediaelement-css'  href='https://dev.tubelive.de/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=5.1.1'  media='all' />
<link rel='stylesheet' id='qt-socicon-css'  href='https://dev.tubelive.de/wp-content/themes/kentha/fonts/qt-socicon/qt-socicon.css?ver=1.5.6'  media='all' />
<link rel='stylesheet' id='material-icons-css'  href='https://dev.tubelive.de/wp-content/themes/kentha/fonts/google-icons/material-icons.css?ver=1.5.6'  media='all' />
<link rel='stylesheet' id='kentha-qt-main-min-css'  href='https://dev.tubelive.de/wp-content/themes/kentha/css/qt-main-min.css?ver=1.5.6'  media='all' />
<link rel='stylesheet' id='kentha-qt-desktop-min-css'  href='https://dev.tubelive.de/wp-content/themes/kentha/css/qt-desktop-min.css?ver=1.5.6'  media='only screen and (min-width: 1201px)' />
<link rel='stylesheet' id='vc_tta_style-css'  href='https://dev.tubelive.de/wp-content/plugins/js_composer/assets/css/js_composer_tta.min.css?ver=5.7'  media='all' />
<link rel='stylesheet' id='animate-css-css'  href='https://dev.tubelive.de/wp-content/plugins/js_composer/assets/lib/bower/animate-css/animate.min.css?ver=5.7'  media='all' />
<script  src='https://dev.tubelive.de/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script  src='https://dev.tubelive.de/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script >
var mejsL10n = {"language":"en","strings":{"mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Turn off Fullscreen","mejs.fullscreen-on":"Go Fullscreen","mejs.download-video":"Download Video","mejs.fullscreen":"Fullscreen","mejs.time-jump-forward":["Jump forward 1 second","Jump forward %1 seconds"],"mejs.loop":"Toggle Loop","mejs.play":"Play","mejs.pause":"Pause","mejs.close":"Close","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.time-skip-back":["Skip back 1 second","Skip back %1 seconds"],"mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.mute-toggle":"Mute Toggle","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Skip ad","mejs.ad-skip-info":["Skip in 1 second","Skip in %1 seconds"],"mejs.source-chooser":"Source Chooser","mejs.stop":"Stop","mejs.speed-rate":"Speed Rate","mejs.live-broadcast":"Live Broadcast","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
</script>
<script  src='https://dev.tubelive.de/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1'></script>
<script  src='https://dev.tubelive.de/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.1.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
/* ]]> */
</script>
<script  src='https://maps.googleapis.com/maps/api/js?ver=5.1.1'></script>
<script  src='https://www.google.com/jsapi?ver=5.1.1'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.8.2'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8.2'></script>
<link rel='https://api.w.org/' href='https://dev.tubelive.de/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://dev.tubelive.de/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://dev.tubelive.de/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.1.1" />
<link rel='shortlink' href='https://dev.tubelive.de/' />
<link rel="alternate" type="application/json+oembed" href="https://dev.tubelive.de/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdev.tubelive.de%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://dev.tubelive.de/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdev.tubelive.de%2F&#038;format=xml" />
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://dev.tubelive.de/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><style type="text/css" media="print">#wpadminbar { display:none; }</style>
	<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
	<meta name="generator" content="Powered by Slider Revolution 5.4.8.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">function setREVStartSize(e){									
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
						}catch(d){console.log("Failure at Presize of Slider:"+d)}						
					};</script>
		<style type="text/css" id="wp-custom-css">
			.qt-footer-bottom {
    padding: 0rem 0 8rem 0;
}

.qt-part-share {display:none}


.qt-menu-footer, .qt-footer-bottom .qt-copyright-text {
	text-align: left !important;
}
/* Spacer unter Header entfernt */
.qt-pageheader-std.qt-negative{
	display: none
}		</style>
		
	<!-- THEME CUSTOMIZER SETTINGS INFO   ================================ -->

	<!-- qt_primary_color: #454955 -->
	<!-- qt_primary_color_light: #565c68 -->
	<!-- qt_primary_color_dark: #101010 -->
	<!-- qt_color_accent: #00ced0 -->
	<!-- qt_color_accent_hover: #00fcff -->
	<!-- qt_color_secondary: #ff0d51 -->
	<!-- qt_color_secondary_hover: #c60038 -->
	<!-- qt_color_background: #444 -->
	<!-- qt_color_paper: #131617 -->
	<!-- qt_textcolor_original: #fff -->

	<!-- ===================================================== -->

	<!-- QT STYLES DYNAMIC CUSTOMIZATIONS ========================= -->
	<style>
	 body, html,.qt-nclinks a,.qt-btn.qt-btn-ghost,.qt-paper h1,.qt-paper h2,.qt-paper h3,.qt-paper h4,.qt-paper h5,.qt-paper h6,.qt-paper{color: rgba(255,255,255,0.87);}.qt-slickslider-outercontainer .slick-dots li button{background: rgba(255,255,255,0.87);} a{color: #00ced0;} a:hover{color: #00fcff;} h1, h2, h3, h4, h5, h6{color: rgba(255,255,255,1);}.dropdown-content li > a,.dropdown-content li > span,.qt-part-archive-item.qt-open .qt-headings .qt-tags a,.qt-link-sec,.qt-caption-small::after,.qt-content-aside a:not(.qt-btn), nav .qt-menu-secondary li a:hover,.qt-menu-secondary li.qt-soc-count a{color: #ff0d51;}.qt-content-secondary,.qt-content-accent,.qt-btn-secondary,.qt-btn-primary,.qt-content-aside .qt-btn-primary ,.qt-content-aside .qt-btn-secondary,.qt-content-aside .tagcloud a,.qt-content-aside a.qt-btn-primary:hover,.qt-content-aside a.qt-btn-secondary:hover, input[type="submit"],.qt-part-archive-item.qt-open .qt-capseparator{color: #fff;}.qt-content-primary,.qt-content-primary-dark,.qt-content-primary-light,.qt-content-primary-dark a,.tabs .tab a,.qt-mobilemenu a.qt-openthis,.qt-mobilemenu .qt-logo-text h2, li.qt-social-linkicon a i, [class*=qt-content-primary] h1, [class*=qt-content-primary] h2, [class*=qt-content-primary] h3, [class*=qt-content-primary] h4, [class*=qt-content-primary] h5, [class*=qt-content-primary] h6{color: #fff;}.qt-part-archive-item.qt-open .qt-iteminner .qt-header .qt-capseparator{border-color: #fff; background: #fff;} body.skrollable-between nav.qt-menubar ul.qt-desktopmenu > li > a, body.skrollable-between nav.qt-menubar .qt-menu-secondary li a, body.skrollable-between .qt-breadcrumb,.qt-breadcrumb a{color: #fff !important;}.qt-desktopmenu-notscr .qt-morphbtn i span{border-color: #fff !important;}.tabs .tab a:hover{color: #c60038;}.qt-content-secondary{color: #fff;} body, html,.qt-main{ background-color: #444;}.qt-paper{background-color: #131617;}.qt-content-primary-light{background-color: #565c68;}.qt-content-primary{background-color: #454955;}.qt-content-primary-dark,.is_ipad .qt-parentcontainer .qt-menubar{background-color: #101010;}.qt-content-accent,.qt-menubar ul.qt-desktopmenu li li::after, input[type="submit"]{background-color: #00ced0;}.qt-content-secondary,.qt-material-slider .indicator-item.active,.tabs .indicator,.qt-mobilemenu ul.qt-side-nav li.menu-item-has-children a.qt-openthis,.qt-capseparator{background-color: #ff0d51;}.qt-tags a,.tagcloud a{background-color: #ff0d51;}.qt-part-archive-item.qt-open .qt-headings,.qt-btn-secondary,.qt-sectiontitle::after, h2.widgettitle::after{background-color: #ff0d51;}.qt-btn-primary ,.qt-menubar ul.qt-desktopmenu > li.qt-menuitem > a::after,.qt-widgets .qt-widget-title::after, input[type="submit"]{background-color:#00ced0;} input:not([type]):focus:not([readonly]), input[type=text]:focus:not([readonly]), input[type=password]:focus:not([readonly]), input[type=email]:focus:not([readonly]), input[type=url]:focus:not([readonly]), input[type=time]:focus:not([readonly]), input[type=date]:focus:not([readonly]), input[type=datetime]:focus:not([readonly]), input[type=datetime-local]:focus:not([readonly]), input[type=tel]:focus:not([readonly]), input[type=number]:focus:not([readonly]), input[type=search]:focus:not([readonly]), textarea.materialize-textarea:focus:not([readonly]){border-color:#00ced0;} ul.qt-side-nav.qt-menu-offc>li>a:not(.qt-openthis)::after,.qt-slickslider-outercontainer__center .qt-carouselcontrols i.qt-arr,.qt-btn.qt-btn-ghost,.qt-capseparator{border-color:#ff0d51;} input:not([type]):focus:not([readonly])+label, input[type=text]:focus:not([readonly])+label, input[type=password]:focus:not([readonly])+label, input[type=email]:focus:not([readonly])+label, input[type=url]:focus:not([readonly])+label, input[type=time]:focus:not([readonly])+label, input[type=date]:focus:not([readonly])+label, input[type=datetime]:focus:not([readonly])+label, input[type=datetime-local]:focus:not([readonly])+label, input[type=tel]:focus:not([readonly])+label, input[type=number]:focus:not([readonly])+label, input[type=search]:focus:not([readonly])+label, textarea.materialize-textarea:focus:not([readonly])+label{color: #00ced0;} @media only screen and (min-width: 1201px){a:hover, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover,.qt-btn.qt-btn-ghost:hover{color:#00fcff;}.qt-content-aside a:hover,.qt-btn.qt-btn-ghost:hover{color: #c60038;}.qt-btn-primary:hover, input[type="submit"]:hover{background-color:#00fcff; color: #fff;}.qt-btn-secondary:hover,.qt-tags a:hover,.qt-mplayer:hover .qt-mplayer__play:hover,.qt-mplayer__volume:hover,.qt-mplayer__cart:hover,.qt-mplayer__prev:hover,.qt-mplayer__next:hover,.qt-mplayer__playlistbtn:hover{background-color: #c60038; color: #fff;} } </style> 
<!-- QT STYLES DYNAMIC CUSTOMIZATIONS END ========= -->


<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>	</head>
	<body id="qtBody" class="qt-body is_osx qt-template- qt-user-logged qt-body-menu-center qt-intro wpb-js-composer js-comp-ver-5.7 vc_responsive" data-start>
			<div class="qt-bodybg qt-bottomlayer-bg" data-bgimage="https://dev.tubelive.de/wp-content/uploads/2019/03/background-1600x900.jpg" data-parallax="0"></div>
			
				<div id="qt-mobile-menu" class="side-nav qt-content-primary">
			<ul class="qt-side-nav  qt-dropdown-menu">
	<li>
		<span class="qt-closesidenav">
			<i class='material-icons'>close</i> Close		</span>
	</li>
				<li class="qt-social-linkicon">
				<a href="http://whatsapp.tubelive.de" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-whatsapp qt-socialicon"></i>
				</a>
			</li>
					<li class="qt-social-linkicon">
				<a href="http://twitter.com/TubeLiveDE" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-twitter qt-socialicon"></i>
				</a>
			</li>
					<li class="qt-social-linkicon">
				<a href="http://instagram.com/TubeLiveDE" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-instagram qt-socialicon"></i>
				</a>
			</li>
					<li class="qt-social-linkicon">
				<a href="http://facebook.com/TubeLiveDE" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-facebook qt-socialicon"></i>
				</a>
			</li>
			<li class="qt-clearfix">
	</li>
	<li id="menu-item-74" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-23 current_page_item menu-item-has-children menu-item-74 qt-menuitem"><a href="https://dev.tubelive.de/" aria-current="page">START</a>
<ul class="sub-menu">
	<li id="menu-item-75" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75 qt-menuitem"><a href="https://dev.tubelive.de/news/">NEWS</a></li>
</ul>
</li>
<li id="menu-item-76" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-76 qt-menuitem"><a href="https://dev.tubelive.de/sendungen/">TubeLive</a>
<ul class="sub-menu">
	<li id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77 qt-menuitem"><a href="https://dev.tubelive.de/sendungen/">SHOWS</a></li>
	<li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78 qt-menuitem"><a href="https://dev.tubelive.de/empfang/">EMPFANG</a></li>
</ul>
</li>
<li id="menu-item-80" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-80 qt-menuitem"><a href="https://dev.tubelive.de/kontakt/">KONTAKT</a>
<ul class="sub-menu">
	<li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79 qt-menuitem"><a href="https://dev.tubelive.de/whatsapp/">TubeLive AUF WHATSAPP</a></li>
</ul>
</li>
</ul>		</div>

		
		<div id="qtMasterContainter" class="qt-parentcontainer">

			<nav class="qt-menubar qt-menubar-center nav-wrapper hide-on-xl-and-down" >
		<div class="qt-content-primary-dark qt-menubg-color">
			</div>
		<div class="qt-container-l">
		<ul class="qt-menu-secondary">
			<li class="qt-3dswitch">
							</li>
			<li class="qt-centerlogo">
				<span>
					<a href="https://dev.tubelive.de/" class="qt-logo-link qt-fontsize-h3"><img src="https://dev.tubelive.de/wp-content/uploads/2019/03/logo_header.png" alt="Home"></a>
				</span>
			</li>
						<li class="qt-soc-count qt-social-linkicon">
				<a href="http://whatsapp.tubelive.de" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-whatsapp qt-socialicon"></i>
								</a>
			</li>
					<li class="qt-soc-count qt-social-linkicon">
				<a href="http://twitter.com/TubeLiveDE" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-twitter qt-socialicon"></i>
								</a>
			</li>
					<li class="qt-soc-count qt-social-linkicon">
				<a href="http://instagram.com/TubeLiveDE" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-instagram qt-socialicon"></i>
								</a>
			</li>
					<li class="qt-soc-count qt-social-linkicon">
				<a href="http://facebook.com/TubeLiveDE" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-facebook qt-socialicon"></i>
								</a>
			</li>
				</ul>
	</div>
			<ul class="qt-desktopmenu">
		<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-23 current_page_item menu-item-has-children menu-item-74 qt-menuitem"><a href="https://dev.tubelive.de/" aria-current="page">START</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75 qt-menuitem"><a href="https://dev.tubelive.de/news/">NEWS</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-76 qt-menuitem"><a href="https://dev.tubelive.de/sendungen/">TubeLive</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77 qt-menuitem"><a href="https://dev.tubelive.de/sendungen/">SHOWS</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78 qt-menuitem"><a href="https://dev.tubelive.de/empfang/">EMPFANG</a></li>
</ul>
</li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-80 qt-menuitem"><a href="https://dev.tubelive.de/kontakt/">KONTAKT</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79 qt-menuitem"><a href="https://dev.tubelive.de/whatsapp/">TubeLive AUF WHATSAPP</a></li>
</ul>
</li>
		</ul>
				<div class="qt-container-l">
		<ul id="qtBreadcrumb" class="qt-breadcrumb qt-item-metas qt-clearfix">

	</ul>

	</div>
	</nav>
						<div id="qt-mob-navbar" class="qt-mobilemenu hide-on-xl-only qt-center qt-content-primary">
				<span id="qwMenuToggle" data-activates="qt-mobile-menu" class="qt-menuswitch button-collapse qt-btn qt-btn-xl left">
	<i class='material-icons'>
	  menu
	</i>
</span>
<a class="qt-logo-text" href="https://dev.tubelive.de/"><img src="https://dev.tubelive.de/wp-content/uploads/2019/03/logo_header.png" alt="Home"></a>
			</div>
			<!-- ======================= MAIN SECTION  ======================= -->
<div id="maincontent">
	<div class="qt-main qt-clearfix qt-3dfx-content">
					<div id="qtPageBg"  >
					<div class="qt-pagebg-in" data-bgimage="https://dev.tubelive.de/wp-content/uploads/2019/03/background-1600x900.jpg"></div>
					<div class="qt-darken-bg-30"></div>
	</div>
		<div id="qtarticle" class="qt-container qt-main-contents post-23 page type-page status-publish hentry">
			<div class="qt-pageheader-std qt-negative">
				<hr class="qt-spacer-m">
				<h1 class="qt-caption"></h1>
				<hr class="qt-capseparator">
			</div>
						<div class="qt-the-content qt-paper qt-paddedcontent  qt-card">
				<div class="qt-the-content">
					<div class="qt-vc-row-container"><div class="vc_row wpb_row vc_row-kentha"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  qt-the-content" >
		<div class="wpb_wrapper">
			<h4 style="text-align: center;">WILLKOMMEN BEI TUBELIVE &#8211; DEINEM TUBERADIO <span class="emoji">😍</span>!<br />
#GÖNNDIR Deine Musik.</h4>
<div class="row">
<div class="col-lg-12 ml-auto">
<p class="lead" style="text-align: center;">Wir spielen die Songs <strong>Deiner</strong> Onlinestars ?, interviewen <strong>Deine</strong> Idole <span class="emoji">?️</span>, machen Verlosungen <span class="emoji">?</span> und vieles mehr!<br />
Und das beste: <strong>Du</strong> kannst das Programm mitgestalten ?! Hör doch mal rein <span class="emoji">?.</span></p>
</div>
</div>

		</div>
	</div>
</div></div></div></div></div><div class="qt-vc-row-container"><div class="vc_row wpb_row vc_row-kentha"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">
		<!-- POSTS CAROUSEL ================================================== -->	
		<div class="qt-container qt-relative">
			<div class="qt-slickslider-outercontainer qt-slickslider-outercontainer__center qt-relative">
				<div class="row">
					<div class="col s9 m8">
													<h3 class="qt-sectiontitle ">NEWS</h3>
											</div>
									</div>
				<div class="row">
					<div class="qt-slickslider-container qt-slickslider-cards">
						<div class="qt-slickslider qt-invisible qt-animated qt-slickslider-multiple" data-slidestoshow="3" data-slidestoscroll="1" data-variablewidth="false" data-arrows="false" data-dots="true" data-infinite="true" data-centermode="false" data-pauseonhover="true" data-autoplay="false" data-arrowsmobile="false"  data-centermodemobile="false" data-dotsmobile="false"  data-slidestoshowmobile="1" data-variablewidthmobile="true" data-infinitemobile="false" data-slidestoshowipad="3">
														<div class="qt-item qt-item-card col s12 m4">
								<article class="qt-part-archive-item qt-post qt-carditem qt-card qt-paper qt-interactivecard qt-scrollbarstyle post-1 post type-post status-publish format-standard hentry category-uncategorized">
	<div class="qt-iteminner">
		<div class="qt-imagelink" data-activatecard>
			<span class="qt-header-bg" data-bgimage="" data-parallax="0" data-attachment="local">
			</span>
		</div>
		<header class="qt-header">
			<div class="qt-headings" data-activatecard>
				<span class="qt-tags">
					<a href="https://dev.tubelive.de/category/uncategorized/">Uncategorized</a>				</span>
				<h3 class="qt-title">Hello world!</h3>
				<span class="qt-details qt-item-metas">
					admin | 30/03/2019				</span>
				<span class="qt-capseparator"></span>
				<i class="material-icons qt-close">close</i>
			</div>

			<div class="qt-actionbtn fixed-action-btn horizontal click-to-toggle">
				<a href="https://dev.tubelive.de/uncategorized/hello-world/" class="btn-floating btn-large qt-btn-primary">
					<i class="material-icons">add</i>
				</a>
			</div>
			<i class="material-icons qt-ho" data-activatecard>keyboard_arrow_down</i>
		</header>
		<span data-color="#ff0d51" class="qt-animation"></span>
		<div class="qt-content">
			<div class="qt-summary">
				<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>
			</div>
			<footer class="qt-item-metas">
				<a href="https://dev.tubelive.de/uncategorized/hello-world/">Read more <i class='material-icons'>arrow_forward</i></a>
			</footer>
		</div>
	</div>
</article>							</div>
													</div>
											</div>
				</div>
			</div>
		</div>
		<!--  POSTS CAROUSEL END ================================================== -->
		</div></div></div></div></div><div class="qt-vc-row-container"><div class="vc_row wpb_row vc_row-kentha"><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_sep_color_grey vc_separator-has-text" ><span class="vc_sep_holder vc_sep_holder_l"><span  class="vc_sep_line"></span></span><h4>WERBUNG</h4><span class="vc_sep_holder vc_sep_holder_r"><span  class="vc_sep_line"></span></span>
</div>
	<div class="wpb_raw_code wpb_content_element wpb_raw_html" >
		<div class="wpb_wrapper">
			<p style="text-align: center;"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- tubelive.de - ad#4 - 336x280 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-8109968342918740"
     data-ad-slot="7501242380"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></p>
		</div>
	</div>
<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey" ><span class="vc_sep_holder vc_sep_holder_l"><span  class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span  class="vc_sep_line"></span></span>
</div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner"><div class="wpb_wrapper"><a id="vc_gid:1553976389679-c6b9f3a9-779e-6-52" class="qt-kentharadio-show qt-autorefresh qt-negative" href="https://dev.tubelive.de/shows/deinabend/">
		<div class="qt-kentharadio-show__header">
		<div class="qt-kentharadio-vc">
			<div class="qt-kentharadio-vi">
				<h5 class="qt-caption-med qt-capfont hide-on-small-and-down">
		  								<span>
						Now On Air					</span>
							  		</h5>				
				<h2 class="qt-kentharadio-nmt qt-fontsize-h0">TubeLive am Abend</h2>
								<h4 class="qt-kentharadio-nmt">
					#GÖNNDIR Deine Musik am Abend				</h4>
								<h5 class="qt-kentharadio-nmt qt-item-metas">6:00 pm<i class='material-icons'>keyboard_arrow_right</i>12:00 am</h5>
			</div>
		</div>
	</div>
</a></div></div></div></div></div>
					<hr class="qt-clearfix">
									</div>
			</div>
			

			<hr class="qt-spacer-l">
		</div>
			</div>
</div>
<!-- ======================= MAIN SECTION END ======================= -->

			
		<div class="qt-footercontainer qt-content-primary-light">
						<div class="qt-footer-bottom qt-content-primary qt-content-aside qt-negative">
		    	<div class="qt-container-l">
		    		<div class="row">
						<div class="col s12 m12 l6">
							<span class="qt-mplayer__btnspacer qt-btn qt-btn-xl hide-on-med-and-down"><i></i></span>							<ul class="qt-menu-social">
											<li class="qt-social-linkicon">
				<a href="http://whatsapp.tubelive.de" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-whatsapp qt-socialicon"></i>
				</a>
			</li>
					<li class="qt-social-linkicon">
				<a href="http://twitter.com/TubeLiveDE" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-twitter qt-socialicon"></i>
				</a>
			</li>
					<li class="qt-social-linkicon">
				<a href="http://instagram.com/TubeLiveDE" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-instagram qt-socialicon"></i>
				</a>
			</li>
					<li class="qt-social-linkicon">
				<a href="http://facebook.com/TubeLiveDE" class="qw-disableembedding qw_social" target="_blank"><i class="qt-socicon-facebook qt-socialicon"></i>
				</a>
			</li>
								    </ul>
						</div>
						<div class="col s12 m12 l6">
						    <h5 class="qt-copyright-text">© 2019 TubeLive - #GÖNNDIR Deine Musik.</h5>
							<ul class="qt-menu-footer qt-small">
								<li id="menu-item-82" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82"><a href="https://dev.tubelive.de/nutzungsbedingungen/">NUTZUNGSBEDINGUNGEN</a></li>
<li id="menu-item-84" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-84"><a href="https://dev.tubelive.de/datenschutz/">DATENSCHUTZERKLÄRUNG</a></li>
<li id="menu-item-83" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-83"><a href="https://dev.tubelive.de/impressum/">IMPRESSUM</a></li>
							</ul>
						</div>
		    		</div>
		    	</div>
		    </div>
		</div>
	</div>

		<div id="qtmusicplayer" class="qt-mplayer qt-scrollbarstyle qt-hidden" data-showplayer="" data-analyzer="" data-autoplay="" data-hiquality="" data-qtmusicplayer-api="https://dev.tubelive.de/?kenthaplayer_json_data=" data-qtmusicplayer-smflash="https://dev.tubelive.de/wp-content/plugins/qt-kenthaplayer/assets/soundmanager/swf/">
				<div id="qtmusicplayer-playlistcontainer" class="qt-mplayer__playlistcontainer qt-content-primary">
			<div class="row qt-mplayer__playlistmaster">
				<div class="col s12 m4 l3">
					<div id="qtmusicplayer-cover" class="qt-mplayer__album">
						<a href="#" class="qt-btn qt-btn-ghost qt-btn-l qt-albumlink">Go to album</a>
					</div>
				</div>
				<div id="qtmusicplayer-playlist" class="qt-mplayer__playlist qt-content-primary col s12 m8 l9">
										<ul class="qt-playlist">
						



						





						




						

											</ul>
				</div>
			</div>
		</div>
				<div id="qtmusicplayer-controls"  class="qt-mplayer__controls" data-hidetimeout="1000">
			<div class="qt-mplayer__controllayer">
				<a id="qtmusicplayerPlay" class="qt-mplayer__play qt-btn-secondary">
					<i class="material-icons">play_arrow</i>
					<i id="qtmplayerNotif" class="qt-mplayer__notification qt-content-secondary"></i>
				</a>
				<span class="qt-mplayer__prev" data-control="prev">
					<i class='material-icons'>skip_previous</i>
				</span>
				<span class="qt-mplayer__next" data-control="next">
					<i class='material-icons'>skip_next</i>
				</span>
				<span id="qtmusicplayerVol" class="qt-mplayer__volume">
					<i class="material-icons">volume_down</i>
					<span id="qtmpvf" class="qt-mplayer__volfill qt-content-accent"></span><span id="qtmpvc" class="qt-mplayer__volcursor"></span>
				</span>
				<a id="qtmusicplayerCart" target="_blank" class="qt-mplayer__cart">
					<i class='material-icons'></i>
				</a>
				<a class="qt-mplayer__cover">
				</a>
				<div id="qtmusicplayerTrackControl" class="qt-mplayer__track">
					<div class="qt-mplayer__rowone">
						<span class="qt-mplayer__title"></span>
						<span id="qtmusicplayerTime" class="qt-mplayer__time"></span>
					</div>
					<div class="qt-mplayer__rowtwo">
						<span class="qt-mplayer__artist"></span>
						<span id="qtmusicplayerDuration" class="qt-mplayer__length"></span>
					</div>
					<span id="qtMplayerBuffer" class="qt-mplayer-track-adv qt-content-primary-light"></span>
					<span id="qtMplayerTadv" class="qt-mplayer-track-adv qt-content-accent"></span>
					<span id="qtMplayerMiniCue" class="qt-mplayer-track-minicue"></span>
				</div>
				<span class="qt-mplayer__playlistbtn" data-playlistopen>
					<i class='material-icons'>playlist_play</i>
				</span>
			</div>
		</div>
	</div>
		    <div id="qt-kentharadio-currentpermalink"  data-permalink="https://dev.tubelive.de/">
    	<!-- Nothing here. Used by javascript for auto refresh -->
    </div>
    <script  src='https://dev.tubelive.de/wp-includes/js/admin-bar.min.js?ver=5.1.1'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/qt-chartvote/js/jquery.cookie.js?ver=1.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var chartvote_ajax_var = {"url":"https:\/\/dev.tubelive.de\/wp-admin\/admin-ajax.php","nonce":"19c4cbd22d"};
/* ]]> */
</script>
<script  src='https://dev.tubelive.de/wp-content/plugins/qt-chartvote/js/qt-chartvote-script.js?ver=1.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/dev.tubelive.de\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
/* ]]> */
</script>
<script  src='https://dev.tubelive.de/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.1'></script>
<script  src='https://dev.tubelive.de/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script  src='https://dev.tubelive.de/wp-includes/js/masonry.min.js?ver=3.3.2'></script>
<script  src='https://dev.tubelive.de/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.1.1'></script>
<script  src='https://dev.tubelive.de/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
/* ]]> */
</script>
<script  src='https://dev.tubelive.de/wp-includes/js/wp-util.min.js?ver=5.1.1'></script>
<script  src='https://dev.tubelive.de/wp-includes/js/backbone.min.js?ver=1.2.3'></script>
<script  src='https://dev.tubelive.de/wp-includes/js/mediaelement/wp-playlist.min.js?ver=5.1.1'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min.js?ver=5.7'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/js_composer/assets/lib/bower/skrollr/dist/skrollr.min.js?ver=5.7'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.7'></script>
<script  src='https://dev.tubelive.de/wp-content/themes/kentha/js/min/qt-main-script.js?ver=1.5.6'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/qt-kenthaplayer/assets/js/min/qt-musicplayer-min.js?ver%5B0%5D=1.9.6.1'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/qt-kentharadio/assets/js/min/qt-kentharadio-script-min.js?ver=1.4'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/qt-places/inc/frontend/assets/min/script-min.js?ver=5.1.1'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/qt-swipebox/min/qt-swipebox-min.js?ver=5.1.1'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/js_composer/assets/lib/vc_accordion/vc-accordion.min.js?ver=5.7'></script>
<script  src='https://dev.tubelive.de/wp-content/plugins/js_composer/assets/lib/vc-tta-autoplay/vc-tta-autoplay.min.js?ver=5.7'></script>
<script  src='https://dev.tubelive.de/wp-includes/js/wp-embed.min.js?ver=5.1.1'></script>
		<script>	
			jQuery(document).ready(function() {			
				
				if (jQuery('#wp-admin-bar-revslider-default').length>0 && jQuery('.rev_slider_wrapper').length>0) {
					var aliases = new Array();
					jQuery('.rev_slider_wrapper').each(function() {
						aliases.push(jQuery(this).data('alias'));
					});								
					if(aliases.length>0)
						jQuery('#wp-admin-bar-revslider-default li').each(function() {
							var li = jQuery(this),
								t = jQuery.trim(li.find('.ab-item .rs-label').data('alias')); //text()
								
							if (jQuery.inArray(t,aliases)!=-1) {
							} else {
								li.remove();
							}
						});
				} else {
					jQuery('#wp-admin-bar-revslider').remove();
				}
			});
		</script>
			<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Toolbar">
				<ul id='wp-admin-bar-root-default' class="ab-top-menu"><li id='wp-admin-bar-wp-logo' class="menupop"><a class='ab-item' aria-haspopup="true" href='https://dev.tubelive.de/wp-admin/about.php'><span class="ab-icon"></span><span class="screen-reader-text">About WordPress</span></a><div class="ab-sub-wrapper"><ul id='wp-admin-bar-wp-logo-default' class="ab-submenu"><li id='wp-admin-bar-about'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/about.php'>About WordPress</a></li></ul><ul id='wp-admin-bar-wp-logo-external' class="ab-sub-secondary ab-submenu"><li id='wp-admin-bar-wporg'><a class='ab-item' href='https://wordpress.org/'>WordPress.org</a></li><li id='wp-admin-bar-documentation'><a class='ab-item' href='https://codex.wordpress.org/'>Documentation</a></li><li id='wp-admin-bar-support-forums'><a class='ab-item' href='https://wordpress.org/support/'>Support Forums</a></li><li id='wp-admin-bar-feedback'><a class='ab-item' href='https://wordpress.org/support/forum/requests-and-feedback'>Feedback</a></li></ul></div></li><li id='wp-admin-bar-site-name' class="menupop"><a class='ab-item' aria-haspopup="true" href='https://dev.tubelive.de/wp-admin/'>TubeLive</a><div class="ab-sub-wrapper"><ul id='wp-admin-bar-site-name-default' class="ab-submenu"><li id='wp-admin-bar-dashboard'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/'>Dashboard</a></li></ul><ul id='wp-admin-bar-appearance' class="ab-submenu"><li id='wp-admin-bar-themes'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/themes.php'>Themes</a></li><li id='wp-admin-bar-widgets'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/widgets.php'>Widgets</a></li><li id='wp-admin-bar-menus'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/nav-menus.php'>Menus</a></li></ul></div></li><li id='wp-admin-bar-customize' class="hide-if-no-customize"><a class='ab-item' href='https://dev.tubelive.de/wp-admin/customize.php?url=https%3A%2F%2Fdev.tubelive.de%2F'>Customize</a></li><li id='wp-admin-bar-comments'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/edit-comments.php'><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 comments awaiting moderation</span></a></li><li id='wp-admin-bar-new-content' class="menupop"><a class='ab-item' aria-haspopup="true" href='https://dev.tubelive.de/wp-admin/post-new.php'><span class="ab-icon"></span><span class="ab-label">New</span></a><div class="ab-sub-wrapper"><ul id='wp-admin-bar-new-content-default' class="ab-submenu"><li id='wp-admin-bar-new-post'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php'>Post</a></li><li id='wp-admin-bar-new-media'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/media-new.php'>Media</a></li><li id='wp-admin-bar-new-page'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=page'>Page</a></li><li id='wp-admin-bar-new-shows'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=shows'>Shows</a></li><li id='wp-admin-bar-new-schedule'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=schedule'>Schedule</a></li><li id='wp-admin-bar-new-radiochannel'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=radiochannel'>Radio channel</a></li><li id='wp-admin-bar-new-chart'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=chart'>Chart</a></li><li id='wp-admin-bar-new-members'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=members'>Team</a></li><li id='wp-admin-bar-new-place'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=place'>Place</a></li><li id='wp-admin-bar-new-artist'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=artist'>Artist</a></li><li id='wp-admin-bar-new-event'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=event'>Event</a></li><li id='wp-admin-bar-new-podcast'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=podcast'>Podcast</a></li><li id='wp-admin-bar-new-release'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post-new.php?post_type=release'>Release</a></li><li id='wp-admin-bar-new-user'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/user-new.php'>User</a></li></ul></div></li><li id='wp-admin-bar-edit'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post.php?post=23&#038;action=edit'>Edit Page</a></li><li id='wp-admin-bar-wpseo-menu' class="menupop"><a class='ab-item' aria-haspopup="true" href='https://dev.tubelive.de/wp-admin/admin.php?page=wpseo_dashboard'><div id="yoast-ab-icon" class="ab-item yoast-logo svg"><span class="screen-reader-text">SEO</span></div><div class="wpseo-score-icon adminbar-seo-score na"><span class="adminbar-seo-score-text screen-reader-text"></span></div></a><div class="ab-sub-wrapper"><ul id='wp-admin-bar-wpseo-menu-default' class="ab-submenu"><li id='wp-admin-bar-wpseo-configuration-wizard'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?page=wpseo_configurator'>Configuration Wizard</a></li><li id='wp-admin-bar-wpseo-kwresearch' class="menupop"><div class="ab-item ab-empty-item" tabindex="0" aria-haspopup="true">Keyword Research</div><div class="ab-sub-wrapper"><ul id='wp-admin-bar-wpseo-kwresearch-default' class="ab-submenu"><li id='wp-admin-bar-wpseo-kwresearchtraining'><a class='ab-item' href='https://yoa.st/wp-admin-bar?php_version=7.1.27&#038;platform=wordpress&#038;platform_version=5.1.1&#038;software=free&#038;software_version=10.0.1&#038;role=administrator&#038;days_active=0' target='_blank'>Keyword research training</a></li><li id='wp-admin-bar-wpseo-adwordsexternal'><a class='ab-item' href='https://yoa.st/keywordplanner' target='_blank'>Google Ads</a></li><li id='wp-admin-bar-wpseo-googleinsights'><a class='ab-item' href='https://yoa.st/google-trends' target='_blank'>Google Trends</a></li></ul></div></li><li id='wp-admin-bar-wpseo-analysis' class="menupop"><div class="ab-item ab-empty-item" tabindex="0" aria-haspopup="true">Analyze this page</div><div class="ab-sub-wrapper"><ul id='wp-admin-bar-wpseo-analysis-default' class="ab-submenu"><li id='wp-admin-bar-wpseo-inlinks'><a class='ab-item' href='https://search.google.com/search-console/links/drilldown?resource_id=https%3A%2F%2Fdev.tubelive.de&#038;type=EXTERNAL&#038;target=https%3A%2F%2Fdev.tubelive.de%2F&#038;domain=' target='_blank'>Check links to this URL</a></li><li id='wp-admin-bar-wpseo-kwdensity'><a class='ab-item' href='http://www.zippy.co.uk/keyworddensity/index.php?url=https%3A%2F%2Fdev.tubelive.de%2F&#038;keyword=' target='_blank'>Check Keyphrase Density</a></li><li id='wp-admin-bar-wpseo-cache'><a class='ab-item' href='//webcache.googleusercontent.com/search?strip=1&#038;q=cache:https%3A%2F%2Fdev.tubelive.de%2F' target='_blank'>Check Google Cache</a></li><li id='wp-admin-bar-wpseo-header'><a class='ab-item' href='//quixapp.com/headers/?r=https%3A%2F%2Fdev.tubelive.de%2F' target='_blank'>Check Headers</a></li><li id='wp-admin-bar-wpseo-structureddata'><a class='ab-item' href='https://search.google.com/structured-data/testing-tool#url=https%3A%2F%2Fdev.tubelive.de%2F' target='_blank'>Google Structured Data Test</a></li><li id='wp-admin-bar-wpseo-facebookdebug'><a class='ab-item' href='//developers.facebook.com/tools/debug/og/object?q=https%3A%2F%2Fdev.tubelive.de%2F' target='_blank'>Facebook Debugger</a></li><li id='wp-admin-bar-wpseo-pinterestvalidator'><a class='ab-item' href='https://developers.pinterest.com/tools/url-debugger/?link=https%3A%2F%2Fdev.tubelive.de%2F' target='_blank'>Pinterest Rich Pins Validator</a></li><li id='wp-admin-bar-wpseo-htmlvalidation'><a class='ab-item' href='//validator.w3.org/check?uri=https%3A%2F%2Fdev.tubelive.de%2F' target='_blank'>HTML Validator</a></li><li id='wp-admin-bar-wpseo-cssvalidation'><a class='ab-item' href='//jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fdev.tubelive.de%2F' target='_blank'>CSS Validator</a></li><li id='wp-admin-bar-wpseo-pagespeed'><a class='ab-item' href='//developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fdev.tubelive.de%2F' target='_blank'>Google Page Speed Test</a></li><li id='wp-admin-bar-wpseo-google-mobile-friendly'><a class='ab-item' href='https://www.google.com/webmasters/tools/mobile-friendly/?url=https%3A%2F%2Fdev.tubelive.de%2F' target='_blank'>Mobile-Friendly Test</a></li></ul></div></li><li id='wp-admin-bar-wpseo-settings' class="menupop"><div class="ab-item ab-empty-item" tabindex="0" aria-haspopup="true">SEO Settings</div><div class="ab-sub-wrapper"><ul id='wp-admin-bar-wpseo-settings-default' class="ab-submenu"><li id='wp-admin-bar-wpseo-general'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?page=wpseo_dashboard'>General</a></li><li id='wp-admin-bar-wpseo-titles'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?page=wpseo_titles'>Search Appearance</a></li><li id='wp-admin-bar-wpseo-search-console'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?page=wpseo_search_console'>Search Console</a></li><li id='wp-admin-bar-wpseo-social'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?page=wpseo_social'>Social</a></li><li id='wp-admin-bar-wpseo-tools'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?page=wpseo_tools'>Tools</a></li><li id='wp-admin-bar-wpseo-licenses'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?page=wpseo_licenses'>Premium</a></li><li id='wp-admin-bar-wpseo-courses'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?page=wpseo_courses'>Courses</a></li></ul></div></li></ul></div></li><li id='wp-admin-bar-delete-cache'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/index.php?action=delcachepage&#038;path=%2F&#038;_wpnonce=c8812e7f29' title='Delete cache of the current page'>Delete Cache</a></li><li id='wp-admin-bar-vc_inline-admin-bar-link' class="vc_inline-link"><a class='ab-item' href='https://dev.tubelive.de/wp-admin/post.php?vc_action=vc_inline&#038;post_id=23&#038;post_type=page'>Edit with WPBakery Page Builder</a></li><li id='wp-admin-bar-new_draft'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?action=duplicate_post_save_as_new_post_draft&#038;post=23&#038;_wpnonce=69534207eb'>Copy to a new draft</a></li><li id='wp-admin-bar-revslider' class="revslider-menu"><a class='ab-item' href='https://dev.tubelive.de/wp-admin/admin.php?page=revslider'><span class="rs-label">Slider Revolution</span></a></li></ul><ul id='wp-admin-bar-top-secondary' class="ab-top-secondary ab-top-menu"><li id='wp-admin-bar-search' class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="https://dev.tubelive.de/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Search</label><input type="submit" class="adminbar-button" value="Search"/></form></div></li><li id='wp-admin-bar-my-account' class="menupop with-avatar"><a class='ab-item' aria-haspopup="true" href='https://dev.tubelive.de/wp-admin/profile.php'>Howdy, <span class="display-name">admin</span><img alt='' src='https://secure.gravatar.com/avatar/d0715a035d49c5470b8b12ae55db7da9?s=26&#038;d=blank&#038;r=g' srcset='https://secure.gravatar.com/avatar/d0715a035d49c5470b8b12ae55db7da9?s=52&#038;d=blank&#038;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id='wp-admin-bar-user-actions' class="ab-submenu"><li id='wp-admin-bar-user-info'><a class='ab-item' tabindex="-1" href='https://dev.tubelive.de/wp-admin/profile.php'><img alt='' src='https://secure.gravatar.com/avatar/d0715a035d49c5470b8b12ae55db7da9?s=64&#038;d=blank&#038;r=g' srcset='https://secure.gravatar.com/avatar/d0715a035d49c5470b8b12ae55db7da9?s=128&#038;d=blank&#038;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>admin</span><span class='username'>nielas_q4vj37vx</span></a></li><li id='wp-admin-bar-edit-profile'><a class='ab-item' href='https://dev.tubelive.de/wp-admin/profile.php'>Edit My Profile</a></li><li id='wp-admin-bar-logout'><a class='ab-item' href='https://dev.tubelive.de/wp-login.php?action=logout&#038;_wpnonce=346f388e45'>Log Out</a></li></ul></div></li></ul>			</div>
						<a class="screen-reader-shortcut" href="https://dev.tubelive.de/wp-login.php?action=logout&#038;_wpnonce=346f388e45">Log Out</a>
					</div>

			</body>
</html>

<!-- Dynamic page generated in 0.079 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2019-03-30 21:06:21 -->
