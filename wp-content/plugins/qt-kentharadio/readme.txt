=============================================================|
QT KENTHARADIO
=============================================================|
MANUAL & DOCUMENTATION:
http://manual-kentharadio.qantumthemes.xyz/
=============================================================|
1.4 [2019 Jan 11]
[x] CHART mp3 samples now playing in main player
[x] ADDED 12 and 24 hours time format for schedule in Appearance > Customize
[x] UPDATED .pot file
[x] ADDED support for port 443 for Shoutcast https stream
[x] IMPROVED responsivity for soundcloud embedded in charts tracklist

1.3 [2018 May 26]
[x] ADDED WooCommerce buy link support for chart tracks

1.2 [2018 May 23]
[x] Song title feed starting automatically

1.1 [2018 May 16]
[x] added class qt-kentharadio-playbuttonlist to ul in shortcode-button.php