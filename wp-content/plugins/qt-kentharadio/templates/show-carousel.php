<?php
/*
Package: qt-kentharadio
*/


$quantity = count($events);
if(!isset($grid_id)){ $grid_id =  uniqid('qt-kentharadio-showslider'); }
?>
<!-- SHOW CAROUSEL ================================================== -->	
<div id="<?php echo esc_attr($grid_id); ?>" class="qt-container qt-relative qt-autorefresh">
	<div class="qt-slickslider-outercontainer qt-slickslider-outercontainer__<?php echo esc_attr($design); ?> qt-relative">
		<div class="row">
			<div class="col s9 m8">
				<?php if($title){ ?>
					<h3 class="qt-sectiontitle "><?php echo esc_html($title); ?></h3>
				<?php } else { ?>
					<span class="qt-sectiontitle qt-fontsize-h3 qt-invisible"></span>
				<?php } ?>
			</div>
			<?php if( intval($quantity) > 3 && $design == 'default'){ ?>
			<div class="col s3 m4 qt-carouselcontrols qt-right">
				<i data-slickprev class="material-icons">chevron_left</i>
				<i data-slicknext class="material-icons">chevron_right</i>
			</div>
			<?php } ?>
		</div>
		<div class="qt-slickslider-container qt-slickslider-cards">
			<div class="qt-slickslider qt-invisible qt-animated qt-slickslider-multiple row" data-slidestoshow="3" data-slidestoscroll="1" data-variablewidth="false" data-arrows="false" data-dots="false" data-infinite="true" data-centermode="false" data-pauseonhover="true" data-autoplay="false" data-arrowsmobile="false"  data-centermodemobile="false" data-dotsmobile="false"  data-slidestoshowmobile="1" data-variablewidthmobile="true" data-infinitemobile="false" data-slidestoshowipad="3">
				<?php  
				foreach($events as $event){ 
					$neededEvents = array('show_id','show_time','show_time_end');
					foreach($neededEvents as $n){
					  if(!array_key_exists($n,$events)){
						  $events[$n] = '';
					  }
					}
					$show_id = $event['show_id'][0];
					$show_time_d = $event['show_time'];
					$show_time_end_d = $event['show_time_end'];
					if($show_time_end_d == "00:00"){
						$show_time_end_d = "24:00";
					}
					if($now < $show_time_end_d && $total <= $maximum){
						$total ++;
						// 12 hours format
						if(get_theme_mod('QT_timing_settings', '12') == '12'){
							$show_time_d = date("g:i a", strtotime($show_time_d));
							$show_time_end_d = date("g:i a", strtotime($show_time_end_d));
						}
						?>
						<div class="qt-item qt-item-card col s12 m4">
							<?php
							/**
							 * ================================================
							 * Dynamic template inclusion (can be overridden by the theme)
							 * [$file name of the php template file]
							 * @var string
							 */
							$file = 'item-show.php';
							$template = locate_template( qt_kentharadio_override_template_path() . $file );
							if ( !$template ){
								$template =  $file;
							}
							include $template;
							/**
							 * End of template inclusion
							 * ================================================*/
							?>
						</div>
						<?php  
					}
					wp_reset_postdata();
				}//foreach
				?>
			</div>
			<?php if( intval($quantity) > 3 && $design == 'center'){ ?>
			<div class="qt-carouselcontrols">
				<i data-slickprev class="qt-arr"></i>
				<i data-slicknext class="qt-arr"></i>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<!--  SHOW CAROUSEL END ================================================== -->