<?php
/*
Package: qt-kentharadio
*/
if(!isset($grid_id)){ $grid_id =  uniqid('qt-kentharadio-showslider'); }
?>
<a id="<?php echo esc_attr($grid_id.'-'.$show_id); ?>" class="qt-kentharadio-show qt-autorefresh qt-negative" href="<?php echo get_the_permalink( $show_id ); ?>">
	<?php 
	if (has_post_thumbnail($show_id)){ 
       	echo get_the_post_thumbnail( $show_id, 'large' );
    }
	?>
	<div class="qt-kentharadio-show__header">
		<div class="qt-kentharadio-vc">
			<div class="qt-kentharadio-vi">
				<h5 class="qt-caption-med qt-capfont hide-on-small-and-down">
		  			<?php if($now > $show_time && $now < $show_time_end){ ?>
					<span>
						<?php echo esc_attr__("Now On Air","qt-kentharadio"); ?>
					</span>
					<?php } else { ?>
					<span>
						<?php echo esc_attr__("Upcoming","qt-kentharadio");  ?>
					</span>
					<?php } ?>
		  		</h5>				
				<h2 class="qt-kentharadio-nmt qt-fontsize-h0"><?php echo get_the_title($show_id); ?></h2>
				<?php if(get_post_meta($show_id,"subtitle", true)){ ?>
				<h4 class="qt-kentharadio-nmt">
					<?php echo esc_attr(get_post_meta($show_id,"subtitle", true)); ?>
				</h4>
				<?php } ?>
				<h5 class="qt-kentharadio-nmt qt-item-metas"><?php echo htmlspecialchars_decode(esc_attr($show_time_d) ); ?><i class='material-icons'>keyboard_arrow_right</i><?php echo htmlspecialchars_decode(esc_attr($show_time_end_d) ); ?></h5>
			</div>
		</div>
	</div>
</a>