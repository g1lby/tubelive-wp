<?php
/*
Package: qt-kentharadio
*/
if(!isset($grid_id)){ $grid_id =  uniqid('qt-kentharadio-showslider'); }
?>

<!-- SLIDER SHORTCODE ================================================== -->
<div id="<?php echo esc_attr($grid_id); ?>" class="slider qt-autorefresh qt-material-slider qt-widescreen" data-proportion="wide" data-proportionmob="wide">
	<ul class="slides">

		<?php  
		foreach($events as $event){ 
			$neededEvents = array('show_id','show_time','show_time_end');
			foreach($neededEvents as $n){
			  if(!array_key_exists($n,$events)){
				  $events[$n] = '';
			  }
			}
			$show_id = $event['show_id'][0];
			$show_time = $event['show_time'];
			$show_time_end = $event['show_time_end'];
			$show_time_d = $show_time;
			$show_time_end_d = $show_time_end;
			if($show_time_end_d == "00:00"){
				$show_time_end_d = "24:00";
			}
			
			if($now < $show_time_end_d && $total <= $maximum){
				$total ++;
				// 12 hours format
				if(get_theme_mod('QT_timing_settings', '12') == '12'){
					$show_time_d = date("g:i a", strtotime($show_time_d));
					$show_time_end_d = date("g:i a", strtotime($show_time_end_d));
				}
				?>
				<li>

					<?php 
					/**
					 * ================================================
					 * Dynamic template inclusion (can be overridden by the theme)
					 * [$file name of the php template file]
					 * @var string
					 */
					$file = 'show-hero.php';
					$template = locate_template( qt_kentharadio_override_template_path() . $file );
					if ( !$template ){
						$template =  $file;
					}
					include $template;
					/**
					 * End of template inclusion
					 * ================================================*/
					?>
				</li>
				<?php 
			}
		}//foreach
		?>
	</ul>
	<div class="qt-control-arrows">
		<a href="#" class="prev qt-slideshow-link"><i class='material-icons qt-icon-mirror'>arrow_back</i></a>
		<a href="#" class="next qt-slideshow-link"><i class='material-icons'>arrow_forward</i></a>
	</div>
</div>
<hr class="qt-clearfix">
<!--  SLIDER SHORTCODE END ================================================== -->