<?php
/*
Package: qt-kentharadio
*/
if(!isset($grid_id)){ $grid_id =  uniqid('qt-kentharadio-showslider'); }
?>
<a id="<?php echo esc_attr($grid_id); ?>" class="qt-kentharadio-showmini qt-autorefresh" href="<?php echo get_the_permalink( $show_id ); ?>">
	<?php 
	if (has_post_thumbnail($show_id)){ 
       	echo get_the_post_thumbnail( $show_id, 'thumbnail' );
    }
	?>
	<div class="<?php if (has_post_thumbnail($show_id)){  ?>thumbtxt<?php } ?>">
		<h4 class="qt-kentharadio-nmt"><?php echo get_the_title( $show_id ); ?></h4>
		<h5 class="qt-kentharadio-nmt qt-item-metas"><?php echo htmlspecialchars_decode(esc_attr($show_time_d) ); ?><i class='material-icons'>keyboard_arrow_right</i><?php echo htmlspecialchars_decode(esc_attr($show_time_end_d) ); ?></h5>
	</div>
</a>
