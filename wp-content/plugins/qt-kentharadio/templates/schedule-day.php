<?php
/*
Package: qt-kentharadio
*/
$id = (isset($id))? $id : get_the_id();
?>
<!-- SCHEDULE DAY ================================================== -->
<div class="qt-kentharadio-scheduleday qt-kentharadio-row qt-kentharadio-row-s-1 qt-kentharadio-row-m-2 qt-kentharadio-row-l-3 qt-kentharadio-row-gap-10">
	<?php
   	$events= get_post_meta($id, 'track_repeatable', true);
    if(is_array($events)){
    	$n = 0;
    	$count = 0;
      	foreach($events as $event){ 
      		if(array_key_exists('show_id', $event)){
      			if(array_key_exists(0, $event['show_id'])){
      				if(is_numeric($event['show_id'][0]) && ($event['show_id'][0] !== 0)){
      					?>
      					<div class="qt-kentharadio-col">
	      					<?php
	      					$show_id = $event['show_id'][0];
							$post = get_post($show_id); 
							if(is_object($post)):
								$count = $count+1;
								$neededEvents = array('show_id','show_time','show_time_end');
								foreach($neededEvents as $n){
								  if(!array_key_exists($n,$events)){
								      $events[$n] = '';
								  }
								}
								setup_postdata($post);
								$show_time_d = $event['show_time'];
								$show_time_end_d = $event['show_time_end'];
								if($show_time_d === "24:00"){
									$show_time_d === "00:00";
								}
								if($show_time_end_d === "24:00"){
									$show_time_end_d === "00:00";
								}
								// 12 hours format
								if(get_theme_mod('QT_timing_settings', '12') == '12'){
									$show_time_d = date("g:i a", strtotime($show_time_d));
									$show_time_end_d = date("g:i a", strtotime($show_time_end_d));
								}
								$now = current_time("H:i");
								/**
								 * ================================================
								 * Dynamic template inclusion (can be overridden by the theme)
								 * [$file name of the php template file]
								 * @var string
								 */
								$file = 'item-show.php';
								$template = locate_template( qt_kentharadio_override_template_path() . $file );
								if ( !$template ){
									$template =  $file ;
								}
								include $template;
								/**
								 * End of template inclusion
								 * ================================================*/
							
							endif; 
							wp_reset_postdata();
							?>
						</div>
						<?php  
					} // is_numeric	      				
				}
			} // array_key_exists
		}//foreach
	} else {
		echo esc_attr__("Sorry, there are no shows scheduled on this day","qt-kentharadio");
	}
	wp_reset_postdata();
	?>
</div>
<!-- SCHEDULE DAY END ================================================== -->