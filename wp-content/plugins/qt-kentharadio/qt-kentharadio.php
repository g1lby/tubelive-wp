<?php
/*
Plugin Name: QT KenthaRadio
Plugin URI: http://qantumthemes.com
Description: Adds radio station capabilities to Kentha WordPress Thems
Version: 1.4
Author: QantumThemes
Author URI: http://qantumthemes.com
Text Domain: qt-kentharadio
Domain Path: /languages
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 *	For theme to check if is active
 */
function qt_kentharadio_active() {
	return true;
}

/**
* Returns current plugin version.
* @return string Plugin version
*/
function qt_kentharadio_plugin_get_version() {
	$plugin_data = get_plugin_data( __FILE__ );
	$plugin_version = $plugin_data['Version'];
	return $plugin_version;
}


function qt_kentharadio_plugin_url() {
	return plugins_url( '' , __FILE__ );
}

/**
 *	The plugin textdomain
 */
function qt_kentharadio_load_plugin_textdomain() {
	load_plugin_textdomain( 'qt-kentharadio', FALSE, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'qt_kentharadio_load_plugin_textdomain' );

/**
 *	Scripts and styles register
 */
add_action("init",'qt_kentharadio_register_scripts');
function qt_kentharadio_register_scripts(){
	wp_register_style(	'qt_kentharadio_style',plugins_url( 'assets/css/qt-kentharadio-style.css' , __FILE__ ), false, qt_kentharadio_plugin_get_version());
	wp_register_script(	'qt-kentharadio-script-min',plugins_url( 'assets/js/min/qt-kentharadio-script-min.js' , __FILE__ ), array('jquery','kentha-qt-main-script','kenthaplayer-min'), qt_kentharadio_plugin_get_version(), true);
}

/**
 *	Scripts and styles enqueue
 */
add_action("wp_enqueue_scripts",'qt_kentharadio_enqueue_scripts');
function qt_kentharadio_enqueue_scripts(){
	wp_enqueue_style( 'qt_kentharadio_style');
	wp_enqueue_script( 'qt-kentharadio-script-min' );
}


/* Get number of current week in the month, used by schedule functions
=============================================*/
function qt_kentharadio_week_number( $date = 'today' ) { 
    return ceil( date( 'j', strtotime( $date ) ) / 7 );
}


/**
 *	This function allows to override local plugin template and use the one in the theme
 *	such as WooCommerce and Page Builder does.
 */
function qt_kentharadio_local_template_path() {
	return 'templates/';
}

function qt_kentharadio_override_template_path() {
	return 'qt-kentharadio-templates/';
}






function qt_kentharadio_autoupdater_currenturl() {
	$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
	?>
    <div id="qt-kentharadio-currentpermalink"  data-permalink="<?php echo esc_url( $actual_link); ?>">
    	<!-- Nothing here. Used by javascript for auto refresh -->
    </div>
    <?php
}
add_action( "wp_footer", 'qt_kentharadio_autoupdater_currenturl');




/**
 *
 *	Files inclusion
 * 
 */
$active_plugins = (array) get_option( 'active_plugins', array() );
if ( ! empty( $active_plugins ) && in_array( 'ttg-core/ttg-core.php', $active_plugins ) ) {

	/**
	 * Custom post types
	 */
	include plugin_dir_path( __FILE__ ) . '/inc/backend/posttypes/shows.php';
	include plugin_dir_path( __FILE__ ) . '/inc/backend/posttypes/schedule.php';
	include plugin_dir_path( __FILE__ ) . '/inc/backend/posttypes/radiochannel.php';
	include plugin_dir_path( __FILE__ ) . '/inc/backend/posttypes/chart.php';
	include plugin_dir_path( __FILE__ ) . '/inc/backend/posttypes/members.php';

	/**
	 * Functions
	 */
	include plugin_dir_path( __FILE__ ) . '/inc/frontend/templatetag-showtable.php';
	include plugin_dir_path( __FILE__ ) . '/inc/frontend/templatetag-singlesocial.php';
	include plugin_dir_path( __FILE__ ) . '/inc/frontend/templatetag-subtitle.php';
	include plugin_dir_path( __FILE__ ) . '/inc/frontend/templatetag-showgenres.php';
	include plugin_dir_path( __FILE__ ) . '/inc/frontend/templatetag-scheduleday.php';
	include plugin_dir_path( __FILE__ ) . '/inc/frontend/templatetag-schedule.php';


	/**
	 * Schortcodes
	 */
	include plugin_dir_path( __FILE__ ) . '/inc/shortcodes/shortcode-schedule.php';
	include plugin_dir_path( __FILE__ ) . '/inc/shortcodes/shortcode-onair.php';
	include plugin_dir_path( __FILE__ ) . '/inc/shortcodes/shortcode-onair-mini.php';
	include plugin_dir_path( __FILE__ ) . '/inc/shortcodes/shortcode-upcoming-shows-carousel.php';
	include plugin_dir_path( __FILE__ ) . '/inc/shortcodes/shortcode-upcoming-shows-slider.php';
	include plugin_dir_path( __FILE__ ) . '/inc/shortcodes/shortcode-appicons.php';
	include plugin_dir_path( __FILE__ ) . '/inc/shortcodes/shortcode-chart.php';
	include plugin_dir_path( __FILE__ ) . '/inc/shortcodes/shortcode-playbutton.php';
	include plugin_dir_path( __FILE__ ) . '/inc/shortcodes/shortcode-songtitle.php';

	/**
	 * Widgets
	 */
	include plugin_dir_path( __FILE__ ) . '/inc/widgets/widget-chart.php';
	include plugin_dir_path( __FILE__ ) . '/inc/widgets/widget-onair.php';
	include plugin_dir_path( __FILE__ ) . '/inc/widgets/widget-upcomingshows.php';

	
}

/**
 * Customizations
 */
if ( ! function_exists( 'Kirki' ) ) {
	require_once	plugin_dir_path( __FILE__ ) . '/inc/backend/customizer/kirki/kirki.php';
}
require_once	plugin_dir_path( __FILE__ ) . '/inc/backend/customizer/kirki-config-class/class-plugin-kirki.php';
require_once	plugin_dir_path( __FILE__ ) . '/inc/backend/customizer/kirki-config-class/include-kirki.php';
require_once	plugin_dir_path( __FILE__ ) . '/inc/backend/customizer/kirki-configuration/sections.php';
require_once	plugin_dir_path( __FILE__ ) . '/inc/backend/customizer/kirki-configuration/fields.php';
require_once	plugin_dir_path( __FILE__ ) . '/inc/backend/customizer/kirki-configuration/configuration.php'; 

