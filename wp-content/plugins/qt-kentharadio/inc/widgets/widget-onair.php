<?php
/*
Package: qt-kentharadio
Description: WIDGET ON AIR SHOW
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/
  

add_action( 'widgets_init', 'qt_kentharadio_onair_widget' );
function qt_kentharadio_onair_widget() {
	register_widget( 'qt_kentharadio_Onair_widget' );
}

class qt_kentharadio_Onair_widget extends WP_Widget {
	/**
	 * [__construct]
	 * =============================================
	 */
	public function __construct() {
		$widget_ops = array( 'classname' => 'qtonairwidget', 'description' => esc_attr__('Display which show is on air by reading the schedule of Wordpress', "qt-kentharadio") );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'qtonairwidget-widget' );
		parent::__construct( 'qtonairwidget-widget', esc_attr__('QT On Air show', "qt-kentharadio"), $widget_ops, $control_ops );
	}
	/**
	 * [widget]
	 * =============================================
	 */
	public function widget( $args, $instance ) {
		extract( $args );


		// We need the ID for ajax refresh
		$widget_id = 'upcomingshow';
		if(isset($this)){
			$widget_id = $this->id;
		}

		
		/**
		 * 
		 * Get group of meta for a post
		 * 	=============================================
		 */
		if(!function_exists('qt_kentharadio_get_group')){
		function qt_kentharadio_get_group( $group_name , $post_id = NULL ){
			global $post; 	  
			if(!$post_id){ $post_id = $post->ID; }
			$post_meta_data = get_post_meta($post_id, $group_name, true);  
			return $post_meta_data;
		}}

		echo $before_widget;
		if(array_key_exists("title",$instance)){
			echo $before_title.apply_filters("widget_title", $instance['title'], "qtonairwidget-widget").$after_title; 
		}

		$date = current_time("Y-m-d");
		$current_dayweek = current_time("D");
		$total = 0;
		$tabsArray = array();
		$id_of_currentday = 0;
		$now = current_time("H:i");

		$args = array(
			'post_type' => 'schedule',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'orderby' => 'menu_order',
			'order'   => 'ASC'
		);

		if(array_key_exists("schedulefilter", $instance)) {
			$qt_kentharadio_schedulefilter = $instance['schedulefilter'];
		}

		if( isset($qt_kentharadio_schedulefilter) ) {
			if($qt_kentharadio_schedulefilter !== ''){
				$args ['tax_query'] = array(
					array(
						'taxonomy' => 'schedulefilter',
						'field'    => 'slug',
						'terms'    => $qt_kentharadio_schedulefilter
					)
				);
			}
		}
		wp_reset_postdata();

		/**
		 * ====================================================================================================
		 * Update from 2017 September 10
		 * adding week-of-the-month filtering if enabled
		 */
		$week_num = qt_kentharadio_week_number();
		$qt_execute_week_control = get_theme_mod('QT_monthly_schedule', '0' );
		if(get_theme_mod('QT_monthly_schedule', '0' )){
			$week_num = qt_kentharadio_week_number();
			$args ['meta_key'] = 'month_week';
			$args ['meta_value'] = $week_num;
			$args['meta_compare'] = 'LIKE';
		}

		/* =========================================== update end ===========================================*/


		$the_query_meta = new WP_Query( $args );

		while ( $the_query_meta->have_posts() && $id_of_currentday == 0 ):

			$the_query_meta->the_post();
			global $post;
			setup_postdata( $post );

			/**
			 * ====================================================================================================
			 * Update from 2017 September 10
			 * adding week-of-the-month filtering if enabled
			 */
			$can_display_this_schedule = true;
			if($qt_execute_week_control  == '1'){
				$can_display_this_schedule = false;
		 		$schedule_weeks = get_post_meta( $post->ID, 'month_week', true );
		 		if(is_array($schedule_weeks)){
		 			foreach ($schedule_weeks as $w){
		 				if($w == $week_num){
		 					$can_display_this_schedule = true;
		 				}
		 			}
		 		}
		 	}
		 	/* =========================================== update end ===========================================*/
		 	

		 	if(true === $can_display_this_schedule){ // check added for 2017-09-10 week schedule update

				$active = '';
				$maincolor = '';
				
				$total++;
				
				/*
				*
				*	Create the array for making the content
				*
				**/
				
				$tab = array('name' => $post->post_name,
							'title' => $post->post_title,
							'id' => $post->ID);
				/*
				*
				*	Find out if is a day of the calendar
				*
				*/
				$schedule_date = get_post_meta($post->ID, 'specific_day', true);
				$schedule_week_day = get_post_meta($post->ID, 'week_day', true);
				/*
				1. Find which is the current day, otherwise random shows will be shown
				*/
				if($schedule_date == $date){
					$id_of_currentday = $post->ID;
				} else {
					/*
					2. check if is this day of the week
					*/
					if(is_array($schedule_week_day)){
						foreach($schedule_week_day as $day){ // each schedule can fit multiple days
							if(strtolower($day) == strtolower($current_dayweek)){
								//echo 'Found'.$post->ID;
								$id_of_currentday = $post->ID;

							}
						}
					}
				}
			}
		endwhile;
		

		if($id_of_currentday != 0){
			
			$events= qt_kentharadio_get_group('track_repeatable', $post->ID);   

			if(is_array($events)){
				$maximum = 1;
				$total = 1;
				foreach($events as $event){ 
					$neededEvents = array('show_id','show_time','show_time_end');
					foreach($neededEvents as $n){
					  if(!array_key_exists($n,$events)){
						  $events[$n] = '';
					  }
					}
					$show_id = $event['show_id'][0];
					global $post;
					global $show_time;
					global $show_time_end;
					setup_postdata( $post );
					$post = get_post($show_id); 
					$show_time = $event['show_time'];
					$show_time_end = $event['show_time_end'];
					if($show_time_end == "00:00"){
						$show_time_end = "24:00";
					}


					if($now < $show_time_end && $total <= $maximum){

						$total ++;
						?>
						<div  id="<?php echo esc_attr($widget_id); ?>" class="qt-autorefresh qt-kentharadio-widget-onair qt-part-archive-item qt-item-inline">
							<?php if (has_post_thumbnail()){ ?>
					        <a href="<?php the_permalink(); ?>" class="qt-inlineimg qt-kentharadio-widget-onair__thumb">
					            <?php the_post_thumbnail( 'post-thumbnail' ); ?>
					        </a>
					     	<?php } ?>
							<h4>
								<span><?php the_title(); ?></span>
							</h4>
							<a href="<?php the_permalink(); ?>" class="qt-btn qt-btn-s qt-btn-secondary"><?php echo esc_attr__("Go To Show", "qt-kentharadio") ?></a>
						</div>
						<?php
					}
					wp_reset_postdata();
				}//foreach
			} else {
				echo esc_attr__("Sorry, there are no shows scheduled on this day","qt-kentharadio");
			}
			wp_reset_postdata();
		}
		echo $after_widget;
	}

	/**
	 * [update save the parameters]
	 * =============================================
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$attarray = array(
			'title',
			'schedulefilter',
		);
		foreach ($attarray as $a){
			$instance[$a] = strip_tags( $new_instance[$a] );
		}
		return $instance;
	}

	/**
	 * [form widget parameters form]
	 * =============================================
	 */
	public function form( $instance ) {
		$defaults = array( 
			'title' => esc_attr__('Now on air', "qt-kentharadio"),
			'schedulefilter' => ''
			);
		$instance = wp_parse_args( (array) $instance, $defaults ); 
		?>
	 	<div class="qt-widgetadmin">
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e('Title:', "qt-kentharadio"); ?></label>
			<input class="qt-text-full" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>"/>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'schedulefilter' ) ); ?>"><?php echo esc_attr__('Schedule filter (slug):', "qt-kentharadio"); ?></label>
			<input class="qt-text-full" id="<?php echo esc_attr( $this->get_field_id( 'schedulefilter' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'schedulefilter' ) ); ?>" value="<?php echo esc_attr( $instance['schedulefilter'] ); ?>"  />
		</p>
		<p><?php echo esc_html("Please remember that the website can only display show items from midnight to midnight", "qt-kentharadio"); ?></p>
		</div>
	<?php
	}
}
