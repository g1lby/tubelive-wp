<?php

add_action('init', 'qt_kentharadio_shows_register_type');  
function qt_kentharadio_shows_register_type() {
	$labelsshow = array(
		'name' => esc_attr__("Shows","qt-kentharadio"),
		'singular_name' => esc_attr__("Shows","qt-kentharadio"),
		'add_new' => esc_attr__("Add new","qt-kentharadio"),
		'add_new_item' => esc_attr__("Add new show","qt-kentharadio"),
		'edit_item' => esc_attr__("Edit show","qt-kentharadio"),
		'new_item' => esc_attr__("New show","qt-kentharadio"),
		'all_items' => esc_attr__("All shows","qt-kentharadio"),
		'view_item' => esc_attr__("View show","qt-kentharadio"),
		'search_items' => esc_attr__("Search show","qt-kentharadio"),
		'not_found' => esc_attr__("No shows found","qt-kentharadio"),
		'not_found_in_trash' => esc_attr__("No shows found in trash","qt-kentharadio"),
		'menu_name' => esc_attr__("Shows","qt-kentharadio")
	);
	$args = array(
		'labels' => $labelsshow,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'capability_type' => 'page',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 50,
		'page-attributes' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'show_in_menu' => true,
		'rewrite' => true,
		'show_in_rest' => true,
		'menu_icon' =>  'dashicons-pressthis',
		'supports' => array('title', 'thumbnail','editor', 'excerpt', 'revisions', 'page-attributes'   )
	); 
	register_post_type( "shows" , $args );

	/* ============= create custom taxonomy for the shows ==========================*/
	$labels = array(
		'name' => esc_html__( 'Genres',"qt-kentharadio" ),
		'singular_name' => esc_html__( 'Genre',"qt-kentharadio" ),
		'search_items' =>  esc_html__( 'Search by genre',"qt-kentharadio" ),
		'popular_items' => esc_html__( 'Popular genres',"qt-kentharadio" ),
		'all_items' => esc_html__( 'All shows',"qt-kentharadio" ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => esc_html__( 'Edit genre',"qt-kentharadio" ), 
		'update_item' => esc_html__( 'Update genre',"qt-kentharadio" ),
		'add_new_item' => esc_html__( 'Add New genre',"qt-kentharadio" ),
		'new_item_name' => esc_html__( 'New genre Name',"qt-kentharadio" ),
		'separate_items_with_commas' => esc_html__( 'Separate genres with commas',"qt-kentharadio" ),
		'add_or_remove_items' => esc_html__( 'Add or remove genres',"qt-kentharadio" ),
		'choose_from_most_used' => esc_html__( 'Choose from the most used genres',"qt-kentharadio" ),
		'menu_name' => esc_html__( 'Genres',"qt-kentharadio" )
	); 
	$args = array(
		'hierarchical' => false,
		'labels' => $labels,
		'show_ui' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'show_in_rest' => true,
		'rewrite' => array( 'slug' => 'showgenre' )
	);
	register_taxonomy('qt-kentharadio-showgenre','shows', $args );
}

/*
*
*	Meta boxes ===========================================================================
*
*	======================================================================================
*/
function qt_kentharadio_shows_capabilities(){
	$fields = array(
	   array(
			'label' => esc_attr__('Subtitle',"qt-kentharadio"),
			'description' => esc_attr__('Used in parallax header',"qt-kentharadio"),
			'id'    => 'subtitle',
			'type'  => 'text'
			)
		, array(
			'label' =>  esc_attr__('Subtitle 2',"qt-kentharadio"),
			'description' => esc_attr__('Used in the parallax header',"qt-kentharadio"),
			'id'    => 'subtitle2',
			'type'  => 'text'
			)
		, array(
			'label' => esc_attr__('Short show description',"qt-kentharadio"),
			'description' => esc_attr__('Used in the schedule',"qt-kentharadio"),
			'id'    => 'show_incipit',
			'type'  => 'editor'
			)
		
	);
	if (class_exists('custom_add_meta_box')){
		$qt_kentharadio_showmetas = new custom_add_meta_box( 'qt_kentharadio_showmetas', 'Show details', $fields, 'shows', true );
	}



	$fields2 = array(
		array(
		'label' => 'Facebook',
		'id'    => 'facebook',
		'type'  => 'text'
		),
		array(
		'label' => 'Flickr',
		'id'    => 'flickr',
		'type'  => 'text'
		),
		array(
		'label' => 'Google+',
		'id'    => 'googleplus',
		'type'  => 'text'
		),
		array(
		'label' => 'Itunes',
		'id'    => 'itunes',
		'type'  => 'text'
		),
		array(
		'label' => 'LastFM',
		'id'    => 'lastfm',
		'type'  => 'text'
		),
		array(
		'label' => 'Linkedin',
		'id'    => 'linkedin',
		'type'  => 'text'
		),
		array(
		'label' => 'Mixcloud',
		'id'    => 'mixcloud',
		'type'  => 'text'
		),
		array(
		'label' => 'Pinterest',
		'id'    => 'pinterest',
		'type'  => 'text'
		),

		array(
		'label' => 'Soundcloud',
		'id'    => 'soundcloud',
		'type'  => 'text'
		),
		array(
		'label' => 'Twitter',
		'id'    => 'twitter',
		'type'  => 'text'
		),

		array(
		'label' => 'Youtube',
		'id'    => 'youtube',
		'type'  => 'text'
		)

	);
	if (class_exists('custom_add_meta_box')){
		$qt_kentharadio_showsocials = new custom_add_meta_box( 'qt_kentharadio_showsocials', 'Social network pages', $fields2, 'shows', true );
	}

}


add_action('wp_loaded', 'qt_kentharadio_shows_capabilities');  