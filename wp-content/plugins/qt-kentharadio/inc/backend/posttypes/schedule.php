<?php

add_action('init', 'qt_kentharadio_schedule_register_type');  
function qt_kentharadio_schedule_register_type() {
	$labelsschedule = array(
		'name' => esc_attr__("Schedule","qt-kentharadio"),
        'singular_name' => esc_attr__("Schedule","qt-kentharadio"),
        'add_new' => esc_attr__("Add new","qt-kentharadio"),
        'add_new_item' => esc_attr__("Add new schedule","qt-kentharadio"),
        'edit_item' => esc_attr__("Edit schedule","qt-kentharadio"),
        'new_item' => esc_attr__("New schedule","qt-kentharadio"),
        'all_items' => esc_attr__("All schedules","qt-kentharadio"),
        'view_item' => esc_attr__("View schedule","qt-kentharadio"),
        'search_items' => esc_attr__("Search schedule","qt-kentharadio"),
        'not_found' => esc_attr__("No schedule found","qt-kentharadio"),
        'not_found_in_trash' => esc_attr__("No schedule found in trash","qt-kentharadio"),
        'menu_name' => esc_attr__("Schedule","qt-kentharadio")
	);
	$args = array(
		'labels' => $labelsschedule,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 50,
		'page-attributes' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'show_in_menu' => true,
		'show_in_rest' => true,
		'menu_icon' =>   'dashicons-clipboard',
		'supports' => array('title','page-attributes')
	); 
	register_post_type( "schedule" , $args );
	
    /* ============= create custom taxonomy  ==========================*/
	$labels = array(
	    'name' => esc_html__( 'Schedule filter',"qt-kentharadio"),
	    'singular_name' => esc_html__( 'Schedule filter',"qt-kentharadio"),
	    'search_items' =>  esc_html__( 'Search by schedule filter',"qt-kentharadio" ),
	    'popular_items' => esc_html__( 'Popular filters',"qt-kentharadio" ),
	    'all_items' => esc_html__( 'All schedule filters',"qt-kentharadio" ),
	    'parent_item' => null,
	    'parent_item_colon' => null,
	    'edit_item' => esc_html__( 'Edit schedule filter',"qt-kentharadio" ), 
	    'update_item' => esc_html__( 'Update schedule filter',"qt-kentharadio" ),
	    'add_new_item' => esc_html__( 'Add schedule filter',"qt-kentharadio" ),
	    'new_item_name' => esc_html__( 'New schedule filter',"qt-kentharadio" ),
	    'separate_items_with_commas' => esc_html__( 'Separate schedule filters with comma',"qt-kentharadio" ),
	    'add_or_remove_items' => esc_html__( 'Add or remove schedule filters',"qt-kentharadio" ),
	    'choose_from_most_used' => esc_html__( 'Choose from the most used schedule filters',"qt-kentharadio" ),
	    'menu_name' => esc_html__( 'Schedule filter',"qt-kentharadio" ),
	); 
	$args = array(
	    'hierarchical' => false,
	    'labels' => $labels,
	    'show_ui' => true,
	    'update_count_callback' => '_update_post_term_count',
	    'query_var' => true,
	    'show_in_rest' => true,
	    'rewrite' => array( 'slug' => 'schedulefilter' )
	);
	register_taxonomy('schedulefilter','schedule',$args	);
}

/*
*
*	Meta boxes ===========================================================================
*
*	======================================================================================
*/


function qt_kentharadio_schedule_capabilities(){
	$week_schedule = array(
		array(
			'label' => 'Week of the month',
			'description' => 'Use to display this schedule only on the following week',
			'id'    => 'month_week',
			'type'  => 'checkbox_group',
			'options' => array( 
		   array('label'=> esc_attr__("1st Week","qt-kentharadio"), 'value' => '1'),
		   array('label'=> esc_attr__("2nd Week","qt-kentharadio"), 'value' => '2'),
		   array('label'=> esc_attr__("3rd Week","qt-kentharadio"), 'value' => '3'),
		   array('label'=> esc_attr__("4th Week","qt-kentharadio"), 'value' => '4'),
		   array('label'=> esc_attr__("5th Week","qt-kentharadio"), 'value' => '5')
		   )
		)
	);	
	$fields = array(
	    array(
			'label' => 'Happens only on a specific day (optional)',
			'description' => 'Used to identify the current show',
			'id'    => 'specific_day',
			'type'  => 'date'
			),
	    array(
			'label' => 'Day of the week (Recursive)',
			'description' => 'Used to identify the current show',
			'id'    => 'week_day',
			'type'  => 'checkbox_group',
			'options' => array( 
	           array('label'=> esc_attr__("Monday","qt-kentharadio"), 'value' => 'mon'),
	           array('label'=> esc_attr__("Tuesday","qt-kentharadio"), 'value' => 'tue'),
	           array('label'=> esc_attr__("Wednesday","qt-kentharadio"), 'value' => 'wed'),
	           array('label'=> esc_attr__("Thursday","qt-kentharadio"), 'value' => 'thu'),
	           array('label'=> esc_attr__("Friday","qt-kentharadio"), 'value' => 'fri'),
	           array('label'=> esc_attr__("Saturday","qt-kentharadio"), 'value' => 'sat'),
	           array('label'=> esc_attr__("Sunday","qt-kentharadio"), 'value' => 'sun')
	           )
			),
		array(
			'label'	=> 'Shows', 
			'desc'	=> 'Add here the shows', 
			'id'	=> 'track_repeatable', 
			'type'	=> 'repeatable', 
			'sanitizer' => array( 
				'featured' => 'meta_box_santitize_boolean',
				'title' => 'sanitize_text_field',
				'desc' => 'wp_kses_data'
			),
			'repeatable_fields' => array (
				'show_id' => array(
					'label' => 'Show',
					'id' => 'show_id',
					'posttype' => 'shows',
					'type' => 'post_chosen'
				),
				'show_time' => array(
					'label' => 'Time (HH:MM)',
					'id' => 'show_time',
					'type' => 'time'
				)
				,'show_end' => array(
					'label' => 'Time End (HH:MM)',
					'id' => 'show_time_end',
					'type' => 'time'
				)
			)
		)
	);
	if(get_theme_mod('QT_monthly_schedule', '0' )){
		$fields = array_merge($week_schedule,$fields);
	}
	if (class_exists('custom_add_meta_box')) {
		$qt_kentharadio_schedule_shows = new custom_add_meta_box( 'qt_kentharadio_schedule_shows', 'Schedule shows', $fields, 'schedule', true );
	}
}

add_action('wp_loaded', 'qt_kentharadio_schedule_capabilities');  

