<?php

add_action('init', 'qt_kentharadio_member_register_type');  
if (!function_exists('qt_kentharadio_member_register_type')){
function qt_kentharadio_member_register_type() {
	$labelsmember = array(
		'name' => esc_attr__("Team","qt-kentharadio"),
		'singular_name' => esc_attr__("Team","qt-kentharadio"),
		'add_new' => esc_attr__("Add new","qt-kentharadio"),
		'add_new_item' => esc_attr__("Add new team member","qt-kentharadio"),
		'edit_item' => esc_attr__("Edit team member","qt-kentharadio"),
		'new_item' => esc_attr__("New team member","qt-kentharadio"),
		'all_items' => esc_attr__("All team members","qt-kentharadio"),
		'view_item' => esc_attr__("View team member","qt-kentharadio"),
		'search_items' => esc_attr__("Search team member","qt-kentharadio"),
		'not_found' => esc_attr__("No team member found","qt-kentharadio"),
		'not_found_in_trash' => esc_attr__("No team member found in trash","qt-kentharadio"),
		'menu_name' => esc_attr__("Team","qt-kentharadio")
	);
	$args = array(
		'labels' => $labelsmember,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'member_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'has_archive' => true,
		'menu_position' => 50,
		'hierarchical' => false,
		'page-attributes' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'show_in_menu' => true,
		'show_in_rest' => true,
		'menu_icon' =>  'dashicons-businessman',
		'supports' => array('title', 'thumbnail','page-attributes','editor', 'revisions'  )
	); 
	register_post_type( "members" , $args );

	/* ============= create custom taxonomy for the members ==========================*/
	$labels = array(
		'name' => esc_html__( 'Types',"qt-kentharadio" ),
		'singular_name' => esc_html__( 'Type',"qt-kentharadio" ),
		'search_items' =>  esc_html__( 'Search by Type',"qt-kentharadio" ),
		'popular_items' => esc_html__( 'Popular Types',"qt-kentharadio" ),
		'all_items' => esc_html__( 'All members',"qt-kentharadio" ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => esc_html__( 'Edit Type',"qt-kentharadio" ), 
		'update_item' => esc_html__( 'Update Type',"qt-kentharadio" ),
		'add_new_item' => esc_html__( 'Add New Type',"qt-kentharadio" ),
		'new_item_name' => esc_html__( 'New Type Name',"qt-kentharadio" ),
		'separate_items_with_commas' => esc_html__( 'Separate Types with commas',"qt-kentharadio" ),
		'add_or_remove_items' => esc_html__( 'Add or remove Types',"qt-kentharadio" ),
		'choose_from_most_used' => esc_html__( 'Choose from the most used Types',"qt-kentharadio" ),
		'menu_name' => esc_html__( 'Member types',"qt-kentharadio" )
	); 
	$args = array(
		'hierarchical' => false,
		'labels' => $labels,
		'show_ui' => true,
		'show_in_rest' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => array( 'slug' => 'membertype' )
	);
	register_taxonomy('membertype','members', $args	);
}}

/*
*
*	Meta boxes ===========================================================================
*
*	======================================================================================
*/
function qt_kentharadio_members_capabilities(){

	$fields = array(
		array(
			'label' => esc_html__('Short bio',"qt-kentharadio"),
			'id'    => 'member_incipit',
			'type'  => 'editor'
			)
		,array(
			'label' => esc_html__('Role in the company',"qt-kentharadio"),
			'id'    => 'member_role',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Facebook link',"qt-kentharadio"),
			'id'    => 'QT_facebook',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Twitter link',"qt-kentharadio"),
			'id'    => 'QT_twitter',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Pinterest link',"qt-kentharadio"),
			'id'    => 'QT_pinterest',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Vimeo link',"qt-kentharadio"),
			'id'    => 'QT_vimeo',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Wordpress link',"qt-kentharadio"),
			'id'    => 'QT_wordpress',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Youtube link',"qt-kentharadio"),
			'id'    => 'QT_youtube',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Soundcloud link',"qt-kentharadio"),
			'id'    => 'QT_soundcloud',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Myspace link',"qt-kentharadio"),
			'id'    => 'QT_myspace',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Itunes link',"qt-kentharadio"),
			'id'    => 'QT_itunes',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Mixcloud link',"qt-kentharadio"),
			'id'    => 'QT_mixcloud',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Resident Advisor link',"qt-kentharadio"),
			'id'    => 'QT_resident-advisor',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('ReverbNation link',"qt-kentharadio"),
			'id'    => 'QT_reverbnation',
			'type'  => 'text'
			)
		,array(
			'label' => esc_html__('Instagram link',"qt-kentharadio"),
			'id'    => 'QT_instagram',
			'type'  => 'text'
		)

		,array(
			'label' => esc_html__('Snapchat link',"qt-kentharadio"),
			'id'    => 'QT_snapchat',
			'type'  => 'text'
		)
		,array(
			'label' => esc_html__('Lastfm link',"qt-kentharadio"),
			'id'    => 'QT_lastfm',
			'type'  => 'text'
		)
		,array(
			'label' => esc_html__('Google+ link',"qt-kentharadio"),
			'id'    => 'QT_googleplus',
			'type'  => 'text'
		)
		,array(
			'label' => esc_html__('Amazon link',"qt-kentharadio"),
			'id'    => 'QT_amazon',
			'type'  => 'text'
		)
		,array(
			'label' => esc_html__('Reddit link',"qt-kentharadio"),
			'id'    => 'QT_reddit',
			'type'  => 'text'
		)
		,array(
			'label' => esc_html__('Vk link',"qt-kentharadio"),
			'id'    => 'QT_vk',
			'type'  => 'text'
		)
		,array(
			'label' => esc_html__('Beatport link',"qt-kentharadio"),
			'id'    => 'QT_beatport',
			'type'  => 'text'
		)
		,array( // Repeatable & Sortable Text inputs
			'label'	=> esc_html__('Associated shows',"qt-kentharadio"), // <label>
			'desc'	=> esc_html__('Manually pick shows to display in the member page',"qt-kentharadio"), // description
			'id'	=> 'members_show_pick', // field id and name
			'type'	=> 'repeatable', // type of field
			'sanitizer' => array( // array of sanitizers with matching kets to next array
				'featured' => 'meta_box_santitize_boolean',
				'title' => 'sanitize_text_field',
				'desc' => 'wp_kses_data'
			),
			'repeatable_fields' => array ( // array of fields to be repeated
				array(
					'label' => esc_html__("Choose a show","qt-kentharadio"),
					'id'	=> 'membershow', // field id and name
					'type' => 'post_chosen',
					'posttype' => 'shows'
				)
			)
		)
	);
	if(class_exists('custom_add_meta_box')) {
		$qt_kentharadio_membersmeta = new custom_add_meta_box( 'qt_kentharadio_membersmeta', 'member details', $fields, 'members', true );
	}

}
add_action('wp_loaded', 'qt_kentharadio_members_capabilities');  
