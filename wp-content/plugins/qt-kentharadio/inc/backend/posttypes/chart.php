<?php

add_action('init', 'qt_kentharadio_chart_register_type');  
function qt_kentharadio_chart_register_type() {
	$labelschart = array(
		'name' => esc_html__("Charts","qt-kentharadio"),
		'singular_name' => esc_html__("Chart","qt-kentharadio"),
		'add_new' => 'Add new ',
		'add_new_item' => 'Add new '.__("chart","qt-kentharadio"),
		'edit_item' => 'Edit '.__("chart","qt-kentharadio"),
		'new_item' => 'New '.__("chart","qt-kentharadio"),
		'all_items' => 'All '.__("Charts","qt-kentharadio"),
		'view_item' => 'View '.__("chart","qt-kentharadio"),
		'search_items' => 'Search '.__("Charts","qt-kentharadio"),
		'not_found' =>  'No '.__("Charts","qt-kentharadio").' found',
		'not_found_in_trash' => 'No '.__("Charts","qt-kentharadio").' found in Trash', 
		'parent_item_colon' => '',
		'menu_name' =>__("Charts","qt-kentharadio")
	);
	$args = array(
		'labels' => $labelschart,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'page',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 50,
		'page-attributes' => true,
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'show_in_menu' => true,
		'show_in_rest' => true,
		'menu_icon' => 'dashicons-playlist-audio',
		'supports' => array('title', 'thumbnail','editor' )
	); 
	register_post_type( "chart" , $args );

	/* ============= create custom taxonomy for the charts ==========================*/
	 $labels = array(
		'name' => esc_html__( 'Chart categories',"qt-kentharadio"),
		'singular_name' => esc_html__( 'Category',"qt-kentharadio"),
		'search_items' =>  esc_html__( 'Search by category',"qt-kentharadio" ),
		'popular_items' => esc_html__( 'Popular categorys',"qt-kentharadio" ),
		'all_items' => esc_html__( 'All charts',"qt-kentharadio" ),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => esc_html__( 'Edit category',"qt-kentharadio" ), 
		'update_item' => esc_html__( 'Update category',"qt-kentharadio" ),
		'add_new_item' => esc_html__( 'Add New category',"qt-kentharadio" ),
		'new_item_name' => esc_html__( 'New category Name',"qt-kentharadio" ),
		'separate_items_with_commas' => esc_html__( 'Separate categorys with commas',"qt-kentharadio" ),
		'add_or_remove_items' => esc_html__( 'Add or remove categorys',"qt-kentharadio" ),
		'choose_from_most_used' => esc_html__( 'Choose from the most used categorys',"qt-kentharadio" ),
		'menu_name' => esc_html__( 'Chart categories',"qt-kentharadio" )
	); 
	$args = array(
		'hierarchical' => false,
		'labels' => $labels,
		'show_ui' => true,
		'show_in_rest' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => true,
		'rewrite' => true
	);
	register_taxonomy('chartcategory', 'chart', $args	);
}


function qt_kentharadio_chart_capabilities(){

	$fields_chart = array(
		array( // Repeatable & Sortable Text inputs
			'label'	=> 'Chart Tracks', // <label>
			'desc'	=> 'Add one for each track in the chart', // description
			'id'	=> 'track_repeatable', // field id and name
			'type'	=> 'repeatable', // type of field
			'sanitizer' => array( // array of sanitizers with matching kets to next array
				'featured' => 'meta_box_santitize_boolean',
				'title' => 'sanitize_text_field',
				'desc' => 'wp_kses_data'
			),
			
			'repeatable_fields' => array ( // array of fields to be repeated
				'releasetrack_track_title' => array(
					'label' => 'Title',
					'id' => 'releasetrack_track_title',
					'type' => 'text'
				)
				,'releasetrack_artist_name' => array(
					'label' => 'Artist/s',
					//'desc'	=> '(All artists separated by comma)', // description
					'id' => 'releasetrack_artist_name',
					'type' => 'text'
				)
				,'releasetrack_soundcloud_url' => array(
					'label' => 'Soundcloud, Youtube or MP3 File',
					'desc'	=> 'Will be transformed into an embedded player in the chart page', // description
					'id' 	=> 'releasetrack_scurl',
					'type' 	=> 'file'
				)
				,'releasetrack_buy_url' => array(
					'label' => 'Track Buy link',
					'desc'	=> 'A link to buy the single track', // description
					'id' 	=> 'releasetrack_buyurl',
					'type' 	=> 'text'
				)
				,'releasetrack_img' => array(
					'label' => 'Cover',
					'desc'	=> 'Better 600x600', // description
					'id' => 'releasetrack_img',
					'type' => 'image'
				)
		
			)
		)
	);




	/**
	 * If the Chart Vote plugin is active, a new field will be added to control the voting
	 */
	if(function_exists('qt_chartvote_active')){
		$fields_chart[0]['repeatable_fields'][] = array(
			'label' => 'Track rating',
			'desc'	=> 'User Rating for the track', // description
			'id' 	=> 'releasetrack_rating',
			'type' 	=> 'number'
		);
	}


	if(class_exists("custom_add_meta_box")){
		$qt_kentharadio_tracks_box = new custom_add_meta_box( 'qt_kentharadio_tracks_box', 'Chart Tracks', $fields_chart, 'chart', true );
	}

	
}
add_action('wp_loaded', 'qt_kentharadio_chart_capabilities');  
