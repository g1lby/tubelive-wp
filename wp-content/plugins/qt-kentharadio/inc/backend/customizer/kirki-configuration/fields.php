<?php  
/**
 * Create customizer fields for the kirki framework.
 * @package Kirki
 */


kentharadioKirki_Kirki::add_field( 'kentharadio_config', array(
	'type'        => 'radio',
	'settings'    => 'QT_timing_settings',
	'label'       => esc_html__( 'Time format for schedule', "qt-kentharadio" ),
	'section'     => 'kentharadio_section',
	'default'     => '12',
	'priority'    => 10,
	'choices'     => array(
		'24'  => esc_attr__( '24h format', "qt-kentharadio" ),
		'12' => esc_attr__( '12h format', "qt-kentharadio" )
	)
));