<?php  
/**
 * Configuration for the Kirki Customizer.
 * @package Kirki
 */


kentharadioKirki_Kirki::add_config( 'kentharadio_config', array(
	'capability'    => 'edit_theme_options',
	'option_type'   => 'theme_mod'
) );



if(!function_exists('kentharadio_kirki_configuration')){
function kentharadio_kirki_configuration( $config ) {
	return wp_parse_args( array (
		'disable_loader' => true
	), $config );
}}

add_filter( 'kirki/config', 'kentharadio_kirki_configuration' );