<?php  
/**
 * Create sections using the WordPress Customizer API.
 * @package Kirki
 */
if(!function_exists('kentharadio_kirki_sections')){
function kentharadio_kirki_sections( $wp_customize ) {
	/**
	 * Player settings with sub panel ============================================================
	 */
	$wp_customize->add_section( 'kentharadio_section', array(
		'title'       => __( 'KenthaRadio Options', 'vlogger' ),
		'description' => __( 'Manage settings for Kentha Radio', 'vlogger' ),
		'priority'    => 0,
	));


	
}}
add_action( 'customize_register', 'kentharadio_kirki_sections' );
