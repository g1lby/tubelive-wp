<?php
/*
Package: qt-kentharadio
*/

if(!function_exists('qt_kentharadio_scheduleday')){
function qt_kentharadio_scheduleday($current_show_ID){
	ob_start();	
	/**
	 * ================================================
	 * Dynamic template inclusion (can be overridden by the theme)
	 * [$file name of the php template file]
	 * 
	 *	This function allows to override local plugin template and use the one in the theme
	 *	such as WooCommerce and Page Builder does.
	 *
	 * @var string
	 */
	$file = 'schedule-day.php';
	$template = locate_template( qt_kentharadio_override_template_path() . $file );
	if ( !$template ){
		$template = plugin_dir_path(  __FILE__ ) .'../../' .qt_kentharadio_local_template_path() . $file ;
	}
	include $template;
	/**
	 * End of template inclusion
	 * ================================================*/
	
	return ob_get_clean();
}}



