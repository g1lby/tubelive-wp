<?php
/*
Package: qt-kentharadio
*/

if(!function_exists('qt_kentharadio_subtitle')){
function qt_kentharadio_subtitle($current_show_ID){
	?>
	<h4 class="qt-subtitle">
	   <?php echo esc_attr(get_post_meta($current_show_ID, "subtitle", true)); ?>
	</h4>
	<?php  
}}

if(!function_exists('qt_kentharadio_subtitle2')){
function qt_kentharadio_subtitle2($current_show_ID){
	?>
	<h4 class="qt-subtitle">
	   <?php echo esc_attr(get_post_meta($current_show_ID, "subtitle2", true)); ?>
	</h4>
	<?php  
}}

