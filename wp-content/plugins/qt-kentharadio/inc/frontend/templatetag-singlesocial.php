<?php
/*
Package: qt-kentharadio
*/

if(!function_exists('qt_kentharadio_singlesocial')){
function qt_kentharadio_singlesocial ($current_show_ID){
	$show_title = get_the_title($current_show_ID);
	$social = array(
		'facebook',
		'amazon',
		'blogger',
		'behance',
		'bebo',
		'flickr',
		'pinterest',
		'rss',
		'triplevision',
		'tumblr',
		'twitter',
		'vimeo',
		'wordpress',
		'whatpeopleplay',
		'youtube',
		'soundcloud',
		'myspace',
		'googleplus',
		'itunes',
		'juno',
		'lastfm',
		'linkedin',
		'mixcloud',
		'resident-advisor',
		'reverbnation'
		);

	$id = get_the_ID();
	?>
	
		<?php
		foreach($social as $s){
			$i = get_post_meta($id, $s, true);
			if($i != ''){
			echo '<a href="'.esc_url($i).'" class="qt-kentharadio-singlesocial__btn qw-disableembedding qt-btn qt-btn-m qt-btn-secondary"  target="_blank"><i class="qt-socicon-'.esc_attr($s).'"></i></a>';
			} 
		}
		?>
	
	<?php  
}}