<?php  
/*
Package: qt-kentharadio
Description: Shortcode for creating app icons
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/

if(!function_exists('qt_kentharadio_playbutton_shortcode')){
	function qt_kentharadio_playbutton_shortcode ($atts){
		extract( shortcode_atts( array(
			'text' => 'click',
			'link' => '#',
			'size' => 'qt-btn-s',
			'style' => 'qt-btn-default',
			'radiochannel' => false,
			'alignment' => '',
			'class' => ''
		), $atts ) );

		ob_start();
		
		if(!$radiochannel || $radiochannel == '') {
			return;
		}
		/**
		 * Playable radio stream
		 * 
		 */
		$id = $radiochannel;
		$mp3_stream_url = get_post_meta( $id, 'mp3_stream_url', true );
		$subtitle = get_post_meta($id, 'qt_radio_subtitle',true);
		if( $mp3_stream_url ){
			$icon = get_post_meta($id, 'qt_player_icon', true );
			$thumb = get_the_post_thumbnail_url( $id, 'medium' );
			?>
			<ul class="qt-kentharadio-playbuttonlist <?php if ( $alignment == 'aligncenter' ) { ?> aligncenter <?php } ?>">
				<li class="qtmusicplayer-trackitem">
					<span class="qt-play qt-link-sec qtmusicplayer-play-btn qt-btn <?php  echo esc_attr($size.' '.$class.' '.$style.' '.$alignment); ?>" 
					data-qtmplayer-type="radio"
					data-qtmplayer-cover="<?php echo esc_url($thumb); ?>" 
					data-qtmplayer-file="<?php echo esc_url( $mp3_stream_url ); ?>" 
					data-qtmplayer-title="<?php echo esc_attr( get_the_title() ); ?>" 
					data-qtmplayer-artist="<?php  echo esc_attr( $subtitle ); ?>" 
					data-qtmplayer-album="<?php echo esc_attr(get_the_title()); ?>" 
					data-qtmplayer-link="<?php the_permalink(); ?>" 
					data-qtmplayer-buylink="" 
					data-qtmplayer-icon="<?php echo esc_attr($icon); ?>" 
					<?php  
					/**
					 * Radio feed reading
					 */
					?>
					data-radiochannel
					data-host="<?php echo esc_attr(get_post_meta( $id, 'qtradiofeedHost',true )); ?>"
					data-port="<?php echo esc_attr(get_post_meta( $id, 'qtradiofeedPort',true )); ?>"
					data-icecasturl="<?php echo esc_attr(get_post_meta( $id, 'qticecasturl',true )); ?>"
					data-icecastmountpoint="<?php echo esc_attr(get_post_meta( $id, 'qticecastMountpoint',true )); ?>"
					data-radiodotco="<?php echo esc_attr(get_post_meta( $id, 'qtradiodotco',true )); ?>"
					data-airtime="<?php echo esc_attr(get_post_meta( $id, 'qtairtime',true )); ?>"
					data-radionomy="<?php echo esc_attr(get_post_meta( $id, 'qtradionomy',true )); ?>"
					data-textfeed="<?php echo esc_attr(get_post_meta( $id, 'qttextfeed',true )); ?>"
					data-channel="<?php echo esc_attr(get_post_meta( $id, 'qtradiofeedChannel',true )); ?>"

					><i class='material-icons'>play_circle_filled</i> <?php echo esc_html($text); ?></span>
				</li>
			</ul>
			<?php
		}
		return ob_get_clean();
	}
}
add_shortcode("qt-kentharadio-playbutton","qt_kentharadio_playbutton_shortcode");

/**
 *  Query public radio channel to display array in button shortcode
 */
if(!function_exists('qt_kentharadio_radiochannels_options_array')){
function qt_kentharadio_radiochannels_options_array (){
	$posts = get_posts( array( 'post_type' => 'radiochannel', 'posts_per_page' => -1, 'orderby' => 'name', 'order' => 'ASC' ) );
	$options = array('Choose one' => false);
	foreach ( $posts as $item ){
		$options[ $item->post_title ] = $item->ID;
	}
	// print_r( $options );
	return $options;
}}

/**
 *  Visual Composer integration
 */
add_action( 'vc_before_init', 'qt_kentharadio_playbutton_shortcode_vc' );
if(!function_exists('qt_kentharadio_playbutton_shortcode_vc')){
function qt_kentharadio_playbutton_shortcode_vc() {
  vc_map( array(
	"name" 			=> esc_html__( "Radio Play Button", "qt-kentharadio" ),
	"base" 			=> "qt-kentharadio-playbutton",
	"icon" 			=> qt_kentharadio_plugin_url(). '/assets/img/radio-play-button.png',
	"description" 	=> esc_html__( "Button to play a radio channel", "qt-kentharadio" ),
	"category" 		=> esc_html__( "QT KenthaRadio Shortcodes", "qt-kentharadio"),
	"params" 		=> array(
			array(
				'type' 		=> 'textfield',
				'value' 	=> '',
				'heading' 	=> 'Text',
				'param_name'=> 'text',
			),
			array(
				'type' 		=> 'dropdown',
				'value' 	=> '',
				'heading'	=> 'Radio channel',
				'param_name'=> 'radiochannel',
				'description'=> esc_html__( 'Which radio to play when pressing button. If already playing it will pause', 'qt-kentharadio' ),
				'value' 	=> qt_kentharadio_radiochannels_options_array()
			),
			array(
				"type" 		=> "dropdown",
				"heading" 	=> esc_html__( "Size", "qt-kentharadio" ),
				"param_name"=> "size",
				'value' 	=> array(
						"Small" 		=> "qt-btn-s",
						"Medium" 		=> "qt-btn-m",
						"Large" 		=> "qt-btn-l", 
						"Extra large" 	=> 'qt-btn-xl'
					),
				"description" => esc_html__( "Button size", "qt-kentharadio" )
			),
			array(
				"type" 		=> "dropdown",
				"heading" 	=> esc_html__( "Button style", "qt-kentharadio" ),
				"param_name"=> "style",
				'value' 	=> array( 
					esc_html__("Default","qt-kentharadio") 	=> "qt-btn-default",
					esc_html__("Primary","qt-kentharadio") 	=> "qt-btn-primary",
					esc_html__("Secondary","qt-kentharadio") => "qt-btn-secondary",
					esc_html__("Ghost","qt-kentharadio") 	=> "qt-btn-ghost",
					)			
				),
			array(
				"type" 		=> "dropdown",
				"heading" 	=> esc_html__( "Alignment", "qt-kentharadio" ),
				"param_name"=> "alignment",
				'value' 	=> array( 
					esc_html__("Default","qt-kentharadio") 	=> "",
					esc_html__("Left","qt-kentharadio") 		=> "alignleft",
					esc_html__("Right","qt-kentharadio") 	=> "alignright",
					esc_html__("Center","qt-kentharadio") 	=> "aligncenter",
					),
				"description" => esc_html__( "Button style", "qt-kentharadio" )
			),
			array(
				"type" 			=> "textfield",
				"heading" 		=> esc_html__( "Class", "qt-kentharadio" ),
				"param_name" 	=> "class",
				'value' 		=> '',
				'description' 	=> 'add an extra class for styling with CSS'
			)
		)
  	));
}}