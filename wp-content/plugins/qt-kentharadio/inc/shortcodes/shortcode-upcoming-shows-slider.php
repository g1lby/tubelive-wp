<?php
/*
Package: qt-kentharadio
Description: Upcoming shows carousel
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/


if(!function_exists('qt_kentharadio_upcoming_slider')) {
	function qt_kentharadio_upcoming_slider($atts){

		extract( shortcode_atts( array(
			'schedulefilter' => '',
			'title' 		=> false,
			'design' 		=> 'default',
			'grid_id'		=> false, 
		), $atts ) );

		$date = current_time("Y-m-d");
		$current_dayweek = current_time("D");
		$tomorrow_dayweek = date('D', time()+86400);//current_time("D"+1);

		$total = 0;
		$tabsArray = array();
		$id_of_currentday = 0;
		$now = current_time("H:i");

		$args = array(
			'post_type' => 'schedule',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'orderby' => 'menu_order',
			'order'   => 'ASC'
		);
		if( isset($schedulefilter) ) {
			if($schedulefilter !== ''){
				$args ['tax_query'] = array(
					array(
						'taxonomy' => 'schedulefilter',
						'field'    => 'slug',
						'terms'    => esc_attr($schedulefilter)
					)
				);
			}
		}
		wp_reset_postdata();
		/**
		 * ====================================================================================================
		 * Update from 2017 September 10
		 * adding week-of-the-month filtering if enabled
		 */
		$week_num = qt_kentharadio_week_number();
		$qt_execute_week_control = get_theme_mod('QT_monthly_schedule', '0' );
		if(get_theme_mod('QT_monthly_schedule', '0' )){
			$week_num = qt_kentharadio_week_number();
			$args ['meta_key'] = 'month_week';
			$args ['meta_value'] = $week_num;
			$args['meta_compare'] = 'LIKE';
		}

		/* =========================================== update end ===========================================*/

		$the_query_meta = new WP_Query( $args );

		while ( $the_query_meta->have_posts() && $id_of_currentday == 0 ):
			
			$the_query_meta->the_post();

			/**
			 * ====================================================================================================
			 * Update from 2017 September 10
			 * adding week-of-the-month filtering if enabled
			 */
			$can_display_this_schedule = true;
			if($qt_execute_week_control  == '1'){
				$can_display_this_schedule = false;
		 		$schedule_weeks = get_post_meta( $the_query_meta->post->ID, 'month_week', true );
		 		if(is_array($schedule_weeks)){
		 			foreach ($schedule_weeks as $w){
		 				if($w == $week_num){
		 					$can_display_this_schedule = true;
		 				}
		 			}
		 		}
		 	}
		 	/* =========================================== update end ===========================================*/


		 	if(true === $can_display_this_schedule){ // check added for 2017-09-10 week schedule update
			 	$active = '';
				$maincolor = '';
				$total++;
				// setup_postdata( $post );
				/*
				*
				*	Create the array for making the content
				*
				**/
				$tab = array('name' => $the_query_meta->post->post_name,
							'title' => $the_query_meta->post->post_title,
							'id' => $the_query_meta->post->ID);
				/*
				*
				*	Find out if is a day of the calendar
				*
				*/
				$schedule_date = get_post_meta($the_query_meta->post->ID, 'specific_day', true);
				$schedule_week_day = get_post_meta($the_query_meta->post->ID, 'week_day', true);
				/*
				1. Find which is the current day, otherwise random shows will be shown
				*/
				if($schedule_date == $date){
					$id_of_currentday = $the_query_meta->post->ID;
				} else {


					/*
					2. check if is this day of the week
					*/
					if(is_array($schedule_week_day)){
						
						foreach($schedule_week_day as $day){ // each schedule can fit multiple days
							if(strtolower($day) == strtolower($current_dayweek)){
								$id_of_currentday = $the_query_meta->post->ID;

							}
						}
					}
				}
			}
		endwhile;
		

		$qt_kentharadio_today_name_title = get_the_title($id_of_currentday);//$the_query_meta->post->post_title;

		if($id_of_currentday != 0){
			ob_start();


			$events= get_post_meta($id_of_currentday, 'track_repeatable', true); 

			if(is_array($events)){
				$maximum = 9;
				$total = 1;
				
				/**
				 * ================================================
				 * Dynamic template inclusion (can be overridden by the theme)
				 * [$file name of the php template file]
				 * 
				 *	This function allows to override local plugin template and use the one in the theme
				 *	such as WooCommerce and Page Builder does.
				 *
				 * @var string
				 */
				
				$file = 'show-slider.php';
				$template = locate_template( qt_kentharadio_override_template_path() . $file );
				if ( !$template ){
					$template = plugin_dir_path(  __FILE__ ) .'../../' .qt_kentharadio_local_template_path() . $file ;
				}
				include $template;
				/**
				 * End of template inclusion
				 * ================================================*/


			} else {
				echo esc_attr__("Sorry, there are no shows scheduled on this day","qt-kentharadio");
			}
			wp_reset_postdata();
			return ob_get_clean();
		}
	}
}	
add_shortcode("qt-upcomingslider","qt_kentharadio_upcoming_slider");


/**
 *  Visual Composer integration
 */
add_action( 'vc_before_init', 'qt_kentharadio_vc_upcoming_slider' );
if(!function_exists('qt_kentharadio_vc_upcoming_slider')){
function qt_kentharadio_vc_upcoming_slider() {
  vc_map( array(
     "name" => esc_html__( "Upcoming Shows Slider", "qt-kentharadio" ),
     "base" => "qt-upcomingslider",
     "icon" => qt_kentharadio_plugin_url(). '/assets/img/radio-upcoming-shows-slider.png',
     "description" => esc_html__( "Display fullpage slider of upcoming shows", "qt-kentharadio" ),
     "category" => esc_html__( "QT KenthaRadio Shortcodes", "qt-kentharadio"),
     "params" => array(
     	array(
		   "type" => "textfield",
		   "heading" => esc_html__( "Title", "kentha" ),
		   "param_name" => "title",
		   'value' => false
		),
      	array(
           "type" => "textfield",
           "heading" => esc_html__( "Filter by 'schedulefilter' taxonomy", "qt-kentharadio" ),
           "description" => esc_html__("Insert the slug of a schedulefilter taxonomy for multi-radio websites","qt-kentharadio"),
           "param_name" => "schedulefilter"
        ),
        array(
			'type' => 'vc_grid_id',
			'param_name' => 'grid_id',
		),
     )
  ) );
}}