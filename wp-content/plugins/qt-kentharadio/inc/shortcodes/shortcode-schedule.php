<?php  
/*
Package: qt-kentharadio
Full schedule shortcode
*/


add_shortcode("qt-schedule","qt_kentharadio_schedule");



/**
 *  Visual Composer integration
 */
add_action( 'vc_before_init', 'qt_kentharadio_vc_showgrid' );
if(!function_exists('qt_kentharadio_vc_showgrid')){
function qt_kentharadio_vc_showgrid() {
  vc_map( array(
     "name" => esc_html__( "Shows schedule", "qt-kentharadio" ),
     "base" => "qt-schedule",
     "icon" => qt_kentharadio_plugin_url(). '/assets/img/radio-shows-schedule.png',
     "description" => esc_html__( "Full calendar of the schedule", "qt-kentharadio" ),
     "category" => esc_html__( "QT KenthaRadio Shortcodes", "qt-kentharadio"),
     "params" => array(
      	array(
           "type" => "textfield",
           "heading" => esc_html__( "Filter by 'schedulefilter' taxonomy", "qt-kentharadio" ),
           "description" => esc_html__("Insert the slug of a schedulefilter taxonomy for multi-radio websites","qt-kentharadio"),
           "param_name" => "schedulefilter"
        )
     )
  ) );
}}