<?php  
/*
Package: qt-kentharadio
Description: Shortcode for creating song title text
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/


if(!function_exists('qt_kentharadio_short_songtitle')){
	function qt_kentharadio_short_songtitle ($atts){
		extract( shortcode_atts( array(
			'title' => '',
			'tag' => false,
			'align' => '',
			'class' => ''
		), $atts ) );

		if(!$tag){
			$tag = 'p';
		}
		ob_start();
		?>
			<?php echo '<'.esc_attr($tag); ?> class="qt-kentharadio-songtitle-shortcode qt-kentharadio-feed <?php echo esc_attr($class.' '.$align); ?>">
			<?php  echo esc_html($title); ?><span class="qt-kentharadio-feed__author"></span> - <span class="qt-kentharadio-feed__title"></span>
			<?php echo '</p'.esc_attr($tag).'>'; ?>
		<?php
		return ob_get_clean();
	}
}
add_shortcode("qt-kentharadio-songtitle","qt_kentharadio_short_songtitle");



/**
 *  Visual Composer integration
 */
add_action( 'vc_before_init', 'qt_kentharadio_short_songtitle_vc' );
if(!function_exists('qt_kentharadio_short_songtitle_vc')){
function qt_kentharadio_short_songtitle_vc() {
	vc_map( array(
		 "name" => esc_html__( "Now On Air text", "qt-kentharadio" ),
		 "base" => "qt-kentharadio-songtitle",
		 "icon" => qt_kentharadio_plugin_url(). '/assets/img/radio-song-title.png',
		 "description" => esc_html__( "Display the song title if enabled in radio channel and currently playing", "qt-kentharadio" ),
		 "category" => esc_html__( "QT KenthaRadio Shortcodes", "qt-kentharadio"),
		 "params" => array(
			array(
				 "type" => "textfield",
				 "heading" => esc_html__( "Title", "qt-kentharadio" ),
				 "param_name" => "title",
				 'value' => ''
			),	
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Container", "qt-kentharadio" ),
				"param_name" => "tag",
				'value' => array("p", "H1","H2","H3","H4","H5","H6",),
				"description" => esc_html__( "Container style", "qt-kentharadio" )
			),	
			array(
				"type" => "dropdown",
				"heading" => esc_html__( "Alignment", "qt-kentharadio" ),
				"param_name" => "align",
				'value' => array("left" => 'qt-left', "center" => 'qt-center',"right" => 'qt-right'),
			),		
			array(
				"type" => "textfield",
				"heading" => esc_html__( "CSS class", "qt-kentharadio" ),
				"param_name" => "class",
				'value' => ''
			),	
		 )
	) );
}}