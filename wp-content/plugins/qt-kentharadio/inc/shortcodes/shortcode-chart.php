<?php 
/*
Package: qt-kentharadio
Description: part chart teaser (limited tracks)
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/

if(!function_exists('qt_kentharadio_chart_tracklist')) {
function qt_kentharadio_chart_tracklist($atts){
	extract( shortcode_atts( array(
			'id' => false,
			'number' => 100,
			'showtitle' => false,
			'chartcategory' => false,
			'chartstyle' => 'chart-normal',
			'showthumbnail' => false
	), $atts ) );

	if(!is_numeric($number)){
		$number = 100;
	}
	if(!is_numeric($id)){
		$id = false;
	}
	if(false == $id){
		$args = array(
			'post_type' => 'chart',
			'posts_per_page' => 1,
			'post_status' => 'publish',
			'orderby' => array ( 'menu_order' => 'ASC', 'date' => 'DESC'),
			'paged' => 1,
			'suppress_filters' => false,
			'ignore_sticky_posts' => 1
		);

		if(false !== $chartcategory && '' !== $chartcategory){
			$args[ 'tax_query'] = array(
					array(
					'taxonomy' => 'chartcategory',
					'field' => 'slug',
					'terms' => array(esc_attr($chartcategory)),
					'operator'=> 'IN' //Or 'AND' or 'NOT IN'
				)
			);
		}
	} else {
		$args = array(
		'p' => esc_attr($id), 
		'post_type' => 'chart');
	}
	
	$wp_query = new WP_Query( $args );
	if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
		ob_start();
		$events= get_post_meta(get_the_ID(), 'track_repeatable', true);   
		$total = count($events);
		if(is_array($events)){


			if($showtitle){
				?>
				<h4 class="qt-title">
				<?php the_title(); ?>
				</h4>
				<?php
			}
			if($showthumbnail){
				?><a href="<?php the_permalink(); ?>" alt="<?php esc_attr_e("Chart image", 'qt-kentharadio'); ?>" class="qt-short-chart-featured"><?php  
				the_post_thumbnail('medium', array("class" => "qt-featuredimage") );
				?></a><?php  
			}

			$pos = 1;
			$counter = 0;
			$trackid = 0;


			/**
			 * ================================================
			 * Dynamic template inclusion (can be overridden by the theme)
			 * [$file name of the php template file]
			 * 
			 *	This function allows to override local plugin template and use the one in the theme
			 *	such as WooCommerce and Page Builder does.
			 *
			 * @var string
			 */
			
			$file = 'chart-tracklist.php';
			$template = locate_template( qt_kentharadio_override_template_path() . $file );
			if ( !$template ){
				$template = plugin_dir_path(  __FILE__ ) .'../../' .qt_kentharadio_local_template_path() . $file ;
			}
			include $template;
			/**
			 * End of template inclusion
			 * ================================================*/

		}//end debuf if


		/**
		 *
		 *	If the total amount of tracks is more than the number we show, add button to single chart page
		 * 
		 */
		if($total > $number){
			?>
			<p class="aligncenter">
				<a class="qt-btn <?php if($chartstyle == "chart-normal") { ?>qt-btn-l<?php } ?> qt-btn-primary" href="<?php the_permalink(); ?>"><?php echo esc_attr__("Full tracklist","qt-kentharadio"); ?></a>
			</p>
			<?php
		}
		
	endwhile; endif;
	wp_reset_postdata();
	return ob_get_clean();
}}

add_shortcode("qt-chart","qt_kentharadio_chart_tracklist");


/**
 *  Visual Composer integration
 */
add_action( 'vc_before_init', 'qt_kentharadio_vc_qt_kentharadio_chart_tracklist' );
if(!function_exists('qt_kentharadio_vc_qt_kentharadio_chart_tracklist')){
function qt_kentharadio_vc_qt_kentharadio_chart_tracklist() {
  vc_map( array(
	 "name" => esc_html__( "Chart tracks", "qt-kentharadio" ),
	 "base" => "qt-chart",
	 "icon" => qt_kentharadio_plugin_url(). '/assets/img/radio-chart-track.png',
	 "description" => esc_html__( "Display the tracks of the latest chart or specify a chart by id.", "qt-kentharadio" ),
	 "category" => esc_html__( "QT KenthaRadio Shortcodes", "qt-kentharadio"),
	 "params" => array(
		array(
		   "type" => "textfield",
		   "heading" => esc_html__( "ID", "qt-kentharadio" ),
		   "description" => esc_html__( "Optional Chart ID, if not specified will always show the latest chart by menu order or publish date", "qt-kentharadio" ),
		   "param_name" => "id",
		   'value' => ''
		),
		array(
		   "type" => "textfield",
		   "heading" => esc_html__( "Number of tracks (default: 100)", "qt-kentharadio" ),
		   "description" => esc_html__( "Optional Chart ID, if not specified will always show the latest chart by menu order or publish date", "qt-kentharadio" ),
		   "param_name" => "number",
		),
		array(
		   "type" => "checkbox",
		   "heading" => esc_html__( "Show chart title", "qt-kentharadio" ),
		   "description" => esc_html__( "Display the title of the chart", "qt-kentharadio" ),
		   "param_name" => "showtitle",
		),
		array(
		   "type" => "checkbox",
		   "heading" => esc_html__( "Featured image", "qt-kentharadio" ),
		   "description" => esc_html__( "Display the image linked to the full chart", "qt-kentharadio" ),
		   "param_name" => "showthumbnail",
		),
		array(
		   "type" => "dropdown",
		   "heading" => esc_html__( "Chart style", "qt-kentharadio" ),
		   "param_name" => "chartstyle",
		   'value' => array("chart-normal","chart-small"),
		   "description" => esc_html__( "Chart visualization style", "qt-kentharadio" )
		)
	 )
  ) );
}}
