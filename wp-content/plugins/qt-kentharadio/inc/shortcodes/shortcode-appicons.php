<?php  
/*
Package: qt-kentharadio
Description: Shortcode for creating app icons
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/



if(!function_exists('qt_kentharadio_appicons')){
	function qt_kentharadio_appicons ($atts){
		extract( shortcode_atts( array(
			'app_android' => '',
			'app_iphone' => '',
			'app_blackberry' => '',
			'app_itunes' => '',
			'app_winphone' => '',
			'app_winamp' => '',
			'app_tunein' => '',
			'app_mediaplayer' => ''
		), $atts ) );
		ob_start();
		?>
			<p class="qt-center qt-kentharadio-appicons">
			<?php 
			foreach ($atts as $var => $val){

				if($val !== '') {
					echo '<a href="'.esc_url($val).'" class="qt-btn qt-btn-primary qt-btn-l qt-kentharadio-appicons__btn"><span class="qt-kentharadio-'.$var.'"></span></a>';
				}
			}  
			?>
			</p>
		<?php
		return ob_get_clean();
	}
}
add_shortcode("qt-appicons","qt_kentharadio_appicons");



/**
 *  Visual Composer integration
 */
add_action( 'vc_before_init', 'qt_kentharadio_vc_appicons' );
if(!function_exists('qt_kentharadio_vc_appicons')){
function qt_kentharadio_vc_appicons() {
	vc_map( array(
		 "name" => esc_html__( "App Icons", "qt-kentharadio" ),
		 "base" => "qt-appicons",
		 "icon" => qt_kentharadio_plugin_url(). '/assets/img/radio-app-icons.png',
		 "description" => esc_html__( "Create links to external players", "qt-kentharadio" ),
		 "category" => esc_html__( "QT KenthaRadio Shortcodes", "qt-kentharadio"),
		 "params" => array(

				array(
					 "type" => "textfield",
					 "heading" => esc_html__( "Android", "qt-kentharadio" ),
					 "param_name" => "app_android",
					 'value' => ''
				),
				array(
					 "type" => "textfield",
					 "heading" => esc_html__( "iPhone", "qt-kentharadio" ),
					 "param_name" => "app_iphone",
					 'value' => ''
				),
				array(
					 "type" => "textfield",
					 "heading" => esc_html__( "Blackberry", "qt-kentharadio" ),
					 "param_name" => "app_blackberry",
					 'value' => ''
				),
				array(
					 "type" => "textfield",
					 "heading" => esc_html__( "iTunes", "qt-kentharadio" ),
					 "param_name" => "app_itunes",
					 'value' => ''
				),
				array(
					 "type" => "textfield",
					 "heading" => esc_html__( "Windows Phone", "qt-kentharadio" ),
					 "param_name" => "app_winphone",
					 'value' => ''
				),
				array(
					 "type" => "textfield",
					 "heading" => esc_html__( "Media Player", "qt-kentharadio" ),
					 "param_name" => "app_mediaplayer",
					 'value' => ''
				),
				array(
					 "type" => "textfield",
					 "heading" => esc_html__( "Winamp", "qt-kentharadio" ),
					 "param_name" => "app_winamp",
					 'value' => ''
				),
				 array(
					 "type" => "textfield",
					 "heading" => esc_html__( "Tunein", "qt-kentharadio" ),
					 "param_name" => "app_tunein",
					 'value' => ''
				),
				
		 )
	) );
}}