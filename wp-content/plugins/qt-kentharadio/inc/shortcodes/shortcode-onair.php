<?php
/*
Package: qt-kentharadio
Description: Shows slideshow
Version: 0.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/


if(!function_exists('qt_kentharadio_onair')) {
	function qt_kentharadio_onair($atts){
		extract( shortcode_atts( array(
			'schedulefilter' 	=> '',
			'parallax' 			=> false,
			'grid_id'			=> false, 
		), $atts ) );

		$date = current_time("Y-m-d");
		$current_dayweek = current_time("D");
		$total = 0;
		$tabsArray = array();
		$id_of_currentday = 0;
		$now = current_time("H:i");

		$args = array(
			'post_type' => 'schedule',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'orderby' => 'menu_order',
			'order'   => 'ASC',
		);
		if( isset($schedulefilter) ) {
			if($schedulefilter !== ''){
				$args ['tax_query'] = array(
					array(
						'taxonomy' => 'schedulefilter',
						'field'    => 'slug',
						'terms'    => $schedulefilter
					)
				);
			}
		}
		wp_reset_postdata();
		


		/**
		 * ====================================================================================================
		 * Update from 2017 September 10
		 * adding week-of-the-month filtering if enabled
		 */
		$week_num = qt_kentharadio_week_number();
		$qt_execute_week_control = get_theme_mod('QT_monthly_schedule', '0' );
		if(get_theme_mod('QT_monthly_schedule', '0' )){
			$week_num = qt_kentharadio_week_number();
			$args ['meta_key'] = 'month_week';
			$args ['meta_value'] = $week_num;
			$args['meta_compare'] = 'LIKE';
		}

		/* =========================================== update end ===========================================*/





		$the_query_meta = new WP_Query( $args );

		while ( $the_query_meta->have_posts() && $id_of_currentday == 0 ):
			$the_query_meta->the_post();
			$post = $the_query_meta->post;
			setup_postdata( $post );

			/**
			 * ====================================================================================================
			 * adding week-of-the-month filtering if enabled
			 * ====================================================================================================
			 */
			$can_display_this_schedule = true;
			if($qt_execute_week_control  == '1'){
				$can_display_this_schedule = false;
		 		$schedule_weeks = get_post_meta( $the_query_meta->post->ID, 'month_week', true );
		 		if(is_array($schedule_weeks)){
		 			foreach ($schedule_weeks as $w){
		 				if($w == $week_num){
		 					$can_display_this_schedule = true;
		 				}
		 			}
		 		}
		 	}
		 	/* =========================================== update end ===========================================*/


		 	if(true === $can_display_this_schedule){ // check added for 2017-09-10 week schedule update

			 	$active = '';
				$maincolor = '';
				$total++;
				/*
				*
				*	Create the array for making the content
				*
				**/
				$tab = array('name' => $post->post_name,
							'title' => $post->post_title,
							'id' => $post->ID);
				/*
				*
				*	Find out if is a day of the calendar
				*
				*/
				$schedule_date = get_post_meta($post->ID, 'specific_day', true);
				$schedule_week_day = get_post_meta($post->ID, 'week_day', true);
				/*
				1. Find which is the current day, otherwise random shows will be shown
				*/
				if($schedule_date == $date){
					$id_of_currentday = $post->ID;
				} else {
					/*
					2. check if is this day of the week
					*/
					if(is_array($schedule_week_day)){
						foreach($schedule_week_day as $day){ // each schedule can fit multiple days
							if(strtolower($day) == strtolower($current_dayweek)){
								//echo 'Found'.$post->ID;
								$id_of_currentday = $post->ID;

							}
						}
					}
				}
			}
		endwhile;

		if($id_of_currentday != 0){
			ob_start();
			$events= get_post_meta($post->ID, 'track_repeatable', true);   
			if(is_array($events)){
				$maximum = 1;
				$total = 1;
				foreach($events as $event){ 
					$neededEvents = array('show_id','show_time','show_time_end');
					foreach($neededEvents as $n){
					  if(!array_key_exists($n,$events)){
						  $events[$n] = '';
					  }
					}
					$show_id = $event['show_id'][0];
					
					$show_time = $event['show_time'];
					$show_time_end = $event['show_time_end'];
					if($show_time_end == "00:00"){
						$show_time_end = "24:00";
					}
					if($now < $show_time_end && $total <= $maximum){
						$total ++;

						$show_time_d = $show_time;
						$show_time_end_d = $show_time_end;
						// 12 hours format
						if(get_theme_mod('QT_timing_settings', '12') == '12'){
							$show_time_d = date("g:i a", strtotime($show_time_d));
							$show_time_end_d = date("g:i a", strtotime($show_time_end_d));
						}

						

						/**
						 * ================================================
						 * Dynamic template inclusion (can be overridden by the theme)
						 * [$file name of the php template file]
						 * 
						 *	This function allows to override local plugin template and use the one in the theme
						 *	such as WooCommerce and Page Builder does.
						 *
						 * @var string
						 */
						
						$file = 'show-hero.php';
						$template = locate_template( qt_kentharadio_override_template_path() . $file );
						if ( !$template ){
							$template = plugin_dir_path(  __FILE__ ) .'../../' .qt_kentharadio_local_template_path() . $file ;
						}
						include $template;
						/**
						 * End of template inclusion
						 * ================================================*/
						

					}
					wp_reset_postdata();
				}//foreach
			} else {
				echo esc_attr__("Sorry, there are no shows scheduled on this day","qt-kentharadio");
			}
			wp_reset_postdata();
		}
		return ob_get_clean();
	}
}

add_shortcode("qt-onair","qt_kentharadio_onair");



/**
 *  Visual Composer integration
 */
add_action( 'vc_before_init', 'qt_kentharadio_vc_onair' );
if(!function_exists('qt_kentharadio_vc_onair')){
function qt_kentharadio_vc_onair() {
  vc_map( array(
     "name" => esc_html__( "Show on air", "qt-kentharadio" ),
     "base" => "qt-onair",
     "icon" => qt_kentharadio_plugin_url(). '/assets/img/radio-show-on-air.png',
     "description" => esc_html__( "Display a hero section of the show actually playing", "qt-kentharadio" ),
     "category" => esc_html__( "QT KenthaRadio Shortcodes", "qt-kentharadio"),
     "params" => array(
      	array(
           "type" => "textfield",
           "heading" => esc_html__( "Filter by 'schedulefilter' taxonomy", "qt-kentharadio" ),
           "description" => esc_html__("Insert the slug of a schedulefilter taxonomy for multi-radio websites","qt-kentharadio"),
           "param_name" => "schedulefilter"
        ),
        array(
			'type' => 'vc_grid_id',
			'param_name' => 'grid_id',
		),
     )
  ) );
}}