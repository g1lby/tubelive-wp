/**
* @package qt-kentharadio
*/

/**====================================================================
 *
 *  Main Script File
 *
 *====================================================================*/

/**
 * These are the files required for the main script to run.
 * They are already imported in the minified version qt-main-min.js
 * If you enable Debug option in customizer, the unminified separated version
 * will be included instead, and you can modify this current js file for your own customizations
 */

// IMPORTANT: THE FOLLOWINT IS NOT A COMMENT! IT IS JAVASCIPT IMPORTING! *** DO NOT DELETE ***
// ===========================================================================================
// @codekit-prepend "libs/_tabs.js"
// @codekit-prepend "libs/_shoutcast.js"

(function($) {
	"use strict";
	var qt_kentharadio_DebugEnabled = 0; // set to 1 to enable console logging

	$.qtKentharadioObj = {
		body: $("body"),
		window: $(window),
		document: $(document),
		htmlAndbody: $('html,body'),
		qtFeedData: {
			host: '',
			port:  '',
			channel:  '',
			icecast:  '',
			icecast_mount:  '',
			radiodotco: '',
			airtime: '',
			radionomy: '',
			textfeed: ''
		},
		/**
		 * ======================================================================================================================================== |
		 * 																																			|
		 * 																																			|
		 * START SITE FUNCTIONS 																													|
		 * 																																			|
		 *																																			|
		 * ======================================================================================================================================== |
		 */
		fn: {
			debug: function(msg){
				if(qt_kentharadio_DebugEnabled && console){
					if(typeof(console.log) === 'function'){
						console.log(msg);
					}
				}
			},
			
			

			/**====================================================================
			 *
			 * 
			 *  Ajax elements refresh
			 *  
			 * 
			 ====================================================================*/
			qtAjaxElementsRefresh: function(){
				var currentpageUrlBox = $("#qtcurrentpermalink");
				if(currentpageUrlBox.length <= 0){
					return;
				}
				var originalContainer, newContent, oldContent;
				var link = currentpageUrlBox.attr("data-permalink"),
					itemsToRefresh = new Array('.qt-autorefresh');
				$.ajax({
					url: link,
					success:function(data) {
						$.ajaxData = data;
						itemsToRefresh.forEach(function(theselector){
							$(theselector).animate({opacity: 0}, 0, function(){
								$(theselector).html( $($.ajaxData).find(theselector).html() ).animate({opacity: 1}, 0, function(){
									$.fn.dynamicBackgrounds(theselector);
									$.fn.slickGallery();
								});
							});
						});
					}
				});
			},
			

			/**====================================================================
			 *
			 * 
			 *  Dropdown tabs
			 *  
			 * 
			 ====================================================================*/
			dropDownTabs: function(){
				$( '#qt-kentharadio-ShowDropdown' ).change(function() {
					$("a#"+$(this).attr("value")).click();
				});
			},


			/**====================================================================
			*
			* SHOTCAST XML Feed Info
			* 
			====================================================================*/

			qtApplyTitle: function(result){
				if(result && result !== ''){
					var feedsplit = result.split(" - "), author, title;
					if(feedsplit.length > 1){
						author = feedsplit[0],
						title = feedsplit[1];
					} else {
						author = "";
						title = result;
					}
					$(".qt-kentharadio-feed").show();
					$('.qt-kentharadio-feed__author').html(author);
					$('.qt-kentharadio-feed__title').html(title);
					$('.qt-mplayer__artist').html(author+': '+title);
				} else {
					$(".qt-kentharadio-feed").hide();
				}
				return;
			},
			
			qtShoutcastFeed: function(){


				var o 	= $.qtKentharadioObj,
					fd = o.qtFeedData,
					fn 	= o.fn,
					qtradiofeedHost		=  (undefined !== fd.host) ? fd.host : '',
					qtradiofeedPort 	=  (undefined !== fd.port) ? fd.port : '',
					qtradiofeedProtocol = 	(undefined !== fd.ssl) ? '1' : '',
					theChannel 			=  (undefined !== fd.channel) ? fd.channel : '',
					qticecasturl 		=  (undefined !== fd.icecasturl) ? fd.icecasturl : '',
					qticecastmountpoint	=  (undefined !== fd.icecast_mount) ? fd.icecast_mount : '',
					qtradiodotco		=  (undefined !== fd.radiodotco) ? fd.radiodotco : '',
					qtairtime			=  (undefined !== fd.airtime) ? fd.airtime : '',
					qtradionomy 		=  (undefined !== fd.radionomy) ? fd.radionomy : '',
					qttextfeed 			=  (undefined !== fd.textfeed) ? fd.textfeed : '',
					qtFeedPlayerTrack = $('#qtFeedPlayerTrack'),
					qtFeedPlayerAuthor = $('#qtFeedPlayerAuthor'),
					qtPlayerTrackInfo = $("#qtPlayerTrackInfo"),
					qtSCuseproxy = false, // will add this option MAYBE in the future
					author, title, result, feedsplit;
					o = $.qtKentharadioObj;
					fd = o.qtFeedData;
					if(qttextfeed === '' && qtradionomy === '' && qtairtime === '' && (qtradiofeedHost === '' || qtradiofeedPort === '' || typeof(qtradiofeedHost) === 'undefined') && qticecasturl === '' && qtradiodotco === '') {
						fn.qtApplyTitle();
						return;
					} else {
						if(qttextfeed !== ''){
							var proxyURL = $("#qantumthemesproxyurl").data('proxyurl'),
									jsondata, title;
							$.ajax({
							   	type: 'GET',
								cache: false,
								url: proxyURL,
								async: true,
								data: { 
							        "qtproxycall": qttextfeed,
							    },
								dataType: "html",
								success: function(data) {
									fn.qtApplyTitle( data );
								},
								error: function(e){
								}
							});

						} else if(qtradionomy !== '' ) {
							$.ajax({
							   	type: 'GET',
								url: qtradionomy,
								async: true,
								cache: false,
								dataType: "xml",
								success: function(data) {
									fn.qtApplyTitle( $(data).find('artists').html()+ ' - ' + $(data).find('title').html() );
								},
								error: function(e){
								}
							});
						} else if(qtairtime !== '' && qtairtime !== 'undefined' && qtairtime !== undefined && typeof(qtairtime) !== 'undefined'){
							var proxyURL = $("#qantumthemesproxyurl").data('proxyurl'),
								jsondata, title;
							$.ajax({
								type: 'GET',
								cache: false,
								url: proxyURL,
								async: true,
								data: {
							        "qtproxycall": qtairtime,
							    },
								contentType: "application/json",
								success: function(data) {
									jsondata = JSON.parse(data);
									title = jsondata.tracks.current.name;
									fn.qtApplyTitle(title);
								},
								error: function(e) {
								}
							});

							
						}
						else if(qticecasturl !== '' && typeof(qticecasturl) !== 'undefined') {
							if(qticecastmountpoint !== '') {
								$.ajax({
									type: 'GET',
									url: qticecasturl,
									async: true,
									jsonpCallback: 'parseMusic',
									contentType: "application/json",
									dataType: 'jsonp',
									success: function(json) {

										if(qticecastmountpoint !== '') {
											result = json[qticecastmountpoint]['title'];
										} else if(json['icestats']['source']){
											result = (json['icestats']['source']['title']);
										}
										fn.qtApplyTitle(result);
									},
									error: function(e) {
									}
								});
							} else {
								var proxyURL = $("#qantumthemesproxyurl").data('proxyurl'),
									jsondata, title;
								$.ajax({
									type: 'GET',
									cache: false,
									url: proxyURL,
									async: true,
									data: { 
								        "qtproxycall": qticecasturl,
								    },
									contentType: "application/json",
									success: function(data) {
										jsondata = JSON.parse(data);
										if(jsondata.icestats.source.title){
											title = jsondata.icestats.source.title;
										} else if(jsondata.icestats.source[1].title) {
											title = jsondata.icestats.source[1].title;
										} else if(jsondata.icestats.source[0].title) {
											title = jsondata.icestats.source[0].title
										}
										fn.qtApplyTitle(title);
									},
									error: function(e) {
									}
								});

							}
						} else if (qtradiofeedHost !== '' && qtradiofeedPort !== '' && typeof(qtradiofeedHost) !== 'undefined'){

							var qtprotocol 	= 'http',
								qtscurl 	= 'http://'+qtradiofeedHost+':'+qtradiofeedPort+'/stats?sid='+theChannel+'&json=1',
								vproxyURL 	= $("#qantumthemesproxyurl").data('proxyurl'),
								jsondata, 
								title;





							if(qtSCuseproxy === '1'){
								// 2017 12 16 with ssl support
								$.ajax({
									type: 'GET',
									cache: false,
									url: proxyURL,
									async: true,
									data: { 
								        "qtproxycall": qtscurl,
								    },
									contentType: "application/json",
									success: function(data) {
										jsondata = JSON.parse(data);
										fn.qtApplyTitle(jsondata.songtitle);
									},
									error: function(e) {
									}
								});
							} else {

								/*
								$.ajax({
									type: 'GET',
									cache: false,
									url: qtscurl,
									async: true,
									contentType: "application/json",
									success: function(data) {
										jsondata = JSON.parse(data);
										fn.qtApplyTitle(jsondata.songtitle);
									},
									error: function(e) {
									}
								});
								*/	
								
								if( 1 === qtradiofeedProtocol ){
									qtprotocol = 'https';
								}
								$.SHOUTcast({
									host : qtradiofeedHost,
									port : qtradiofeedPort,
									protocol: qtprotocol,
									interval: 10000,
									stream : theChannel,
									stats : function(){
										if(this.onAir()){
											result = this.get('songtitle');
											fn.qtApplyTitle(result);
										}
									}
								}).startStats();
							}
						} else if (qtradiodotco !== '' && typeof(qtradiodotco) !== 'undefined'){
							var rUrl = 'https://public.radio.co/stations/'+qtradiodotco+'/status'
							$.ajax({
								type: 'GET',
								cache: false,
								url: rUrl,
								async: true,
								contentType: "application/json",
								success: function(data) {
									title = data['current_track']['title'];
									fn.qtApplyTitle(title);
								},
								error: function(e) {
								}
							});
						}
						
					}
					
				return;
			},
			feedInterval: null,
			stopFeed: function(){
				var o = $.qtKentharadioObj;
				if("undefined" !== typeof($.qtKentharadioObj.fn.qtFeedInterval) ){
					clearInterval($.qtKentharadioObj.fn.qtFeedInterval);
				}
			},
			radioSong: function(){
				var o = $.qtKentharadioObj;
				o.fn.stopFeed();
				o.fn.qtShoutcastFeed();
				$.qtKentharadioObj.fn.qtFeedInterval = setInterval(function(){ o.fn.qtShoutcastFeed() }, 5000);
			},
			radioSongClick: function(){
				var o = $.qtKentharadioObj;
				o.body.on("click", '.qtmusicplayer-play-btn', function(){
					o.fn.stopFeed();
				});
				o.body.on("click", '[data-radiochannel]', function(){
					var c = $(this);
					o.fn.stopFeed();
					o.qtFeedData = {
						host: c.attr("data-host"),
						port:  c.attr("data-port"),
						channel:  c.attr("data-channel"),
						icecast:  c.attr("data-icecasturl"),
						icecast_mount:  c.attr("data-icecastmountpoint"),
						radiodotco:  c.attr("data-radiodotco"),
						airtime:  c.attr("data-airtime"),
						radionomy:  c.attr("data-radionomy"),
						textfeed: c.attr("data-textfeed")
					};
					o.fn.radioSong();
				});
			},

			/**====================================================================
			 *
			 * 
			 *  Auto update ajax content
			 *  
			 *
			 ====================================================================*/
			refreshInterval: null,
			autoRefresh: function(){
				var link = $("#qt-kentharadio-currentpermalink").data("permalink");
				if(!link || undefined === link || '' === link){
					return;
				}
				var tempid, 
					that,item,
					itemsToRefresh = new Array(".qt-autorefresh");
				$.ajax({
					url: link,
					success:function(data) {
						$.ajaxData = data;
						itemsToRefresh.forEach(function(theselector,c){
							that = $(c);
							tempid = that.attr('id');
							item = $(theselector);
							item.animate({opacity: 0}, 0, function(){
								item.html( $($.ajaxData).find('#'+tempid).html() ).animate({opacity: 1}, 0);
							});
						});
					}
				});
			},
			autoRefreshloop: function(){
				if("undefined" !== typeof($.qtKentharadioObj.fn.refreshInterval) ){
					clearInterval($.qtKentharadioObj.fn.refreshInterval);
				}
				$.qtKentharadioObj.fn.refreshInterval = setInterval(function() {
					$.qtKentharadioObj.fn.autoRefresh();
				},60000);
			},
			


			/**====================================================================
			 *
			 *	After ajax page initialization
			 *  all the functions that needs recall if you load the page via ajax
			 * 	MUST RETURN TRUE IF ALL OK.
			 * 
			 ====================================================================*/
			initializeAfterAjax: function(){
				var f = $.qtKentharadioObj.fn;
				try {
					f.dropDownTabs();
					// f.qtShoutcastFeed();
					f.autoRefreshloop();
					return true;
				} catch(e) {
					f.debug('initializeAfterAjax: error');
					return false;
				}
			},

			/**====================================================================
			 *
			 * 
			 *  Functions to run once on first page load
			 *  
			 *
			 ====================================================================*/
			init: function() {
				var f = $.qtKentharadioObj.fn;
				f.radioSongClick();
				f.debug("\n ================ \n Initializing KenthaRadio \n ================ \n")
				var intz = f.initializeAfterAjax();
				if (true === intz){
					f.debug('Kentha Radio initializeAfterAjax(): success');
				}
			},
		}
		/**
		 * ======================================================================================================================================== |
		 * 																																			|
		 * 																																			|
		 * END SITE FUNCTIONS 																														|
		 * 																																			|
		 *																																			|
		 * ======================================================================================================================================== |
		 */
	};
	/**====================================================================
	 *
	 *	Page Ready Trigger
	 * 	This needs to call only $.ttgXtendWebsiteObj.fn.qtInitTheme
	 * 
	 ====================================================================*/
	jQuery(document).ready(function() {
		$.qtKentharadioObj.fn.init();	
		
	});

	/**====================================================================
	 *
	 *	Resize Trigger
	 * 	Called also on Visual COmposer frontend editing from some shortcodes
	 * 
	 ====================================================================*/
	$( window ).resize(function() {
		if(jQuery('body').hasClass('vc_editor')){
			var f = $.qtKentharadioObj.fn;
		}
	});
})(jQuery);





