/**
* @package qt-kentharadio
*/

// Called from shortcode changes like grid, to restore masonry and similar on frontend visual composer editing
function qt_kentharadio_jscomposerfront_reinit(){
	jQuery( window ).resize();
}

