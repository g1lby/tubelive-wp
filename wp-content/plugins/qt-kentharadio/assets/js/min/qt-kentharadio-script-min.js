/**
 *
 * Custom tabs function
 */
!function(_){var a={init:function(b){var t={onShow:null,swipeable:!1,responsiveThreshold:1/0};b=_.extend(t,b);var j=Materialize.objectSelectorString(_(this));return this.each(function(t){var a=j+t,i=_(this),e=_(window).width(),n,s,o=i.find("li.tab a"),r=i.width(),l=_(),c,d=Math.max(r,i[0].scrollWidth)/o.length,h,u=prev_index=0,p=!1,f,y=300,v=function(t){return Math.ceil(r-t.position().left-t.outerWidth()-i.scrollLeft())},m=function(t){return Math.floor(t.position().left+i.scrollLeft())},q=function(t){0<=u-t?(h.velocity({right:v(n)},{duration:y,queue:!1,easing:"easeOutQuad"}),h.velocity({left:m(n)},{duration:y,queue:!1,easing:"easeOutQuad",delay:90})):(h.velocity({left:m(n)},{duration:y,queue:!1,easing:"easeOutQuad"}),h.velocity({right:v(n)},{duration:y,queue:!1,easing:"easeOutQuad",delay:90}))};
// For each set of tabs, we want to keep track of
// which tab is active and its associated content
// Change swipeable according to responsive threshold
b.swipeable&&e>b.responsiveThreshold&&(b.swipeable=!1),
// If no match is found, use the first link or any with class 'active' as the initial active tab.
0===(
// If the location.hash matches one of the links, use that as the active tab.
n=_(o.filter('[href="'+location.hash+'"]'))).length&&(n=_(this).find("li.tab a.active").first()),0===n.length&&(n=_(this).find("li.tab a").first()),n.addClass("active"),(u=o.index(n))<0&&(u=0),void 0!==n[0]&&(s=_(n[0].hash)).addClass("active"),
// append indicator then set indicator width to tab width
i.find(".indicator").length||i.append('<div class="qt-kentharadio-indicator"></div>'),h=i.find(".qt-kentharadio-indicator"),
// we make sure that the indicator is at the end of the tabs
i.append(h),i.is(":visible")&&
// $indicator.css({"right": $tabs_width - ((index + 1) * $tab_width)});
// $indicator.css({"left": index * $tab_width});
setTimeout(function(){h.css({right:v(n)}),h.css({left:m(n)})},0),_(window).off("resize.tabs-"+a).on("resize.tabs-"+a,function(){r=i.width(),d=Math.max(r,i[0].scrollWidth)/o.length,u<0&&(u=0),0!==d&&0!==r&&(h.css({right:v(n)}),h.css({left:m(n)}))}),
// Initialize Tabs Content.
b.swipeable?(
// TODO: Duplicate calls with swipeable? handle multiple div wrapping.
o.each(function(){var t=_(Materialize.escapeHash(this.hash));t.addClass("carousel-item"),l=l.add(t)}),c=l.wrapAll('<div class="tabs-content carousel"></div>'),l.css("display",""),_(".tabs-content.carousel").carousel({fullWidth:!0,noWrap:!0,onCycleTo:function(t){if(!p){var a=u;u=c.index(t),n=o.eq(u),q(a)}}})):
// Hide the remaining content
o.not(n).each(function(){_(Materialize.escapeHash(this.hash)).hide()}),
// Bind the click event handler
i.off("click.tabs").on("click.tabs","a",function(t){if(_(this).parent().hasClass("disabled"))t.preventDefault();else
// Act as regular link if target attribute is specified.
if(!_(this).attr("target")){p=!0,r=i.width(),d=Math.max(r,i[0].scrollWidth)/o.length,
// Make the old tab inactive.
n.removeClass("active");var a=s;
// Update the variables with the new link and content
n=_(this),s=_(Materialize.escapeHash(this.hash)),o=i.find("li.tab a");var e=n.position();
// Make the tab active.
n.addClass("active"),prev_index=u,(u=o.index(_(this)))<0&&(u=0),
// Change url to current tab
// window.location.hash = $active.attr('href');
// Swap content
b.swipeable?l.length&&l.carousel("set",u):(void 0!==s&&(s.show(),s.addClass("active"),"function"==typeof b.onShow&&b.onShow.call(this,s)),void 0===a||a.is(s)||(a.hide(),a.removeClass("active"))),
// Reset clicked state
f=setTimeout(function(){p=!1},y),
// Update indicator
q(prev_index),
// Prevent the anchor's default click action
t.preventDefault()}})})},select_tab:function(t){this.find('a[href="#'+t+'"]').trigger("click")}};_.fn.qt_kentharadio_tabs=function(t){return a[t]?a[t].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof t&&t?void _.error("Method "+t+" does not exist on jQuery.tabs"):a.init.apply(this,arguments)},_(document).ready(function(){_("ul.qt-kentharadio-tabs").qt_kentharadio_tabs()})}(jQuery),
/*

This version of the library has been customized for the OnAir 2 theme by Qantumthemes
last edit 2016 11 20
* variables check
* clearInterval fixes

*/
function(n){"use strict";function a(t){this._attr={},void 0===t&&(t={}),
//how often to auto update
this.playedInterval=t.playedInterval||t.interval||3e4,this.statsInterval=t.statsInterval||t.interval||5e3,this.protocol="http",this.host=t.host,this.port=parseInt(t.port,10)||8e3,this.stream=parseInt(t.stream,10)||1,this.stats_path=t.stats_path||"stats",this.played_path=t.played_path||"played",this._statsinterval=null,this._playedinterval=null,this._stats=t.stats||function(){},this._played=t.played||function(){}}
/**
	 * Get attributes
	 * @param  {string} k the key to get e.g. songtitle [optional] - if theres no key all attributes will be returned
	 * @param  {mixed} d default value if the key value is not present [optional]
	 * @return {mixed}
	 */if(a.prototype.get=function(t,a){return t?void 0!==this._attr[t.toLowerCase()]?this._attr[t.toLowerCase()]:a:this._attr},
/**
	 * Get the shoutcast stats using /stats?sid=
	 * @param  {Function} fn the callback function, this will be passed the stats on success
	 * @return {SHOUTcast}      return this for chaining.
	 */
a.prototype.stats=function(a){var e=this,t;"443"!==this.port&&443!==this.port||(this.protocol="https");var i=this.protocol+"://"+this.host+":"+this.port+"/"+this.stats_path+"?sid="+this.stream+"&json=1";return null!==this._statsinterval&&clearInterval(this._statsinterval),a=a||function(){},(t=n.ajax({url:i,dataType:"jsonp",timeout:"5000"})).success(function(t){"object"==typeof t&&void 0!==t.streamstatus?(
//2 = on air, 1 = no source
e._status=1===t.streamstatus?2:1,e._attr=t,e._attr.status=e.getStatusAsText(),
//call the update method, give the raw data just incase its required
a.call(e,e._attr),e._stats(e._attr)):e._status=0}),t.error(function(){
// console.log("Error retriving the data via ajax.");
null!==this._statsinterval&&clearInterval(this._statsinterval)}),this},
/**
	 * Get the played information from /played?sid=
	 * @param  {Function} fn the callback function, will be passed an array of objects on success
	 * @return {SHOUTcast}      return this for chaining
	 */
a.prototype.played=function(a){"443"===this.port&&(this.protocol="https");var e=this,t=this.protocol+"://"+this.host+":"+this.port+"/"+this.played_path+"?sid="+this.stream+"&type=json";return n.ajax({url:t,dataType:"jsonp",timeout:2e3,success:function(t){!t instanceof Array||(a&&a.call(e,t),e._played(t))}}),this},
/**
	 * Start updating using the stats method
	 * @return {SHOUTcast} return this for chaining
	 */
a.prototype.startStats=function(){
// this._statsinterval = setInterval($.proxy(this.stats,this), this.statsInterval);
// console.log("\n =======  STOPPING STATS  ========= \n");
return this.stats(),null!==this._statsinterval&&void 0!==this._statsinterval&&
// console.log("Clearing stats interval this._statsinterval ===>" + this._statsinterval);
clearInterval(this._statsinterval),this},
/**
	 * Stop updating stats
	 * @return {SHOUTcast} return this for chaining
	 */
a.prototype.stopStats=function(){return this._statsinterval&&clearInterval(this._statsinterval),this},
/**
	 * Start updating played information
	 * @return {SHOUTcast} this for chaining
	 */
a.prototype.startPlayed=function(){
// this._playedinterval = setInterval($.proxy(this.played,this),this.playedInterval);
return this.stopPlayed(),this.played(),n.proxy(this.played,this),this},
/**
	 * Stop updating the played information
	 * @return {SHOUTcast} this for chaining
	 */
a.prototype.stopPlayed=function(){return this._playedinterval&&clearInterval(this._playedinterval),this},
/**
	 * Get the SHOUTcast status based on the last stats call
	 * @return {int} the status 0 = offline, 1 = no source connected, 2 = on air
	 */
a.prototype.getStatus=function(){return this._status},a.prototype.getStatusAsText=function(){return["Offline","Awaiting Connection","On Air"][this._status]},
/**
	 * Check if the SHOUTcast server is on air.
	 * @return {bool} whether the server is on air or not (this means source connected)
	 */
a.prototype.onAir=function(){return 2===this._status},
/**
	 * The jQuery plug
	 * @param {object} opt the options to pass to the shoutcast object
	 * @return { SHOUTcast} the shotucast object
	 */
n.SHOUTcast=function(t){return new a(t)},!n("body").hasClass("qt-debug")){var t={log:function(){}};window.console=t}}(jQuery),
/**
* @package qt-kentharadio
*/
/**====================================================================
 *
 *  Main Script File
 *
 *====================================================================*/
/**
 * These are the files required for the main script to run.
 * They are already imported in the minified version qt-main-min.js
 * If you enable Debug option in customizer, the unminified separated version
 * will be included instead, and you can modify this current js file for your own customizations
 */
// IMPORTANT: THE FOLLOWINT IS NOT A COMMENT! IT IS JAVASCIPT IMPORTING! *** DO NOT DELETE ***
// ===========================================================================================
// @codekit-prepend "libs/_tabs.js"
// @codekit-prepend "libs/_shoutcast.js"
function(k){"use strict";var t=0;// set to 1 to enable console logging
k.qtKentharadioObj={body:k("body"),window:k(window),document:k(document),htmlAndbody:k("html,body"),qtFeedData:{host:"",port:"",channel:"",icecast:"",icecast_mount:"",radiodotco:"",airtime:"",radionomy:"",textfeed:""},
/**
		 * ======================================================================================================================================== |
		 * 																																			|
		 * 																																			|
		 * START SITE FUNCTIONS 																													|
		 * 																																			|
		 *																																			|
		 * ======================================================================================================================================== |
		 */
fn:{debug:function(t){0},
/**====================================================================
			 *
			 * 
			 *  Ajax elements refresh
			 *  
			 * 
			 ====================================================================*/
qtAjaxElementsRefresh:function(){var t=k("#qtcurrentpermalink");if(!(t.length<=0)){var a,e,i,n=t.attr("data-permalink"),s=new Array(".qt-autorefresh");k.ajax({url:n,success:function(t){k.ajaxData=t,s.forEach(function(t){k(t).animate({opacity:0},0,function(){k(t).html(k(k.ajaxData).find(t).html()).animate({opacity:1},0,function(){k.fn.dynamicBackgrounds(t),k.fn.slickGallery()})})})}})}},
/**====================================================================
			 *
			 * 
			 *  Dropdown tabs
			 *  
			 * 
			 ====================================================================*/
dropDownTabs:function(){k("#qt-kentharadio-ShowDropdown").change(function(){k("a#"+k(this).attr("value")).click()})},
/**====================================================================
			*
			* SHOTCAST XML Feed Info
			* 
			====================================================================*/
qtApplyTitle:function(t){if(t&&""!==t){var a=t.split(" - "),e,i;i=1<a.length?(e=a[0],a[1]):(e="",t),k(".qt-kentharadio-feed").show(),k(".qt-kentharadio-feed__author").html(e),k(".qt-kentharadio-feed__title").html(i),k(".qt-mplayer__artist").html(e+": "+i)}else k(".qt-kentharadio-feed").hide()},qtShoutcastFeed:function(){var t=k.qtKentharadioObj,a=t.qtFeedData,e=t.fn,i=void 0!==a.host?a.host:"",n=void 0!==a.port?a.port:"",s=void 0!==a.ssl?"1":"",o=void 0!==a.channel?a.channel:"",r=void 0!==a.icecasturl?a.icecasturl:"",l=void 0!==a.icecast_mount?a.icecast_mount:"",c=void 0!==a.radiodotco?a.radiodotco:"",d=void 0!==a.airtime?a.airtime:"",h=void 0!==a.radionomy?a.radionomy:"",u=void 0!==a.textfeed?a.textfeed:"",p=k("#qtFeedPlayerTrack"),f=k("#qtFeedPlayerAuthor"),y=k("#qtPlayerTrackInfo"),v=!1,// will add this option MAYBE in the future
m,q,b,j;if(a=(t=k.qtKentharadioObj).qtFeedData,""!==u||""!==h||""!==d||""!==i&&""!==n&&void 0!==i||""!==r||""!==c){if(""!==u){var _=k("#qantumthemesproxyurl").data("proxyurl"),x,q;k.ajax({type:"GET",cache:!1,url:_,async:!0,data:{qtproxycall:u},dataType:"html",success:function(t){e.qtApplyTitle(t)},error:function(t){}})}else if(""!==h)k.ajax({type:"GET",url:h,async:!0,cache:!1,dataType:"xml",success:function(t){e.qtApplyTitle(k(t).find("artists").html()+" - "+k(t).find("title").html())},error:function(t){}});else if(""!==d&&"undefined"!==d&&void 0!==d&&void 0!==d){var _=k("#qantumthemesproxyurl").data("proxyurl"),x,q;k.ajax({type:"GET",cache:!1,url:_,async:!0,data:{qtproxycall:d},contentType:"application/json",success:function(t){x=JSON.parse(t),q=x.tracks.current.name,e.qtApplyTitle(q)},error:function(t){}})}else if(""!==r&&void 0!==r)if(""!==l)k.ajax({type:"GET",url:r,async:!0,jsonpCallback:"parseMusic",contentType:"application/json",dataType:"jsonp",success:function(t){""!==l?b=t[l].title:t.icestats.source&&(b=t.icestats.source.title),e.qtApplyTitle(b)},error:function(t){}});else{var _=k("#qantumthemesproxyurl").data("proxyurl"),x,q;k.ajax({type:"GET",cache:!1,url:_,async:!0,data:{qtproxycall:r},contentType:"application/json",success:function(t){(x=JSON.parse(t)).icestats.source.title?q=x.icestats.source.title:x.icestats.source[1].title?q=x.icestats.source[1].title:x.icestats.source[0].title&&(q=x.icestats.source[0].title),e.qtApplyTitle(q)},error:function(t){}})}else if(""!==i&&""!==n&&void 0!==i){var g="http",w="http://"+i+":"+n+"/stats?sid="+o+"&json=1",T=k("#qantumthemesproxyurl").data("proxyurl"),x,q;
/*
								$.ajax({
									type: 'GET',
									cache: false,
									url: qtscurl,
									async: true,
									contentType: "application/json",
									success: function(data) {
										jsondata = JSON.parse(data);
										fn.qtApplyTitle(jsondata.songtitle);
									},
									error: function(e) {
									}
								});
								*/
1===s&&(g="https"),k.SHOUTcast({host:i,port:n,protocol:g,interval:1e4,stream:o,stats:function(){this.onAir()&&(b=this.get("songtitle"),e.qtApplyTitle(b))}}).startStats()}else if(""!==c&&void 0!==c){var A="https://public.radio.co/stations/"+c+"/status";k.ajax({type:"GET",cache:!1,url:A,async:!0,contentType:"application/json",success:function(t){q=t.current_track.title,e.qtApplyTitle(q)},error:function(t){}})}}else e.qtApplyTitle()},feedInterval:null,stopFeed:function(){var t=k.qtKentharadioObj;void 0!==k.qtKentharadioObj.fn.qtFeedInterval&&clearInterval(k.qtKentharadioObj.fn.qtFeedInterval)},radioSong:function(){var t=k.qtKentharadioObj;t.fn.stopFeed(),t.fn.qtShoutcastFeed(),k.qtKentharadioObj.fn.qtFeedInterval=setInterval(function(){t.fn.qtShoutcastFeed()},5e3)},radioSongClick:function(){var a=k.qtKentharadioObj;a.body.on("click",".qtmusicplayer-play-btn",function(){a.fn.stopFeed()}),a.body.on("click","[data-radiochannel]",function(){var t=k(this);a.fn.stopFeed(),a.qtFeedData={host:t.attr("data-host"),port:t.attr("data-port"),channel:t.attr("data-channel"),icecast:t.attr("data-icecasturl"),icecast_mount:t.attr("data-icecastmountpoint"),radiodotco:t.attr("data-radiodotco"),airtime:t.attr("data-airtime"),radionomy:t.attr("data-radionomy"),textfeed:t.attr("data-textfeed")},a.fn.radioSong()})},
/**====================================================================
			 *
			 * 
			 *  Auto update ajax content
			 *  
			 *
			 ====================================================================*/
refreshInterval:null,autoRefresh:function(){var t=k("#qt-kentharadio-currentpermalink").data("permalink");if(t&&void 0!==t&&""!==t){var e,i,n,a=new Array(".qt-autorefresh");k.ajax({url:t,success:function(t){k.ajaxData=t,a.forEach(function(t,a){i=k(a),e=i.attr("id"),(n=k(t)).animate({opacity:0},0,function(){n.html(k(k.ajaxData).find("#"+e).html()).animate({opacity:1},0)})})}})}},autoRefreshloop:function(){void 0!==k.qtKentharadioObj.fn.refreshInterval&&clearInterval(k.qtKentharadioObj.fn.refreshInterval),k.qtKentharadioObj.fn.refreshInterval=setInterval(function(){k.qtKentharadioObj.fn.autoRefresh()},6e4)},
/**====================================================================
			 *
			 *	After ajax page initialization
			 *  all the functions that needs recall if you load the page via ajax
			 * 	MUST RETURN TRUE IF ALL OK.
			 * 
			 ====================================================================*/
initializeAfterAjax:function(){var a=k.qtKentharadioObj.fn;try{return a.dropDownTabs(),
// f.qtShoutcastFeed();
a.autoRefreshloop(),!0}catch(t){return a.debug("initializeAfterAjax: error"),!1}},
/**====================================================================
			 *
			 * 
			 *  Functions to run once on first page load
			 *  
			 *
			 ====================================================================*/
init:function(){var t=k.qtKentharadioObj.fn,a;t.radioSongClick(),t.debug("\n ================ \n Initializing KenthaRadio \n ================ \n"),!0===t.initializeAfterAjax()&&t.debug("Kentha Radio initializeAfterAjax(): success")}}
/**
		 * ======================================================================================================================================== |
		 * 																																			|
		 * 																																			|
		 * END SITE FUNCTIONS 																														|
		 * 																																			|
		 *																																			|
		 * ======================================================================================================================================== |
		 */},
/**====================================================================
	 *
	 *	Page Ready Trigger
	 * 	This needs to call only $.ttgXtendWebsiteObj.fn.qtInitTheme
	 * 
	 ====================================================================*/
jQuery(document).ready(function(){k.qtKentharadioObj.fn.init()}),
/**====================================================================
	 *
	 *	Resize Trigger
	 * 	Called also on Visual COmposer frontend editing from some shortcodes
	 * 
	 ====================================================================*/
k(window).resize(function(){if(jQuery("body").hasClass("vc_editor"))var t=k.qtKentharadioObj.fn})}(jQuery);