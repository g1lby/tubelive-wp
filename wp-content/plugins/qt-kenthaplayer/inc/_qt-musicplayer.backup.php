<?php
/**
 * @package Kentha
 */

if(!function_exists('qt_musicplayer')){
function qt_musicplayer(){
	$musicplayerapi = site_url( '/?kenthaplayer_json_data=' );
	$smflash = qt_kenthaplayer_flashurl();
	$color = get_theme_mod( 'kentha_color_secondary', '#ff0d51' );
	$data_analyzer = get_option( 'qt_kenthaplayer_basicplayer' );
	if(wp_is_mobile()){
		$data_analyzer = 0;
	}
	$autoplay = get_option( 'qt_kenthaplayer_autoplay' );


	/**
	 * Since 1.8.3 check if I have channels or releases
	 */
	$quantity = 0;
	$args = array(
		'post_type' => 'radiochannel',
		'post_status' => 'publish',
	);
	$wp_query = new WP_Query( $args );
	$quantity = $quantity + $wp_query->found_posts;

	$args = array(
		'post_type' => 'release',
		'post_status' => 'publish',
	);
	$wp_query = new WP_Query( $args );
	$quantity = $quantity + $wp_query->found_posts;


	/**
	 * New Chrome policy: autoplay is disabled
	 * @var string
	 */
	// $autoplay = '';
	
	?>
	<div id="qtmusicplayer" class="qt-mplayer qt-scrollbarstyle <?php if($quantity == 0) { ?>qt-hidden<?php } ?>" data-showplayer="<?php  echo esc_attr(get_option( 'qt_kenthaplayer_showplayer' )); ?>" data-analyzer="<?php echo esc_attr($data_analyzer); ?>" data-autoplay="<?php if( function_exists('qt_ajax_pageload_is_active') && !wp_is_mobile() ){  echo esc_attr( $autoplay ); } ?>" data-hiquality="<?php  echo esc_attr(get_option( 'qt_kenthaplayer_hiquality' )); ?>" data-qtmusicplayer-api="<?php echo esc_attr($musicplayerapi); ?>" data-qtmusicplayer-smflash="<?php echo esc_attr($smflash); ?>">
		<?php  
		/**
		 *
		 *	Playlist
		 * 
		 */
		?>
		<div id="qtmusicplayer-playlistcontainer" class="qt-mplayer__playlistcontainer qt-content-primary">
			<div class="row qt-mplayer__playlistmaster">
				<div class="col s12 m4 l3">
					<div id="qtmusicplayer-cover" class="qt-mplayer__album">
						<a href="#" class="qt-btn qt-btn-ghost qt-btn-l qt-albumlink"><?php esc_html_e( "Go to album", 'qt-kenthaplayer' ); ?></a>
					</div>
				</div>
				<div id="qtmusicplayer-playlist" class="qt-mplayer__playlist qt-content-primary col s12 m8 l9">
					<?php  
					/**
					 * UL moved here from part-playlist.php in Version 1.1.5 for QT Kentharadio compatibility
					 */
					?>
					<ul class="qt-playlist">
						<?php
						/**
						 * =================================================
						 * KenthaRadio plugin support
						 * =================================================
						 * Since 1.7.0
						 * List of radios in player from QT KenthaRadio Plugin
						 * =================================================
						 */
						if(function_exists( 'qt_kentharadio_active' )){
							$args = array(
								'post_type' => 'radiochannel',
								'ignore_sticky_posts' => 1,
								'post_status' => 'publish',
								'orderby' => array ( 'menu_order' => 'ASC', 'date' => 'DESC'),
								'suppress_filters' => false,
								'posts_per_page' => -1,
								'paged' => 1,
								'meta_query' => array(
									array(
										'key' => 'qt-kentharadio-addtoplaylist',
										'value' => '1',
										'compare' => '='
									)
								)
							);
							$wp_query = new WP_Query( $args );
							if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
								$post = $wp_query->post;
								setup_postdata( $post );
								get_template_part('phpincludes/part-playlist-radio' );  
							endwhile; endif;
							wp_reset_postdata();
						}
						/**
						 * =================================================
						 * KenthaRadio plugin support end
						 * =================================================
						 */
						if(is_single() && get_post_type( get_the_id()) == 'release'){
							get_template_part('phpincludes/part-playlist' );  
						} else {
							// get latest release
							$args = array(
								'post_type' => 'release',
								'ignore_sticky_posts' => 1,
								'post_status' => 'publish',
								'orderby' => array ( 'menu_order' => 'ASC', 'date' => 'DESC'),
								'suppress_filters' => false,
								'posts_per_page' => 1,
								'paged' => 1
							);
							
							$wp_query = new WP_Query( $args );
							if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
								$post = $wp_query->post;
								setup_postdata( $post );
								get_template_part('phpincludes/part-playlist' );  
							endwhile; else: ?>
								<ul class="qt-playlist"></ul>
							<?php 
							endif;
							wp_reset_postdata();
						}
						?>
					</ul>
				</div>
			</div>
		</div>
		<?php
		/**
		 *
		 *	Control bar
		 * 
		 */
		?>
		<div id="qtmusicplayer-controls"  class="qt-mplayer__controls" data-hidetimeout="1000">
			<div class="qt-mplayer__controllayer">
				<a id="qtmusicplayerPlay" class="qt-mplayer__play qt-btn-secondary">
					<i class="material-icons">play_arrow</i>
					<i id="qtmplayerNotif" class="qt-mplayer__notification qt-content-secondary"></i>
				</a>
				<span class="qt-mplayer__prev" data-control="prev">
					<i class='material-icons'>skip_previous</i>
				</span>
				<span class="qt-mplayer__next" data-control="next">
					<i class='material-icons'>skip_next</i>
				</span>
				<span id="qtmusicplayerVol" class="qt-mplayer__volume">
					<i class="material-icons">volume_down</i>
					<span id="qtmpvf" class="qt-mplayer__volfill qt-content-accent"></span><span id="qtmpvc" class="qt-mplayer__volcursor"></span>
				</span>
				<a id="qtmusicplayerCart" target="_blank" class="qt-mplayer__cart">
					<i class='material-icons'></i>
				</a>
				<a class="qt-mplayer__cover">
				</a>
				<div id="qtmusicplayerTrackControl" class="qt-mplayer__track">
					<div class="qt-mplayer__rowone">
						<span class="qt-mplayer__title"></span>
						<span id="qtmusicplayerTime" class="qt-mplayer__time"></span>
					</div>
					<div class="qt-mplayer__rowtwo">
						<span class="qt-mplayer__artist"></span>
						<span id="qtmusicplayerDuration" class="qt-mplayer__length"></span>
					</div>
					<span id="qtMplayerBuffer" class="qt-mplayer-track-adv qt-content-primary-light"></span>
					<span id="qtMplayerTadv" class="qt-mplayer-track-adv qt-content-accent"></span>
					<span id="qtMplayerMiniCue" class="qt-mplayer-track-minicue"></span>
				</div>
				<span class="qt-mplayer__playlistbtn" data-playlistopen>
					<i class='material-icons'>playlist_play</i>
				</span>
			</div>
		</div>
	</div>
	<?php if(get_option( 'qt_kenthaplayer_basicplayer' )){ ?>
	<canvas id="qtmusicplayerWF" class="qt-mplayer__waveform"></canvas>
	<div id="qtmusicplayerFFT" data-color="<?php esc_attr_e($color); ?>" class="qt-mplayer__waves"></div>
	<?php } ?>
	<?php
}}
