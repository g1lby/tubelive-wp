<?php
/**
 * @package Kentha
 */

if(!function_exists('qt_musicplayer')){
function qt_musicplayer(){
	$musicplayerapi = site_url( '/?kenthaplayer_json_data=' );
	$smflash = qt_kenthaplayer_flashurl();
	$color = get_theme_mod( 'kentha_color_secondary', '#ff0d51' );
	$data_analyzer = get_option( 'qt_kenthaplayer_basicplayer' );
	if(wp_is_mobile()){
		$data_analyzer = 0;
	}
	$autoplay = get_option( 'qt_kenthaplayer_autoplay' );


	/**
	 * Since 1.8.3 check if I have channels or releases
	 */
	$quantity = 0;
	$args = array(
		'post_type' => 'radiochannel',
		'post_status' => 'publish',
	);
	$wp_query = new WP_Query( $args );
	$quantity = $quantity + $wp_query->found_posts;

	$args = array(
		'post_type' => 'release',
		'post_status' => 'publish',
	);
	$wp_query = new WP_Query( $args );
	$quantity = $quantity + $wp_query->found_posts;


	/**
	 * New Chrome policy: autoplay is disabled
	 * @var string
	 */
	// $autoplay = '';
	
	?>
	<div id="qtmusicplayer" class="qt-mplayer qt-scrollbarstyle <?php if($quantity == 0) { ?>qt-hidden<?php } ?>" data-showplayer="<?php  echo esc_attr(get_option( 'qt_kenthaplayer_showplayer' )); ?>" data-analyzer="<?php echo esc_attr($data_analyzer); ?>" data-autoplay="<?php if( function_exists('qt_ajax_pageload_is_active') && !wp_is_mobile() ){  echo esc_attr( $autoplay ); } ?>" data-hiquality="<?php  echo esc_attr(get_option( 'qt_kenthaplayer_hiquality' )); ?>" data-qtmusicplayer-api="<?php echo esc_attr($musicplayerapi); ?>" data-qtmusicplayer-smflash="<?php echo esc_attr($smflash); ?>">
		<?php
		/**
		 *
		 *	Playlist
		 * 
		 */
		?>
		<div id="qtmusicplayer-playlistcontainer" class="qt-mplayer__playlistcontainer qt-content-primary">
			<div class="row qt-mplayer__playlistmaster">
				<div class="col s12 m4 l3">
					<div id="qtmusicplayer-cover" class="qt-mplayer__album">
						<a href="#" class="qt-btn qt-btn-ghost qt-btn-l qt-albumlink"><?php esc_html_e( "Go to album", 'qt-kenthaplayer' ); ?></a>
					</div>
				</div>
				<div id="qtmusicplayer-playlist" class="qt-mplayer__playlist qt-content-primary col s12 m8 l9">
					<?php  
					/**
					 * UL moved here from part-playlist.php in Version 1.1.5 for QT Kentharadio compatibility
					 */
					?>
					<ul class="qt-playlist">
						



						<?php
						/**
						 * =================================================
						 * Single release playlist
						 * if I'm in a release page, load always this playlist first
						 * =================================================
						 * @since 1.9.0
						 * @var array
						 */
						if(is_single() && get_post_type( get_the_id()) == 'release'){
							get_template_part('phpincludes/part-playlist' );  
						}
						/**
						 * =================================================
						 * END OF Single release playlist
						 * =================================================
						 */
						?>






						<?php
						/**
						 * =================================================
						 * KenthaRadio plugin support
						 * =================================================
						 * Since 1.7.0
						 * List of radios in player from QT KenthaRadio Plugin
						 * =================================================
						 */
						if(function_exists( 'qt_kentharadio_active' )){
							$args = array(
								'post_type' => 'radiochannel',
								'ignore_sticky_posts' => 1,
								'post_status' => 'publish',
								'orderby' => array ( 'menu_order' => 'ASC', 'date' => 'DESC'),
								'suppress_filters' => false,
								'posts_per_page' => -1,
								'paged' => 1,
								'meta_query' => array(
									array(
										'key' => 'qt-kentharadio-addtoplaylist',
										'value' => '1',
										'compare' => '='
									)
								)
							);
							$wp_query = new WP_Query( $args );
							if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
								$post = $wp_query->post;
								setup_postdata( $post );
								get_template_part('phpincludes/part-playlist-radio' );  
							endwhile; endif;
							wp_reset_postdata();
						}
						/**
						 * =================================================
						 * END OF KenthaRadio plugin support
						 * =================================================
						 */
						
						?>





						<?php
						/**
						 * =================================================
						 * Kentha custom playlist
						 * =================================================
						 * @since 1.9.0
						 * @var array
						 */
						$playlist = get_theme_mod( 'kentha_custom_playlist' );
						if($playlist){
							if( is_array( $playlist )){
								$n = 1;
								foreach($playlist as $track){
									$track_index = str_pad($n, 2 , "0", STR_PAD_LEFT);
									$neededEvents = array('title','artist','album','buylink','link', 'sample', 'art','icon', 'price');
									foreach($neededEvents as $ne){
										if(!array_key_exists($ne,$track)){
											$track[$ne] = '';
										}
									}
									switch ($track['icon']){
										case "download": 
											$icon = 'file_download';
											break;
										case "cart": 
										default:
											$icon = 'add_shopping_cart';
									}



									// Get the right thumbnail
									if( $track['art'] !== '' ) {
										$cover = wp_get_attachment_image_src( $track['art'], 'kentha-squared' );
										$thumb = wp_get_attachment_image_src( $track['art'], 'thumbnail' );
									}

									// Get the sample URL
									$file = false;
									if( array_key_exists('sample',  $track)) {
										if( $track['sample'] !== '' ) {
											$file = wp_get_attachment_url( $track['sample'] );
											
											
											$price = $track['price'];

											?>
											

											<li class="qtmusicplayer-trackitem">
												<?php
												if( isset( $thumb ) ){
													?>
													<img src="<?php echo esc_url($thumb[0]); ?>" alt="cover">
													<?php
												}
												?>
												<span class="qt-play qt-link-sec qtmusicplayer-play-btn" 
												data-qtmplayer-cover="<?php echo esc_url($cover[0]); ?>" 
												data-qtmplayer-file="<?php echo esc_url($file); ?>" 
												data-qtmplayer-title="<?php echo esc_attr($track['title']); ?>" 
												data-qtmplayer-artist="<?php echo esc_attr($track['artist']); ?>" 
												data-qtmplayer-album="<?php echo esc_attr($track['album']); ?>" 
												data-qtmplayer-link="<?php echo esc_url($track['link']); ?>" 
												data-qtmplayer-price="<?php echo esc_url($track['price']); ?>" 
												data-qtmplayer-buylink="<?php echo esc_attr($track['buylink']); ?>" 
												data-qtmplayer-icon="<?php echo esc_attr($icon); ?>" ><i class='material-icons'>play_circle_filled</i></span>
												<p>
													<span class="qt-tit"><?php echo esc_attr($track_index); ?>. <?php echo esc_attr($track['title']); ?></span><br>
													<span class="qt-art">
														<?php echo esc_attr($track['artist']); ?>
													</span>
												</p>
												<?php 
												if($track['buylink'] !== ''){
													/**
													 *
													 * WooCommerce update:
													 *
													 */
													$buylink = $track['buylink'];
													if(is_numeric($buylink)) {
														$prodid = $buylink;
														$buylink = add_query_arg("add-to-cart" ,   $buylink, get_the_permalink());
														?>
														<a href="<?php echo esc_attr($buylink); ?>" data-quantity="1" data-product_id="<?php echo esc_attr($prodid); ?>" class="qt-cart product_type_simple add_to_cart_button ajax_add_to_cart">
														<?php echo ($price !== '')? '<span class="qt-price qt-btn qt-btn-xs qt-btn-primary">'.esc_html($price).'</span>' : '<i class="material-icons">'.esc_html($icon).'</i>'; ?>
														</a>
														<?php 
													} else {
														?>
														<a href="<?php echo esc_attr($buylink); ?>" class="qt-cart" target="_blank">
														<?php echo ($price !== '')? '<span class="qt-price qt-btn qt-btn-xs qt-btn-primary">'.esc_html($price).'</span>' : '<i class="material-icons">'.esc_html($icon).'</i>'; ?>
														</a>
														<?php
													}
												} 
												?>
											</li>

											<?php
										}
									}
									$n = $n + 1;
								}
							}
						}


						/**
						 * =================================================
						 * END OF Kentha custom playlist
						 * =================================================
						 */
						
						?>


						<?php

						/**
						 * =================================================
						 * Kentha preload release playlist
						 * =================================================
						 * @since 1.9.0
						 * @var array
						 * Featured releases by ID
						 */
						$featureds = get_theme_mod( 'kentha_player_releasefeatured' );
						$ids_featured = false;
						if ($featureds){
							$ids_featured = str_replace(' ', '', $featureds);
							$args = array (
								'post_type' =>  'release',
								'post__in'=> explode(',',$ids_featured),
								'orderby' => 'post__in',
							);  
							$wp_query = new WP_Query( $args );
							if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
								$post = $wp_query->post;
								setup_postdata( $post );
								get_template_part('phpincludes/part-playlist' );  
							endwhile; endif; 
							wp_reset_postdata();
						}

						/**
						 * Default release extraction
						 * @since 1.0.0
						 */
						$releases_amount = intval( get_theme_mod( 'kentha_player_releaseamount', '1' ) );
						if ($releases_amount !== 0){
							$args = array(
								'post_type' => 'release',
								'ignore_sticky_posts' => 1,
								'post_status' => 'publish',
								'orderby' => array ( 'menu_order' => 'ASC', 'date' => 'DESC'),
								'suppress_filters' => false,
								'posts_per_page' => $releases_amount, // @since 1.9.0
								'paged' => 1
							);

							/**
							 * Remove featured IDS from the result to avoid duplicates
							 * @since 1.9.0
							 */
							if( $ids_featured ){
								$args['post__not_in'] = explode(',',$ids_featured);
							}
							$wp_query = new WP_Query( $args );
							if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
								$post = $wp_query->post;
								setup_postdata( $post );
								get_template_part('phpincludes/part-playlist' );  
							endwhile; endif; 
							wp_reset_postdata();
						}
						
						/**
						 * =================================================
						 * END OF Kentha preload release playlist 
						 * =================================================
						 */
						


						/**
						 * =================================================
						 * Kentha preload podcast
						 * =================================================
						 * @since 1.9.0
						 * @var array
						 * Featured releases by ID
						 */
						$featureds = get_theme_mod( 'kentha_player_podcastfeatured' );
						$ids_featured = false;
						if ($featureds){
							$ids_featured = str_replace(' ', '', $featureds);
							$args = array (
								'post_type' =>  'podcast',
								'post__in'=> explode(',',$ids_featured),
								'orderby' => 'post__in',
							);  
							$wp_query = new WP_Query( $args );
							if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
								$post = $wp_query->post;
								setup_postdata( $post );
								get_template_part('phpincludes/part-playlist-podcast' );  
							endwhile; endif; 
							wp_reset_postdata();
						}

						/**
						 * Default release extraction
						 * @since 1.0.0
						 */
						$releases_amount = intval( get_theme_mod( 'kentha_player_podcastamount', '1' ) );
						if ($releases_amount !== 0){
							$args = array(
								'post_type' => 'podcast',
								'ignore_sticky_posts' => 1,
								'post_status' => 'publish',
								'orderby' => array ( 'menu_order' => 'ASC', 'date' => 'DESC'),
								'suppress_filters' => false,
								'posts_per_page' => $releases_amount, // @since 1.9.0
								'paged' => 1,
								'meta_query' => array(
									array(
										'key' => '_podcast_resourceurl',
										'value' => '.mp3',
										'compare' => 'LIKE'
									)
								)
							);

							/**
							 * Remove featured IDS from the result to avoid duplicates
							 * @since 1.9.0
							 */
							if( $ids_featured ){
								$args['post__not_in'] = explode(',',$ids_featured);
							}
							$wp_query = new WP_Query( $args );
							if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
								$post = $wp_query->post;
								setup_postdata( $post );
								get_template_part('phpincludes/part-playlist-podcast' );  
							endwhile; endif; 
							wp_reset_postdata();
						}
						/**
						 * =================================================
						 * END OF Kentha preload podcast
						 * =================================================
						 */
						






						?>
					</ul>
				</div>
			</div>
		</div>
		<?php
		/**
		 *
		 *	Control bar
		 * 
		 */
		?>
		<div id="qtmusicplayer-controls"  class="qt-mplayer__controls" data-hidetimeout="1000">
			<div class="qt-mplayer__controllayer">
				<a id="qtmusicplayerPlay" class="qt-mplayer__play qt-btn-secondary">
					<i class="material-icons">play_arrow</i>
					<i id="qtmplayerNotif" class="qt-mplayer__notification qt-content-secondary"></i>
				</a>
				<span class="qt-mplayer__prev" data-control="prev">
					<i class='material-icons'>skip_previous</i>
				</span>
				<span class="qt-mplayer__next" data-control="next">
					<i class='material-icons'>skip_next</i>
				</span>
				<span id="qtmusicplayerVol" class="qt-mplayer__volume">
					<i class="material-icons">volume_down</i>
					<span id="qtmpvf" class="qt-mplayer__volfill qt-content-accent"></span><span id="qtmpvc" class="qt-mplayer__volcursor"></span>
				</span>
				<a id="qtmusicplayerCart" target="_blank" class="qt-mplayer__cart">
					<i class='material-icons'></i>
				</a>
				<a class="qt-mplayer__cover">
				</a>
				<div id="qtmusicplayerTrackControl" class="qt-mplayer__track">
					<div class="qt-mplayer__rowone">
						<span class="qt-mplayer__title"></span>
						<span id="qtmusicplayerTime" class="qt-mplayer__time"></span>
					</div>
					<div class="qt-mplayer__rowtwo">
						<span class="qt-mplayer__artist"></span>
						<span id="qtmusicplayerDuration" class="qt-mplayer__length"></span>
					</div>
					<span id="qtMplayerBuffer" class="qt-mplayer-track-adv qt-content-primary-light"></span>
					<span id="qtMplayerTadv" class="qt-mplayer-track-adv qt-content-accent"></span>
					<span id="qtMplayerMiniCue" class="qt-mplayer-track-minicue"></span>
				</div>
				<span class="qt-mplayer__playlistbtn" data-playlistopen>
					<i class='material-icons'>playlist_play</i>
				</span>
			</div>
		</div>
	</div>
	<?php if(get_option( 'qt_kenthaplayer_basicplayer' )){ ?>
	<div id="qtmusicplayerWF"  class="qt-mplayer__waveform">
		<canvas></canvas>
	</div>




	<div id="qtmusicplayerFFT" data-color="<?php esc_attr_e($color); ?>" class="qt-mplayer__waves"></div>
	<?php } ?>
	<?php
}}
