How to integrate the Connector Client (also called Theme Dashboard) on a theme:


BEFORE ANYTHING, BE SURE THAT THE SERVER IS READY:
- Connector installed
- new theme versions uploaded (with prod id and all)
- plugins uploaded (with prod id)


1) Add the ZIP of the plugin to the installable plugins of the theme
2) Be sure the to unzip the connector client and check the SERVER URL for services, in the plugin's main file:

define ("T2GCONNECTORSERVER", '[main URL of the website with connectore server installed]');

3) Add a folder in the theme called T2G-connector-client
4) Add a subfolder called "demo"
5) For each demo exported with Easy Demo Installer plugin, add a folder containing the files:
	- config.xml
	- dummy.jpg
	- dummy.xml
	- screenshot.jpg


6) Add a file t2gconnectorclient-config.php
7) in the file, add the settings as follow:

<?php

if(!class_exists("T2GConnectorConfig")){
class T2GConnectorConfig {

	public function __construct(){
		
	}
	///return $this->login_url.$this->client_id.'&redirect_uri='.$this->login_page;
	// This function needs to stay in the functions.php of the theme
	public function t2gconnector_product_sku() {
		return '[Envato product ID]';
	}
	// generic settings
	public function t2gconnector_settings(){
		$settings = array 
			(
				"dashboard_title" => "[Custom title for dashboard item]"
			);
		return $settings;
	}
	// for auto plugin update
	public function t2gconnector_plugins_list(){
		$plugins = array(
				array(
					'name'      => esc_attr('Theme Core Plugin','lifecoach'),
					'slug'      => 'ttg-core',
					'version'	=> '1.0.5',
					'required'  => true,
					'source'             => get_template_directory() . '/TGM-Plugin-Activation/plugins/ttg-core.zip',
					'force_activation'   => false, 
					'force_deactivation' => true
				),
		
		);
		return $plugins;
	}
	public function t2gconnector_demos_list(){
		$demos = array(
			'demo1' => array(
				'name'      	=> "Demo 1",
				'folder'       	=>  '/T2G-connector-client/demo/demo1/',
				'screenshot'   	=>  '/T2G-connector-client/demo/demo1/screenshot.jpg', 
				'description'	=>	'Basic product demo'
			),
		);
		return $demos;
	}
}
}?>






