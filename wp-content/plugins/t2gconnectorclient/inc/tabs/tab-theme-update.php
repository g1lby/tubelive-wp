<?php
/**
 * @package t2gconnectorclient
 * @author Themes2Go
 */



/**
 * Output
 */
function t2gconnectorclient_theme_update_output(){

	if(!t2gconnector_is_autoupdate()){
		return;
	}
	?>
	<h2>Theme update</h1>
	<?php
	$remote_ver = get_option("t2gconnectorclient_".t2connectorclient_theme_slug()."_remote_ver", t2connectorclient_get_local_theme_version());
	$curr_ver = t2connectorclient_get_local_theme_version();
	if(version_compare($curr_ver, $remote_ver, '<')){
		echo "<h3 class=\"t2gconnectorclient-notices\"><span class=\" t2gconnectorclient-roundicon-warning \">!</span>".esc_attr__('Hooray! A new theme version is available','t2gconnectorclient').": ".$remote_ver.'</h3>';
		echo '<p><a class="t2gconnectorclient-btn" href="'.admin_url('update-core.php').'">Go to updates</a></p>';
		echo get_option("t2gconnectorclient_".t2connectorclient_theme_slug()."_description", "");
	} else {
		echo "<h3 class=\"t2gconnectorclient-notices\"><span class=\"dashicons dashicons-yes t2gconnectorclient-roundicon-success \"></span>".esc_attr__('Your theme is up to date.','t2gconnectorclient').'</h3>';
	}	
	?><hr class="t2gconnectorclient-spacer"><?php  
}


function t2gconnectorclient_support_output(){

	$newConfig = new T2GConnectorConfig();
	$settings = $newConfig->t2gconnector_settings();


	/**
	 * This separator is used only in case of an auto update
	 */
	if(t2gconnector_is_autoupdate()){
		?>
		<h2><?php echo esc_attr__('Manual and support', 't2gconnectorclient'); ?></h1>
		<hr class="t2gconnectorclient-spacer">
		<?php
	}
	

	if(array_key_exists('manual_url', $settings)){
		$manual_url = $settings['manual_url'];
		?>

		<div class="t2gconnectorclient-infobox t2gconnectorclient-infobox-column">
			<div class="t2gcol-left">
				<img src="<?php echo plugins_url( 't2gconnectorclient/assets/img/manual-icon.png' ); ?>">
			</div>
			<div class="t2gcol-right">
				<h2 class="t2gconnectorclient-steptitle"><?php echo esc_attr__('Read the documentation', 't2gconnectorclient'); ?></h2>
				<p>Be sure to check our documentation as every function is well described here.</p>
				<p><a target="_blank" class="t2gconnectorclient-btn " href="<?php echo esc_url($manual_url); ?>"><?php echo esc_attr__("Go to manual", "t2gconnector"); ?></a>
				</p>
			</div>
		</div>
		
		<?php
	}


	if(array_key_exists('helpdesk_url', $settings)){
		$helpdesk_url = $settings['helpdesk_url'];
		?>
		<div class="t2gconnectorclient-infobox t2gconnectorclient-infobox-column">
			<div class="t2gcol-left">
				<img src="<?php echo plugins_url( 't2gconnectorclient/assets/img/support-icon.png' ); ?>">
			</div>
			<div class="t2gcol-right">
				<h2 class="t2gconnectorclient-steptitle"><?php echo esc_attr__('Visit our support center', 't2gconnectorclient'); ?></h2>
				<p>We provide support only via our helpdesk forum.</p>
				<p><a target="_blank" class="t2gconnectorclient-btn " href="<?php echo esc_url($helpdesk_url); ?>"><?php echo esc_attr__("Go to Helpdesk", "t2gconnector"); ?></a>
				</p>
			</div>
		</div>
		<hr class="t2gconnectorclient-spacer">
		<?php
	}

}