<?php
/**
 * @package t2gconnectorclient
 * @author Themes2Go
 */

function t2gconnectorclient_demo_import(){
	if(false == t2gconnectorclient_check_code()){
		return;
	}
	$demos = t2gconnector_demos_list();
	if(false == $demos){
		?>
		Sorry, there are no available demos for this theme.
		<?php
		return;
	}

	/**
	 *  Saving options
	 */
	$caninstall = false;
	if(! empty( $_POST ) && check_admin_referer( 't2gconnectorclient_demo', 't2gconnectorclient_demo_set' )){
		if(isset($_POST['t2gdemoid'])){
			$caninstall = true;
		}
	}

	if ( true == $caninstall ) {

		if( isset($_POST['t2g_understood']) ){
			if(isset($_POST['t2gdemoid'])){
				if(array_key_exists($_POST['t2gdemoid'], $demos)){
					$config = $demos[$_POST['t2gdemoid']];
					$name = $config['name'];
					$folder = $config['folder'];
					$description = $config['description'];
					$screenshot = get_template_directory_uri().$folder.'screenshot.jpg';
					$demo_xml 		= get_template_directory().$folder.'dummy.xml';
					$demo_config 	= get_template_directory().$folder.'config.xml';
					$dummy_image 	= get_template_directory_uri().$folder.'dummy.jpg';// not used
					if(file_exists($demo_xml)){
						/* We need to save all the fields (xml and image) because the importer is called before theme loads */
						update_option("t2gconnector_t2gdemoid", $_POST['t2gdemoid']);
						update_option("t2gconnector_demo_xml", $demo_xml);
						update_option("t2gconnector_demo_config", $demo_config);
						update_option("t2gconnector_demo_dummy_image", $dummy_image);
						?>
						<h2>Step 2: confirm import of <?php echo esc_attr($name); ?></h2>
						
						<div class="t2gconnectorclient-row">
							<div class="t2gconnectorclient-col col-50">
								<img src="<?php echo esc_url($screenshot); ?>">
							</div>
							<div class="t2gconnectorclient-col col-50">
								<h3>Demo details</h3>
								<p><?php echo esc_attr($description); ?></p>
								<p>
									<a id="t2gconnectorclient-confirmation" href="<?php echo admin_url( 'themes.php?page=easint&demo_install=true'); ?>"  class="t2gconnectorclient-btn"><?php echo esc_attr__("Start import now", "t2gconnectorclient"); ?></a>
									or 
									<a href="<?php echo esc_url($_SERVER["REQUEST_URI"]); ?>"  class="t2gconnectorclient-btn t2gconnectorclient-btn-ghost"><?php echo esc_attr__("Change demo", "t2gconnectorclient"); ?></a>
								</p>
							</div>
						</div>
						<?php
					} else {
						?>
						<h3><?php echo esc_attr__("The selected demo doesn't exists", "t2gconnectorclient"); ?></h3>
						<p>
							<a href="<?php echo esc_url($_SERVER["REQUEST_URI"]); ?>"  class="t2gconnectorclient-btn"><?php echo esc_attr__("Change demo", "t2gconnectorclient"); ?></a>
						</p>
						<?php
					}
				} else {
					?>
					The selected demo doesn't exists
					<?php
				}
			}
		} else {
			?>
			<p>Please read the important information and accept to proceed.
				<a href="<?php echo esc_url($_SERVER["REQUEST_URI"]); ?>"  class="t2gconnectorclient-btn"><?php echo esc_attr__("Go back", "t2gconnectorclient"); ?></a>
			</p>
			<?php
		}
	} else {
		?>
		<form method="post" action="<?php echo esc_url($_SERVER["REQUEST_URI"]); ?>">
			<?php wp_nonce_field( "t2gconnectorclient_demo", "t2gconnectorclient_demo_set", true, true ); ?>
			<h2>Step 1: Choose your demo</h2>
			<p><strong style="color:#fff; background: #f00; padding: 2px 5px; border-radius: 3px;">Important:</strong> <strong>this operation can change or delete existing contents</strong>. Never import demo contents on existing websites. <input type="checkbox" class="filled-in" id="t2g_understood" name="t2g_understood" /> <label for="t2g_understood">I understand</label>
			</p>
			<div class="t2gconnectorclient-demos-archive">
				<?php
				foreach($demos as $id => $config){
					$name = $config['name'];
					$folder = $config['folder'];
					$description = $config['description'];
					$screenshot = get_template_directory_uri().$folder.'screenshot.jpg';
					$demo_xml = get_template_directory().$folder.'config.xml';
					$selected = '';
					if(get_option("t2gconnector_t2gdemoid", "") == $id){
						$selected = "selected";
					}
					if(file_exists($demo_xml)){
						?>
						<div class="t2gconnectorclient-demos-item">
							<label for="t2gdemoid<?php echo esc_attr($id);?>" class="<?php echo esc_attr($selected); ?>">
								<img class="demos-img" src="<?php echo esc_url($screenshot);?>">
								<input type="radio" id="t2gdemoid<?php echo esc_attr($id);?>" class="t2g-demoselector" name="t2gdemoid" value="<?php echo esc_attr($id);?>">
								<span class="demos-title"><?php echo esc_attr($name); ?></span>
								<span class="t2gconnector-card-hover t2gconnector-cloud-download">
								<img src="<?php echo plugins_url( 't2gconnectorclient/assets/img/cloud-download.png' ); ?>">
								</span>
							</label>
						</div>
						<?php
					} else {
						?>
						<div class="t2gconnectorclient-demos-item unavailable">
							<label for="t2gdemoid<?php echo esc_attr($id);?>">
								<span class="demos-title"><?php echo esc_attr($name); ?> [not available]</span>
								<img class="demos-img" src="<?php echo esc_url($screenshot);?>">	
							</label>
						</div>
						<?php
					}
				}
				?>
			</div>
			<p class="t2gconnectorclient-hidden"><input type="submit" id="t2gconnector-demo-select-submit" value="<?php echo esc_attr__("Proceed to importing", "t2gconnectorclient"); ?>" class="t2gconnectorclient-btn"></p>
		</form>
		<?php
	}
	return;
}






