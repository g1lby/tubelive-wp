<?php

/**
 * @package t2gconnectorclient
 * @author Themes2Go
 */
/**
 * Saving the parameters
 */
add_action('init', 't2gconnectorclient_settings_form_save' );
function t2gconnectorclient_settings_form_save(){
	if ( ! empty( $_POST ) ) {
		if(array_key_exists('t2gconnectorclient_product_activation_key',$_POST)){
			if(!check_admin_referer( 't2gconnectorclient_settings_update', 't2gconnectorclient_settings' )){
				return false;
			} else {
				$fields = array("t2gconnectorclient_product_activation_key");

				/**
				 * Check if the prod key is valid before saving
				 */
				if(isset($_POST['t2gconnectorclient_product_activation_key'])){
					$prod_k = $_POST['t2gconnectorclient_product_activation_key'];
					if(false == t2gconnectorclient_check_code($prod_k)) {
						echo 'Invalid code';
						update_option("t2gconnectorclient_product_activation_key", "");
						return false;
					}
					

					foreach ( $fields as $a ) {
						if(isset($_POST[$a])){
							update_option($a, sanitize_text_field(trim($_POST[$a])));
						}else{
							update_option($a, "");
						}
					}
				}
				return true;
			}
		}
	}
	if(!empty($_GET)){
		if(array_key_exists('t2gaction_remove',$_GET)){
			if($_GET['t2gaction_remove'] == 'remove'){
				update_option("t2gconnectorclient_product_activation_key", "");
			}
		}
	}
}
function t2gconnectorclient_settings_form(){
	$t2gconnector_deepcheck = t2gconnector_deepcheck();
	$stored_value = get_option("t2gconnectorclient_product_activation_key", "");
	if($stored_value != '' || !$t2gconnector_deepcheck){
		if(false == t2gconnectorclient_check_code() && $t2gconnector_deepcheck){
			?>
			<p class="t2gconnectorclient-alert">
				<?php echo esc_attr__("Invalid product key.","t2gconnectorclient"); ?>
				<a target="_blank" class="button button-primary" href="<?php echo esc_url(t2gconnectorclient_product_key_page()); ?>"><?php echo esc_attr__("Create new Product Key", "t2gconnector"); ?></a>
			</p>
			<?php
		} else {
			?>
			<div class="t2gconnectorclient-success">
				
				<h3 class="t2gconnectorclient-notices"><span class="dashicons dashicons-yes t2gconnectorclient-roundicon-success"></span>
				<?php echo esc_attr__('Theme active','t2gconnectorclient'); ?>
				</h3>
				
				<?php if($t2gconnector_deepcheck){ ?>
					<a href="<?php echo admin_url("?page=t2gconnectorclient-dashboard&tab=admin-settings&t2gaction_remove=remove"); ?>" class="t2gconnectorclient-btn t2gconnectorclient-btn-ghost">Remove activation</a>
				<?php } ?>

				<?php  

				/**
				 * [$tabs List of pages of the admin screen]
				 * @var array
				 */
				$tabs = array();
				$tabs['admin-plugins'] = __('Install required plugins','t2gconnectorclient');
				$tabs['editor-paste-text'] = __('Install demo contents','t2gconnectorclient');
				?>
				<h3>What can I do now?</h3>
				<ol class="t2gconnectorclient-inpagemenu">
				<?php  
				foreach( $tabs as $tab => $name ){
					$path = t2gconnectorclient_dashboard_path();
					?>	
					<li>
						<a class="t2gconnectorclient-nav-tab " href="<?php echo admin_url("admin.php?page=". esc_attr($path)."&tab=". esc_attr($tab)); ?>">
							<?php echo esc_attr($name);?>
						</a>
					</li>
					<?php
				}
				?>
				</ol>
			</div>
			<?php
		}
	} else {
		?>
		<div class="t2gconnectorclient-activation-settings">
			<div class="t2gconnectorclient-infobox ">
				<div class="t2gcol-left">
					<img src="<?php echo plugins_url( 't2gconnectorclient/assets/img/key-icon.png' ); ?>">
				</div>
				<div class="t2gcol-right">
					<h5 class="t2gconnectorclient-stepintro">Step 1</h5>
					<h2 class="t2gconnectorclient-steptitle">Generate product key</h2>
					<p>A new window will open. You will be required to create a new user connected with your Envato account</p>
					<p><a target="_blank" class="t2gconnectorclient-btn " href="<?php echo esc_url(t2gconnectorclient_registration_page()); ?>"><?php echo esc_attr__("Create your product key", "t2gconnector"); ?></a>
					</p>
				</div>
			</div>
			
			<div class="t2gconnectorclient-infobox ">
				<div class="t2gcol-left">
					<img src="<?php echo plugins_url( 't2gconnectorclient/assets/img/unlock-icon.png' ); ?>">
				</div>
				<div class="t2gcol-right">
					<h5 class="t2gconnectorclient-stepintro">Step 2</h5>
					<h2 class="t2gconnectorclient-steptitle">Add your Product Key</h2>

					<?php
					if(isset($_POST['t2gconnectorclient_product_activation_key'])){
					$prod_k = $_POST['t2gconnectorclient_product_activation_key'];
					if(false == t2gconnectorclient_check_code($prod_k)) {
							?>
							<p class="t2gconnectorclient-warning">
								<?php echo esc_attr__("Attention: the product key you entered is incorrect", "t2gconnectorclient");?>
							</p>
							<?php
						}
					}
					?>
					<form class="t2gconnectorclient-productkey-form" method="post" action="<?php echo admin_url('admin.php?page=t2gconnectorclient-dashboard&tab=admin-settings' ); ?>" novalidate="novalidate">
						<?php wp_referer_field(); ?>
						<?php wp_nonce_field('t2gconnectorclient_settings_update','t2gconnectorclient_settings'); ?>
						<?php
						$prod_k = get_option("t2gconnectorclient_product_activation_key", "");
						if(isset($_GET)){
							if(isset($_GET['t2gconnectorclient_prodk'])){
								if($_GET['t2gconnectorclient_prodk'] !== ''){
									$prod_k = $_GET['t2gconnectorclient_prodk'];
								}
							}
						}

						?>
						<p>
							<input name="t2gconnectorclient_product_activation_key" type="text" id="t2gconnectorclient_product_activation_key" 
							value="<?php echo esc_attr($prod_k); ?>" class="input-text" placeholder="Paste your product key here">
						<p>
						<p>
							<input type="submit" name="submit" id="submit" class="t2gconnectorclient-btn t2gconnectorclient-submit-inline" value="Activate Product">
						</p>
					</form>
				</div>
			</div>
		</div>
		<?php
	}

}