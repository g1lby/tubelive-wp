<?php
/*
Plugin Name: T2G Theme Dashboard
Plugin URI: http://www.themes2go.xyz
Description: Receive automatic updates, demo contents and support access
Version: 1.3.7
Author: Themes2Go
Text Domain: t2gconnectorclient
Domain Path: /languages
*/


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Redirects to dashboard after activation
 */
register_activation_hook(__FILE__, 't2gconnectorclient_plugin_activate');

/**
 *
 *	All the shared functions
 * 
 */
require plugin_dir_path( __FILE__ ) . '/inc/_t2gconnectorclient_shared.php';

/**
 *
 *	Backend dashboard
 * 
 */
require plugin_dir_path( __FILE__ ) . '/inc/_t2gconnector_admin_dashboard.php';

/**
 *
 *	TGM plugins activation client
 * 
 */
require plugin_dir_path( __FILE__ ) . '/TGM-Connector-Plugin-Activation/t2gconnector-plugins-activation.php';

/**
 *
 *	Theme Updater 
 * 
 */
// require plugin_dir_path( __FILE__ ) . '/T2G-Theme-Updater/t2gconnector_theme_updater.php';

/**
 *
 *	Easy Installer 
 * 
 */
require plugin_dir_path( __FILE__ ) . '/easy_installer/init.php';