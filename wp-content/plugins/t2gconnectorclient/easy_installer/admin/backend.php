<?php
/**
 *  Name - Installer Panel
 *  Dependency - Core Admin Class
 *  Version - 1.0
 *  Code Name - Nobody
 */

class IOAEasyFrontInstaller extends PLUGIN_IOA_PANEL_CORE {
	
	
	// init menu
	function __construct () { 

		add_action('admin_menu',array(&$this,'manager_admin_menu'));
        add_action('admin_init',array(&$this,'manager_admin_init'));
        
	 }
	
	// setup things before page loads , script loading etc ...
	function manager_admin_init(){	 }	
	
	function manager_admin_menu(){
		
		add_theme_page('Installer Panel', 'Installer Panel', 'edit_theme_options', 'easint' ,array($this,'manager_admin_wrap'));
		  
				  
	  }

	
	/**
	 * Main Body for the Panel
	 */

	function panelmarkup(){	
	   global $easy_metadata;

	   ?>

	   <div class="t2gconnectorclient-dashboard-container">

	   		<h2 class="t2gconnectorclient-tab-wrapper t2gconnectorclient-paper t2gconnectorclient-card">
				<?php  
				$current = 'editor-paste-text';
				$tabs = array(  
					'admin-settings' => 'Product Activation',
					'admin-plugins' => 'Plugins',
					'editor-paste-text' => 'Install demo', 
					'welcome-view-site' => 'Updates and Support',
				);
				foreach( $tabs as $tab => $name ){
					$class = ( $tab == $current ) ? ' nav-tab-active' : '';
					$path = t2gconnectorclient_dashboard_path();
					?>
						<a class="t2gconnectorclient-nav-tab <?php echo esc_attr($class); ?>" href="<?php echo admin_url("admin.php?page=". esc_attr($path)."&tab=". esc_attr($tab)); ?>">
							<i class="dashicons dashicons-<?php echo esc_attr($tab); ?>"></i>
							<?php echo esc_attr($name);?>
						</a>
					<?php
				}
				?>
			</h2>

		   	<?php
			if( (isset($_GET['page']) && $_GET['page'] == 'easint') && isset($_GET['demo_install'])  ) :
				?>
				<pre class="t2gconnectorclient-installation-output">
				<?php
					easy_import_start();
					EASYFInstallerHelper::beginInstall(); 
				?>
				</pre>
				<?php
				endif; 
			?>
			
			<?php 
			if(isset($_GET['demo_install'])): 
				easy_success_notification(); 
			else:
				?>
				<div class="t2gconnectorclient-dashboard-content t2gconnectorclient-paper t2gconnectorclient-card">
					<div class="t2gconnectorclient-demo-installer clearfix">
					<h1>Demo contents</h1>
					<p>To install the demo contents please visit the theme dashboard <a href="<?php echo admin_url('/admin.php?page=t2gconnectorclient-dashboard&tab=editor-paste-text' );?>">here</a></p>
					</div>
				</div>
				<?php
			endif; 
			?>

		</div>

		<?php

	    
	 }
	 

}

new IOAEasyFrontInstaller();