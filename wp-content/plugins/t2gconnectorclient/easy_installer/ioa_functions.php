<?php
/**
 *
 *	Themes2Go
 *	
 *	This file is heavily customized because otherwise is not setting the menu.
 *	setOptions needs to run before setMenus
 *
 *
 *
 *
 * 
 */
if(!is_admin()) return;




/**
 * Add CSS & JS Files
 */

add_action('admin_enqueue_scripts','addEASYJSFScripts');

function addEASYJSFScripts() {
			if(  isset($_GET['page']) && $_GET['page'] == 'easint'  )
			{
				wp_enqueue_style('easy-instf-css',EASY_F_PLUGIN_URL.'sprites/custom.css');
			}
		
	}
		

function addMetaDemoData()
{
	global $easy_metadata;

	    if(!$easy_metadata['data']) return; // No Config File Return

	    easy_import_after_xml();

	    /**
	     *
	     *
	     *	Important!
	     *	Set Options runs BEFORE setMenus
	     * 
	     */
	EASYFInstallerHelper::setOptions();
	EASYFInstallerHelper::setMenus();
	EASYFInstallerHelper::setMediaData();
	EASYFInstallerHelper::setWidgets();
	EASYFInstallerHelper::setHomePage();
	easy_import_end();
	flush_rewrite_rules();

}

if(isset($_GET['page']) && $_GET['page'] == 'easint' ) 
add_action('import_end','addMetaDemoData');