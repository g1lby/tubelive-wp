<?php

/**
 *
 *
 *
 * Themes2Go version
 * "017 July 29 H 01.27 Themes2Go fixed"
 */
			



class EASYFInstallerHelper
	{
		static function beginInstall()
		{
			global $easy_metadata;
				EASYFInstallerHelper::createImages();
				if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);
				if ( !class_exists( 'WP_Import' ) ) {
				  $class_wp_import = EASY_F_PLUGIN_PATH . '/admin/importer/wordpress-importer.php';
				  if ( file_exists( $class_wp_import ) )
				  require_once($class_wp_import);
				}
				$import_file = get_option("t2gconnector_demo_xml");
				// echo $import_file;
				if(is_file($import_file)){
					$wp_import = new WP_Import();
					$wp_import->fetch_attachments = false;
					if(isset($easy_metadata['data']->allow_attachment) && $easy_metadata['data']->allow_attachment == "yes"){
						$wp_import->fetch_attachments = true;
					}
					$wp_import->import($import_file);
				}
		}


		static function setMediaData()
		{	
			/**
			 *
			 *
			 *	Themes2Go new function
			 *
			 * 
			 */
			global $easy_metadata;
			$ttg_media_id = get_option('ttg_demo_image');
			if(!$ttg_media_id) {
				echo 'Missing media ID';
				return;
			}
			$ttg_media_url  = wp_get_attachment_image_src($ttg_media_id,'full');
			$ttg_media_url = $ttg_media_url[0];
			// setting as featured image for any post type
			$wp_query = new WP_Query(array(
				"post_type" => 'any',
				"posts_per_page" => -1
			));
			$pattern = '(\https?:\/\/\S+?\.(?:jpg|png|gif))';
			while ( $wp_query->have_posts() ) : $wp_query->the_post();
				$id =  get_the_ID();
			//	echo 'Setting image '.$ttg_media_id.' for post '.$id;
				set_post_thumbnail($id,   $ttg_media_id );
				$content = preg_replace($pattern,addslashes($ttg_media_url),  get_the_content() );
				wp_update_post(array( "ID" => $id , "post_content" => $content ));
			endwhile; 			
		}

		static function setOptions()
		{
			/**
			 * "017 July 29 H 01.27 Themes2Go fixed"
			 */
			global $easy_metadata, $wpdb;
			$input = json_decode( base64_decode($easy_metadata['data']->options),true);
			$excludes = explode(',',$easy_metadata['data']->options_exclude);

			/**
			 *
			 *
			 * 
			 * Themes2Go force exclude options
			 * @var array
			 *
			 *
			 * 
			 */
			$force_exclude = array("active_plugins","siteurl","home","blogname","blogdescription","admin_email","mailserver_login","mailserver_pass","auto_core_update_notified");
			foreach($input as $key => $val){
				if (!in_array($val["option_name"], $force_exclude)) {
					/**
					 *
					 *	Themes2Go notes
					 *	this can return "Error importing" for many options that WordPress forbids to import
					 * 
					 */
					
					
					/*
					2017 08 24 for the moment we don't do content replace
					///////////////////////////////////
					$ttg_media_id = get_option('ttg_demo_image');

					preg_match('/theme_mods/', $val["option_name"], $matches);

					if(count($matches) > 0){
						echo '<hr>';
						echo '<h3>'.$val["option_name"].'</h3>';
						echo $val["option_value"];
						echo '<hr>';
						





						if($ttg_media_id){
							
							$ttg_media_url  = wp_get_attachment_image_src($ttg_media_id,'medium');
							$ttg_media_url = $ttg_media_url[0];
							$pattern = '/([^"]*\.(?:png|jpeg|jpg|gif|bmp))/';
							$val["option_value"] = preg_replace($pattern, $ttg_media_url, $val["option_value"] );


							echo '<hr>';
							echo $val["option_value"];
						} 
					}*/


					if( is_serialized($val["option_value"]) ){
						if(false == update_option($val["option_name"],unserialize($val["option_value"]))){
							// echo '<h2>Error importing '.$val["option_name"].'</h2>';
						}
					} else {
						if(false == update_option($val["option_name"],$val["option_value"])){
							// echo '<h2>Error importing '.$val["option_name"].'</h2>';
						}
					}
				}
			}
			$sn = $easy_metadata['data']->option_shortname;
			foreach ($excludes as $key => $value) {
				if(trim($value)!="")
				delete_option($sn.$value);
			}
		}

		static function setMenus()
		{
			global $easy_metadata;
			
			$menus = explode(',',$easy_metadata['data']->menu_data);
			$menu_locs = array();
			foreach ($menus as $key => $menu) {
				$menu_item_array =  explode(':', $menu);
				$menu_obj = wp_get_nav_menu_object($menu_item_array[0]);
				$menu_id = $menu_obj->term_id;
				$menu_locs[$menu_item_array[1]] = $menu_id;
			}
			set_theme_mod( 'nav_menu_locations', $menu_locs );
		}
		static function setHomePage()
		{
			global $easy_metadata;
			$page = get_page_by_title( html_entity_decode($easy_metadata['data']->home_page) );
			update_option( 'page_on_front', $page->ID );
			update_option( 'show_on_front', 'page' );
		}
		static function setWidgets()
		{
			global $easy_metadata, $wpdb;
			$widgets = json_decode(base64_decode($easy_metadata['data']->sidebar_widgets),true);
			update_option('sidebars_widgets',$widgets);
			$inputs = base64_decode($easy_metadata['data']->widgets_data);	
			$inputs = json_decode($inputs,true);
			foreach ($inputs as $key => $input) {
				update_option($key,$input);
			}
		}
		static function createImages()
		 {
			/**
			 *
			 *
			 *	Themes2Go new function
			 *
			 * 
			 */
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			$filename = EASY_F_PLUGIN_PATH."demo_data_here/dummy.jpg";

			
			if(!file_exists($filename))
			{
				die("Missing demo iamge: ");
			}

			$filetype = wp_check_filetype( basename( $filename ), null );
			$path = wp_upload_dir(); 
			$cstatus =   copy($filename,  $path['path'].'/dummy.jpg'  );
			$filename = $path['path'].'/dummy.jpg';
			$wp_upload_dir = wp_upload_dir();

			$attachment = array(
				'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
				'post_mime_type' => $filetype['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			// Insert the attachment.
			$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );
			update_option('ttg_demo_image',$attach_id);

		}

	}