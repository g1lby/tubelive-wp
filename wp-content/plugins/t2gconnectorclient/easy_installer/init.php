<?php
/**
 * ONE CLICK Installer Plugin
 * http://themes2go.xyz
 * Description: Setup Websites in seconds
 * Version: 2.0
 * Author: Artillegence
 * Author URI: http://artillgence.com
 * Requires at least: 3.5
 * Tested up to: 4.7.4
 *
 * Text Domain: ioa
 * Domain Path: /i18n/languages/
 *
 * @package IOA
 * @category Builder
 * @author Artillegence + Themes2Go (LOTS of bug fixing and LOTS of dynamic implementations)
 */

// Variables & Constants Declaration
define('EASY_F_PLUGIN_URL',plugins_url().'/t2gconnectorclient/easy_installer/');
define('EASY_F_PLUGIN_PATH',plugin_dir_path(__FILE__));


/**
 * T2G Connector dynamic demos
 * @var [type]
 */
/*function t2gconnector_get_demo_data(){
	$xml = get_option("t2gconnector_demo_xml", '');
	$dummy_image = get_option("t2gconnector_demo_dummy_image", '');

	if(file_exists($xml)){
		$return = array(
			"config" 	=> $xml,
			"image" 	=> $dummy_image 
		);
	} else {
		$return = array(
			"config" 	=> '',	//get_template_directory(). '/T2G-connector-client/demo/demo1/'.'config.xml',
			"image" 	=> '' 	// get_template_directory(). '/T2G-connector-client/demo/demo1/dummy.jpg'
		);
	}
	return $return;
}
$easy_metadata = t2gconnector_get_demo_data();
*/


$easy_metadata = array(
	"config" => get_option("t2gconnector_demo_config"),
	"image" => EASY_F_PLUGIN_PATH."/demo_data_here/dummy.jpg"
);

if(file_exists($easy_metadata['config'])) :
	$xml= simplexml_load_file($easy_metadata['config']);
	$easy_metadata['data'] = $xml;
else:
	$easy_metadata['data'] = false;
endif;

// Init Core Rountines
require_once('ioa_hooks.php');


require_once(EASY_F_PLUGIN_PATH.'classes/class_installer_helper.php');
require_once(EASY_F_PLUGIN_PATH.'classes/class_admin_panel_maker.php');

require_once('ioa_functions.php');
require_once(EASY_F_PLUGIN_PATH.'admin/backend.php');
