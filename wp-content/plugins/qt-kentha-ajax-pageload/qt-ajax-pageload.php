<?php  
/*
Plugin Name: QT Kentha Ajax Pageload
Plugin URI: http://qantumthemes.com
Description: Adds page load with ajax to keep music playing across pages
Version: 1.2.4
Author: QantumThemes
Author URI: http://qantumthemes.com
*/

function qt_ajax_pageload_is_active(){
	return true;
}



/**
 * 	Enqueue scripts
 * 	=============================================
 */
if(!function_exists('qt_ajax_pageload_enqueue_scripts')){
function qt_ajax_pageload_enqueue_scripts(){
	if(is_user_logged_in()){
		if(current_user_can('edit_pages' )){
			return;
		}
	}
	// css already in the theme styles
	// wp_enqueue_style('qt_ajax_pageload_style', plugin_dir_url(__FILE__).'qt-apl-style.css' );
	if(get_theme_mod("kentha_enable_debug", 0)){
		wp_enqueue_script('qt_ajax_pageload_script', plugin_dir_url(__FILE__).'js/qt-kentha-ajax-pageload.js', array('jquery', 'kentha-qt-main-script'), '1.0', true );
	} else {
		wp_enqueue_script('qt_ajax_pageload_script', plugin_dir_url(__FILE__).'js/min/qt-kentha-ajax-pageload-min.js', array('jquery', 'kentha-qt-main-script'), '1.0', true );
	}
	
}}
add_action( 'wp_enqueue_scripts', 'qt_ajax_pageload_enqueue_scripts' );

