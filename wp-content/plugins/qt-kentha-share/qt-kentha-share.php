<?php
/*
Plugin Name: QT Kentha Share
Plugin URI: http://qantumthemes.com
Description: Add sharing icons
Version: 1.0.0
Author: QantumThemes
Author URI: http://qantumthemes.com
*/


function qt_kentha_share(){
	ob_start();
	$revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
    $pageurl = strtr(rawurlencode( get_permalink( get_the_ID() )  ), $revert);
	?>
	<div class="qt-part-share qt-content-primary qt-card qt-negative">	
		<a class="qt-btn-fb" target="_blank" href="<?php echo esc_url('https://www.facebook.com/sharer/sharer.php?u=').$pageurl; ?>"><i class="qt-socicon-facebook"></i></a>
		<a class="qt-btn-tw" target="_blank" href="<?php echo esc_url('https://twitter.com/intent/tweet?url=').$pageurl; ?>"><i class="qt-socicon-twitter"></i></a>
		<a class="qt-btn-gp" target="_blank" href="<?php echo esc_url('https://plus.google.com/share?url=').$pageurl; ?>"><i class="qt-socicon-googleplus"></i></a>
		<a class="qt-btn-pi" target="_blank" href="<?php echo esc_url('https://www.pinterest.es/pin/create/bookmarklet/?url=').$pageurl; ?>"><i class="qt-socicon-pinterest"></i></a>
	</div>
	<?php  
	echo ob_get_clean();
}




add_shortcode( 'qt-kentha-share', 'qt_kentha_share' );